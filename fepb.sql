-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2018 at 01:18 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fepb`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'बोर्डको परिचय', 1, '<p>नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार प्रवर्द्धन बोर्डको व्यवस्था गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ ।</p>\r\n\r\n<p>माननीय श्रम तथा रोजगार मन्त्रीको अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा अध्यक्ष सहित २५ जना सदस्यहरु रहनु भएको छ ।</p>', '<p>नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार प्रवर्द्धन बोर्डको व्यवस्था गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ ।</p>\r\n\r\n<p>माननीय श्रम तथा रोजगार मन्त्रीको अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा अध्यक्ष सहित २५ जना सदस्यहरु रहनु भएको छ ।</p>', 2, 2, '2018-11-28 03:23:33', '2018-12-06 03:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `image`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'banner 1', '5bffc02d1a4d7_og-logo.png', 1, 'thi si description of the banner page to be displayed in first', 2, 2, '2018-11-29 04:47:13', '2018-11-29 04:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'samachar1', 'kartik 2,2074', '5bfcd94e4ad25_3894133980_7e38f821fa.jpg', '5bfcd94e4dae5_CMS_Creative_164657191_Kingfisher.jpg', 'samachar', 1, 'main description here', 'wiauskg sadgdoibsd dgoisdg', 2, NULL, '2018-11-26 23:57:38', '2018-11-26 23:57:38'),
(2, 'suchana 1', 'mangsir 2, 2075', '5bfcdab0b56c4_well-images-for-wallpaper-desktop-24.jpg', '5bfcdab0b5d3b_images.jpg', 'suchana', 1, '<p>suchana ko edit page</p>', '<p>suchana ko ekbjewsfa</p>', 2, 2, '2018-11-27 00:03:32', '2018-12-06 03:26:36'),
(3, 'Press 1', 'push 2, 4044', NULL, NULL, 'press', 1, 'oiadlgoirsgb', 'ogirsdgolgs', 2, 2, '2018-11-27 00:04:23', '2018-11-27 00:04:33'),
(4, 'biill 1', 'magh 14 1075', NULL, NULL, 'bill', 1, 'awiskdjvdfakfjb', 'kdsjbgkdsjgb sdgiu', 2, NULL, '2018-11-27 00:05:01', '2018-11-27 00:05:01'),
(5, 'Chester Norman', '9782jwwe', NULL, '5bfe68ce51ae8_download.html', 'press', 1, NULL, NULL, 2, 2, '2018-11-28 04:22:10', '2018-11-28 04:22:41'),
(6, 'album2', 'jkasd2', '5bff8f1b380ee_5ba3cffb1b6aa_WhatsApp Image 2018-09-20 at 10.34.20 PM.jpeg', NULL, 'photo', 1, 'delfwfdsan', NULL, 2, 2, '2018-11-29 01:17:51', '2018-12-02 00:06:10'),
(7, 'video 1', 'dskba 324', '5bff8fa0d2fa9_eventbanner.jpg', NULL, 'video', 1, 'ldksab fsdg ', 'https://www.youtube.com/watch?time_continue=1&v=xadQwMU4qtU', 2, 2, '2018-11-29 01:20:04', '2018-12-06 05:06:40'),
(8, 'album1', 'djasf dsaf', '5bff96fa1ef64_5ba3d0489b0f9_WhatsApp Image 2018-09-20 at 10.30.53 PM.jpeg', NULL, 'photo', 1, 'dlsab eridgsk vosdgv', NULL, 2, 2, '2018-11-29 01:51:26', '2018-12-02 00:05:50'),
(9, 'album2', 'kweajs 214', '5c0371176c191_CMS_Creative_164657191_Kingfisher.jpg', NULL, 'photo', 1, 'eidsb dsigk asdgik edgsiagk&nbsp;', 'uiaksd sidug', 2, NULL, '2018-12-01 23:58:51', '2018-12-01 23:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `board_sachibalayas`
--

CREATE TABLE `board_sachibalayas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_sachibalayas`
--

INSERT INTO `board_sachibalayas` (`id`, `title`, `post`, `resource`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'sabin', 'adakxhya', '2', 1, 'thdsifhhsdf', 2, NULL, '2018-11-28 20:30:06', '2018-11-28 20:30:06'),
(2, NULL, 'upaadakshya', '3', 1, NULL, 2, NULL, '2018-11-28 20:30:20', '2018-11-28 20:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `board_works`
--

CREATE TABLE `board_works` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_works`
--

INSERT INTO `board_works` (`id`, `title`, `status`, `category`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'board gathan', 1, 'boardgathan', '<h4><strong>board gahtan title</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This is inside text for the page</p>', 2, 2, '2018-11-28 02:26:02', '2018-12-06 00:31:56'),
(11, 'work 1', 1, 'boardWork', '<h4><strong>Title for board work is here</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Detail about the work section is here</p>', 2, 2, '2018-12-06 00:17:30', '2018-12-06 00:31:16');

-- --------------------------------------------------------

--
-- Table structure for table `charts`
--

CREATE TABLE `charts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `male` double DEFAULT NULL,
  `female` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charts`
--

INSERT INTO `charts` (`id`, `title`, `date`, `fiscal`, `male`, `female`, `total`, `category`, `status`, `description`, `created_by`, `updated_by`, `category_id`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, '2064/65', 0, 0, 1300, NULL, 1, NULL, 2, NULL, 35, '2018-12-12 23:49:41', '2018-12-13 00:02:29'),
(3, NULL, NULL, '2066/67', 0, 0, 1800, NULL, 1, NULL, 2, NULL, 35, '2018-12-12 23:49:41', '2018-12-13 00:02:29'),
(12, NULL, NULL, '61/62', 0, 0, 150, NULL, 1, NULL, 2, NULL, 37, '2018-12-13 01:26:32', '2018-12-13 01:26:32');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(3, 'sabin', 'sabin8peace@gmail.com', NULL, 'usayd adsf', 'sasghd messag', '2018-11-30 01:10:04', '2018-11-30 01:10:04'),
(4, 'sandhya', 'sandhya8peace@gmail.com', NULL, 'slhd ddsafk', 'dsk idfg fsdg', '2018-11-30 01:10:27', '2018-11-30 01:10:27'),
(5, 'aewugsf', 'sabin8peace@gmail.com', NULL, 'weasf', 'rewsdhg', '2018-11-30 01:19:33', '2018-11-30 01:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `descriptions`
--

CREATE TABLE `descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `maintitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `descriptions`
--

INSERT INTO `descriptions` (`id`, `maintitle`, `title`, `status`, `description`, `note`, `short_description`, `created_by`, `category_id`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, '2iugjhvvjh', '2dfkfsbgdgsigbkb', 1, '<p>2fkvcxhbnfdxbk</p>', '<p>2fsfddlgfdlkg</p>', '<p>2kdsbgskdgd</p>', 2, 20, NULL, '2018-12-14 04:28:08', '2018-12-14 04:28:08'),
(4, '3 srdoilhsdg', '3fsodfilgh', 1, '<p>3 godflhnb</p>', '<p>3 rddfhi</p>', '<p>3rdlfkn</p>', 2, 20, NULL, '2018-12-14 04:50:00', '2018-12-14 04:50:00'),
(5, '4orsieddf', '4fdolhkndfdh', 1, '<p>4gdlhfnv</p>', '<p>4eodlghkl</p>', '<p>4preosdfhdnl</p>', 2, 20, NULL, '2018-12-14 04:50:00', '2018-12-14 04:50:00'),
(6, 'idausfdssddb 1', 'ukfdssdjafkj1', 1, '<p>isdkafkfsjddm1</p>', '<p>ikwasgfbdskfb1</p>', '<p>dskfb 1</p>', 2, 24, NULL, '2018-12-14 05:16:02', '2018-12-14 05:16:02'),
(7, '2 fsdzbdsfoig', '2ofdgl trfodigs redg', 1, '<p>2rosdgl rtsotoddyreyresh</p>', '<p>2oelkgbdoerig</p>', '<p>2ros grsfdogisdbf</p>', 2, 24, NULL, '2018-12-14 05:16:02', '2018-12-14 05:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `district` varchar(15) NOT NULL,
  `province` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `district`, `province`) VALUES
(1, 'TAPLEJUNG', 1),
(2, 'PANCHTHAR', 1),
(3, 'ILAM', 1),
(4, 'JHAPA', 1),
(5, 'MORANG', 1),
(6, 'SUNSARI', 1),
(7, 'DHANKUTA', 1),
(8, 'TERHATHUM', 1),
(9, 'SANKHUWASABHA', 1),
(10, 'BHOJPUR', 1),
(11, 'SOLUKHUMBU', 1),
(12, 'OKHALDHUNGA', 1),
(13, 'KHOTANG', 1),
(14, 'UDAYAPUR', 1),
(15, 'SAPTARI', 2),
(16, 'SIRAHA', 2),
(17, 'DHANUSHA', 2),
(18, 'MAHOTTARI', 2),
(19, 'SARLAHI', 2),
(20, 'SINDHULI', 3),
(21, 'RAMECHHAP', 3),
(22, 'DOLAKHA', 3),
(23, 'SINDHUPALCHOK', 3),
(24, 'KABHREPALANCHOK', 3),
(25, 'LALITPUR', 3),
(26, 'BHAKTAPUR', 3),
(27, 'KATHMANDU', 3),
(28, 'NUWAKOT', 3),
(29, 'RASUWA', 3),
(30, 'DHADING', 3),
(31, 'MAKAWANPUR', 3),
(32, 'RAUTAHAT', 2),
(33, 'BARA', 2),
(34, 'PARSA', 2),
(35, 'CHITAWAN', 3),
(36, 'GORKHA', 4),
(37, 'LAMJUNG', 4),
(38, 'TANAHU', 4),
(39, 'SYANGJA', 4),
(40, 'KASKI', 4),
(41, 'MANANG', 4),
(42, 'MUSTANG', 4),
(43, 'MYAGDI', 4),
(44, 'PARBAT', 4),
(45, 'BAGLUNG', 4),
(46, 'GULMI', 5),
(47, 'PALPA', 5),
(48, 'NAWALPARASI_W', 5),
(49, 'RUPANDEHI', 5),
(50, 'KAPILBASTU', 5),
(51, 'ARGHAKHANCHI', 5),
(52, 'PYUTHAN', 5),
(53, 'ROLPA', 5),
(54, 'RUKUM_W', 6),
(55, 'SALYAN', 6),
(56, 'DANG', 5),
(57, 'BANKE', 5),
(58, 'BARDIYA', 5),
(59, 'SURKHET', 6),
(60, 'DAILEKH', 6),
(61, 'JAJARKOT', 6),
(62, 'DOLPA', 6),
(63, 'JUMLA', 6),
(64, 'KALIKOT', 6),
(65, 'MUGU', 6),
(66, 'HUMLA', 6),
(67, 'BAJURA', 7),
(68, 'BAJHANG', 7),
(69, 'ACHHAM', 7),
(70, 'DOTI', 7),
(71, 'KAILALI', 7),
(72, 'KANCHANPUR', 7),
(73, 'DADELDHURA', 7),
(74, 'BAITADI', 7),
(75, 'DARCHULA', 7),
(76, 'NAWALPARASI_E', 4),
(77, 'RUKUM_E', 5);

-- --------------------------------------------------------

--
-- Table structure for table `excel_files`
--

CREATE TABLE `excel_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `date` text COLLATE utf8mb4_unicode_ci,
  `other_file` text COLLATE utf8mb4_unicode_ci,
  `category` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_files`
--

INSERT INTO `excel_files` (`id`, `title`, `date`, `other_file`, `category`, `description`, `short_description`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(16, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(17, 'itsolutionstuff.com', NULL, NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(18, 'Demo.itsolutionstuff.com', NULL, NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(19, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(20, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(21, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(22, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(23, 'itsolutionstuff.com', NULL, NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(24, 'Demo.itsolutionstuff.com', NULL, NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(25, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(26, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filetype` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`id`, `title`, `date`, `image`, `other_file`, `category`, `filetype`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'report1', '2018-11-27', '5bfcdf97a01e4_report1.jpg', '5bfae7a2a1619_C4C Inventory.pdf', 'report', NULL, 1, 'salkhslaglka', 'www.fnc.com', 2, 2, '2018-11-25 12:34:14', '2018-11-27 00:24:27'),
(3, 'report2', 'manhs 24949', '5bfcdfa16cf1f_report2.jpg', NULL, 'report', NULL, 1, 'qewkgsdabdsbgkj', 'ekwdsg', 2, 2, '2018-11-27 00:23:30', '2018-11-27 00:24:37'),
(4, 'report 3', 'ldadsf 83,3485', '5bfcdfb00007a_report1.jpg', NULL, 'report', NULL, 1, 'eweqksadbsdak', NULL, 2, 2, '2018-11-27 00:23:53', '2018-11-27 00:24:52'),
(5, 'video title 1', 'magh 2,3233', '5bfce01363359_video-player.png', NULL, 'video', NULL, 1, 'this is descripition of video section', 'https://www.youtube.com/watch?v=UDmky4jM4qU', 2, 2, '2018-11-27 00:26:31', '2018-11-27 00:30:23'),
(6, 'video title 2', 'falgun 2,2349', '5bfce04a019e0_video-player.png', NULL, 'video', NULL, 1, 'this is video detail 2 ikedsdja dfidsugb rrirfdsgxb rfdigb rtdfigb rtddif g<br />\r\nrfdskgb fgdjkg fd gfkd gifidg xrfsdg', 'https://www.youtube.com/watch?v=9I_bOgWQDZw', 2, 2, '2018-11-27 00:27:26', '2018-11-27 00:30:44'),
(7, 'video title 3', 'msh 3,34257', '5bfce0df2d5b9_video-player.png', NULL, 'video', NULL, 1, 'laskhf serddgk ferersiidg rffsddug rtef dsgirtisdf gg fdg usdfg&nbsp;', 'https://www.youtube.com/watch?v=UIftZGIyxHg', 2, NULL, '2018-11-27 00:29:55', '2018-11-27 00:29:55'),
(8, 'other file', 'jsd 2213`', NULL, '5bfe757326e28_contactus.html', 'other', 'in dsof', 1, NULL, NULL, 2, NULL, '2018-11-28 05:16:07', '2018-11-28 05:16:07'),
(9, 'report 1', 'dks 232', '5bff7b19c4bb6_report1.jpg', '5bff7b19c5768_Daily Report (2).pdf', 'report', 'progress', 1, NULL, NULL, 2, NULL, '2018-11-28 23:52:29', '2018-11-28 23:52:29'),
(10, 'testing product', 'asldfnk98', '5bff7b48ec5da_256-256-76f453c62108782f0cad9bfc2da1ae9d.png', '5bff7b48ed6b2_Experience-Letter1.pdf', 'report', 'publication', 1, NULL, NULL, 2, 2, '2018-11-28 23:53:16', '2018-11-28 23:53:47'),
(11, 'ani 1', 'asfd97', NULL, '5bffb197d9d6a_5bff8910167ee_Daily Report (1).pdf', 'ain', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 03:44:59', '2018-11-29 03:44:59'),
(12, 'bulletin 1', 'sjaf 32', '5bffb1b52221a_5ba3ccc9c5c14_APJB9192.JPG', '5bffb1b522c36_Experience-Letter1.pdf', 'bulletin', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 03:45:29', '2018-11-29 03:45:29'),
(13, 'pretibedhan', 'sl12`', NULL, '5bffb1cbe95a5_5bff892210158_cvmilan-1-4 (1).pdf', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'वार्षिक प्रतिवेदन', 2, 2, '2018-11-29 03:45:51', '2018-12-14 13:01:01'),
(14, 'adhyan pra isadf', 'ds 345', NULL, '5bffb35858301_5bff8910167ee_Daily Report (1).pdf', 'pratibedhan', 'adhyan prathibedhan', 1, NULL, NULL, 2, NULL, '2018-11-29 03:52:28', '2018-11-29 03:52:28'),
(15, 'nebadhan form', 'das 323', NULL, '5bffbb01d7533_5bff8910167ee_Daily Report (1).pdf', 'nebadan', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 04:25:09', '2018-11-29 04:25:09'),
(16, 'bulletin', 'dateh 2364', '5c10bd46150ff_th.jpg', '5c10bd4615884_Capture.PNG', 'bulletin', NULL, 1, NULL, NULL, 2, 2, '2018-12-12 02:03:22', '2018-12-12 02:08:48'),
(17, 'prakashan', 'date 31', '5c10bea0c20e8_th.jpg', '5c10bea0c2750_acl.PNG', 'prakashan', NULL, 1, NULL, NULL, 2, NULL, '2018-12-12 02:09:08', '2018-12-12 02:09:08'),
(18, 'adhyan pratibedhan', 'date 3213', NULL, '5c10c0b34af37_inventory.PNG', 'pratibedhan', 'adhyan prathibedhan', 1, NULL, NULL, 2, NULL, '2018-12-12 02:17:59', '2018-12-12 02:17:59'),
(19, 'pratibedhan', 'date 32', NULL, '5c10c1b66e00b_inventory.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'मासिक प्रतिवेदन', 2, 2, '2018-12-12 02:22:18', '2018-12-12 02:23:57'),
(20, 'FAQ SECTION', NULL, NULL, NULL, 'faq', NULL, 1, '<p>this&nbsp; is answer for first question</p>', 'this is qtn 1', 2, 2, '2018-12-12 04:10:52', '2018-12-12 04:11:51'),
(21, 'कामदारहरुले ध्यान दिनु पर्ने कुराहरु title here', NULL, NULL, NULL, 'basicnote', NULL, 1, '<p>description of&nbsp;कामदारहरुले ध्यान दिनु पर्ने कुराहरु</p>', NULL, 2, 2, '2018-12-12 04:13:12', '2018-12-12 04:15:17'),
(22, 'सहयोगी संस्थाहरु  title here', 'date 213', NULL, NULL, 'institute', NULL, 1, '<p><strong>सहयोगी संस्थाहरु </strong> description here</p>', NULL, 2, 2, '2018-12-12 04:14:55', '2018-12-12 04:15:04'),
(23, 'pragati pratibeshan 2', 'ksdfdh 35', NULL, '5c13fa16b4d5f_inventory.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'चौमासिक प्रतिवेदन', 2, NULL, '2018-12-14 12:59:38', '2018-12-14 12:59:38'),
(24, 'pragati pratibedhan 3', 'askf 46', NULL, '5c13fa39c2ebd_acl.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'वार्षिक प्रतिवेदन', 2, NULL, '2018-12-14 13:00:13', '2018-12-14 13:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `first_table`
--

CREATE TABLE `first_table` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` text,
  `email` text,
  `phone` double DEFAULT NULL,
  `address` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gapanapa`
--

CREATE TABLE `gapanapa` (
  `id` int(11) NOT NULL,
  `province` int(11) NOT NULL,
  `district` varchar(15) NOT NULL,
  `gapanapa` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gapanapa`
--

INSERT INTO `gapanapa` (`id`, `province`, `district`, `gapanapa`) VALUES
(1, 1, 'TAPLEJUNG', 'Aathrai Tribeni'),
(2, 1, 'TAPLEJUNG', 'Maiwakhola'),
(3, 1, 'TAPLEJUNG', 'Meringden'),
(4, 1, 'TAPLEJUNG', 'Mikwakhola'),
(5, 1, 'TAPLEJUNG', 'Phaktanglung'),
(6, 1, 'TAPLEJUNG', 'Phungling'),
(7, 1, 'TAPLEJUNG', 'Sidingba'),
(8, 1, 'TAPLEJUNG', 'Sirijangha'),
(9, 1, 'TAPLEJUNG', 'Yangwarak'),
(10, 1, 'PANCHTHAR', 'Falelung'),
(11, 1, 'PANCHTHAR', 'Falgunanda'),
(12, 1, 'PANCHTHAR', 'Hilihang'),
(13, 1, 'PANCHTHAR', 'Kummayak'),
(14, 1, 'PANCHTHAR', 'Miklajung'),
(15, 1, 'PANCHTHAR', 'Phidim'),
(16, 1, 'PANCHTHAR', 'Tumbewa'),
(17, 1, 'PANCHTHAR', 'Yangwarak'),
(18, 1, 'ILAM', 'Chulachuli'),
(19, 1, 'ILAM', 'Deumai'),
(20, 1, 'ILAM', 'Fakphokthum'),
(21, 1, 'ILAM', 'Illam'),
(22, 1, 'ILAM', 'Mai'),
(23, 1, 'ILAM', 'Maijogmai'),
(24, 1, 'ILAM', 'Mangsebung'),
(25, 1, 'ILAM', 'Rong'),
(26, 1, 'ILAM', 'Sandakpur'),
(27, 1, 'ILAM', 'Suryodaya'),
(28, 1, 'JHAPA', 'Arjundhara'),
(29, 1, 'JHAPA', 'Barhadashi'),
(30, 1, 'JHAPA', 'Bhadrapur'),
(31, 1, 'JHAPA', 'Birtamod'),
(32, 1, 'JHAPA', 'Buddhashanti'),
(33, 1, 'JHAPA', 'Damak'),
(34, 1, 'JHAPA', 'Gauradhaha'),
(35, 1, 'JHAPA', 'Gauriganj'),
(36, 1, 'JHAPA', 'Haldibari'),
(37, 1, 'JHAPA', 'Jhapa'),
(38, 1, 'JHAPA', 'Kachankawal'),
(39, 1, 'JHAPA', 'Kamal'),
(40, 1, 'JHAPA', 'Kankai'),
(41, 1, 'JHAPA', 'Mechinagar'),
(42, 1, 'JHAPA', 'Shivasataxi'),
(43, 1, 'MORANG', 'Belbari'),
(44, 1, 'MORANG', 'Biratnagar'),
(45, 1, 'MORANG', 'Budhiganga'),
(46, 1, 'MORANG', 'Dhanpalthan'),
(47, 1, 'MORANG', 'Gramthan'),
(48, 1, 'MORANG', 'Jahada'),
(49, 1, 'MORANG', 'Kanepokhari'),
(50, 1, 'MORANG', 'Katahari'),
(51, 1, 'MORANG', 'Kerabari'),
(52, 1, 'MORANG', 'Letang'),
(53, 1, 'MORANG', 'Miklajung'),
(54, 1, 'MORANG', 'Patahrishanishchare'),
(55, 1, 'MORANG', 'Rangeli'),
(56, 1, 'MORANG', 'Ratuwamai'),
(57, 1, 'MORANG', 'Sundarharaicha'),
(58, 1, 'MORANG', 'Sunwarshi'),
(59, 1, 'MORANG', 'Uralabari'),
(60, 1, 'SUNSARI', 'Barah'),
(61, 1, 'SUNSARI', 'Barju'),
(62, 1, 'SUNSARI', 'Bhokraha'),
(63, 1, 'SUNSARI', 'Dewanganj'),
(64, 1, 'SUNSARI', 'Dharan'),
(65, 1, 'SUNSARI', 'Duhabi'),
(66, 1, 'SUNSARI', 'Gadhi'),
(67, 1, 'SUNSARI', 'Harinagara'),
(68, 1, 'SUNSARI', 'Inaruwa'),
(69, 1, 'SUNSARI', 'Itahari'),
(70, 1, 'SUNSARI', 'Koshi'),
(71, 1, 'SUNSARI', 'Ramdhuni'),
(72, 1, 'SUNSARI', 'Koshi Tappu Wildlife Reserve'),
(73, 1, 'DHANKUTA', 'Chaubise'),
(74, 1, 'DHANKUTA', 'Chhathar Jorpati'),
(75, 1, 'DHANKUTA', 'Dhankuta'),
(76, 1, 'DHANKUTA', 'Khalsa Chhintang Shahidbhumi'),
(77, 1, 'DHANKUTA', 'Mahalaxmi'),
(78, 1, 'DHANKUTA', 'Pakhribas'),
(79, 1, 'DHANKUTA', 'Sangurigadhi'),
(80, 1, 'TERHATHUM', 'Aathrai'),
(81, 1, 'TERHATHUM', 'Chhathar'),
(82, 1, 'TERHATHUM', 'Laligurans'),
(83, 1, 'TERHATHUM', 'Menchayam'),
(84, 1, 'TERHATHUM', 'Myanglung'),
(85, 1, 'TERHATHUM', 'Phedap'),
(86, 1, 'SANKHUWASABHA', 'Bhotkhola'),
(87, 1, 'SANKHUWASABHA', 'Chainpur'),
(88, 1, 'SANKHUWASABHA', 'Chichila'),
(89, 1, 'SANKHUWASABHA', 'Dharmadevi'),
(90, 1, 'SANKHUWASABHA', 'Khandbari'),
(91, 1, 'SANKHUWASABHA', 'Madi'),
(92, 1, 'SANKHUWASABHA', 'Makalu'),
(93, 1, 'SANKHUWASABHA', 'Panchakhapan'),
(94, 1, 'SANKHUWASABHA', 'Sabhapokhari'),
(95, 1, 'SANKHUWASABHA', 'Silichong'),
(96, 1, 'BHOJPUR', 'Aamchowk'),
(97, 1, 'BHOJPUR', 'Arun'),
(98, 1, 'BHOJPUR', 'Bhojpur'),
(99, 1, 'BHOJPUR', 'Hatuwagadhi'),
(100, 1, 'BHOJPUR', 'Pauwadungma'),
(101, 1, 'BHOJPUR', 'Ramprasad Rai'),
(102, 1, 'BHOJPUR', 'Salpasilichho'),
(103, 1, 'BHOJPUR', 'Shadananda'),
(104, 1, 'BHOJPUR', 'Tyamkemaiyung'),
(105, 1, 'SOLUKHUMBU', 'Dudhkaushika'),
(106, 1, 'SOLUKHUMBU', 'Dudhkoshi'),
(107, 1, 'SOLUKHUMBU', 'Khumbupasanglahmu'),
(108, 1, 'SOLUKHUMBU', 'Likhupike'),
(109, 1, 'SOLUKHUMBU', 'Mahakulung'),
(110, 1, 'SOLUKHUMBU', 'Nechasalyan'),
(111, 1, 'SOLUKHUMBU', 'Solududhakunda'),
(112, 1, 'SOLUKHUMBU', 'Sotang'),
(113, 1, 'OKHALDHUNGA', 'Champadevi'),
(114, 1, 'OKHALDHUNGA', 'Chisankhugadhi'),
(115, 1, 'OKHALDHUNGA', 'Khijidemba'),
(116, 1, 'OKHALDHUNGA', 'Likhu'),
(117, 1, 'OKHALDHUNGA', 'Manebhanjyang'),
(118, 1, 'OKHALDHUNGA', 'Molung'),
(119, 1, 'OKHALDHUNGA', 'Siddhicharan'),
(120, 1, 'OKHALDHUNGA', 'Sunkoshi'),
(121, 1, 'KHOTANG', 'Ainselukhark'),
(122, 1, 'KHOTANG', 'Barahapokhari'),
(123, 1, 'KHOTANG', 'Diprung'),
(124, 1, 'KHOTANG', 'Halesi Tuwachung'),
(125, 1, 'KHOTANG', 'Jantedhunga'),
(126, 1, 'KHOTANG', 'Kepilasagadhi'),
(127, 1, 'KHOTANG', 'Khotehang'),
(128, 1, 'KHOTANG', 'Lamidanda'),
(129, 1, 'KHOTANG', 'Rupakot Majhuwagadhi'),
(130, 1, 'KHOTANG', 'Sakela'),
(131, 1, 'UDAYAPUR', 'Belaka'),
(132, 1, 'UDAYAPUR', 'Chaudandigadhi'),
(133, 1, 'UDAYAPUR', 'Katari'),
(134, 1, 'UDAYAPUR', 'Rautamai'),
(135, 1, 'UDAYAPUR', 'Sunkoshi'),
(136, 1, 'UDAYAPUR', 'Tapli'),
(137, 1, 'UDAYAPUR', 'Triyuga'),
(138, 1, 'UDAYAPUR', 'Udayapurgadhi'),
(139, 1, 'UDAYAPUR', 'Koshi Tappu Wildlife Reserve'),
(140, 2, 'SAPTARI', 'Agnisair Krishna Savaran'),
(141, 2, 'SAPTARI', 'Balan Bihul'),
(142, 2, 'SAPTARI', 'Belhi Chapena'),
(143, 2, 'SAPTARI', 'Bishnupur'),
(144, 2, 'SAPTARI', 'Bode Barsain'),
(145, 2, 'SAPTARI', 'Chhinnamasta'),
(146, 2, 'SAPTARI', 'Dakneshwori'),
(147, 2, 'SAPTARI', 'Hanumannagar Kankalini'),
(148, 2, 'SAPTARI', 'Kanchanrup'),
(149, 2, 'SAPTARI', 'Khadak'),
(150, 2, 'SAPTARI', 'Mahadeva'),
(151, 2, 'SAPTARI', 'Rajbiraj'),
(152, 2, 'SAPTARI', 'Rupani'),
(153, 2, 'SAPTARI', 'Saptakoshi'),
(154, 2, 'SAPTARI', 'Shambhunath'),
(155, 2, 'SAPTARI', 'Surunga'),
(156, 2, 'SAPTARI', 'Tilathi Koiladi'),
(157, 2, 'SAPTARI', 'Tirahut'),
(158, 2, 'SAPTARI', 'Koshi Tappu Wildlife Reserve'),
(159, 2, 'SIRAHA', 'Arnama'),
(160, 2, 'SIRAHA', 'Aurahi'),
(161, 2, 'SIRAHA', 'Bariyarpatti'),
(162, 2, 'SIRAHA', 'Bhagawanpur'),
(163, 2, 'SIRAHA', 'Bishnupur'),
(164, 2, 'SIRAHA', 'Dhangadhimai'),
(165, 2, 'SIRAHA', 'Golbazar'),
(166, 2, 'SIRAHA', 'Kalyanpur'),
(167, 2, 'SIRAHA', 'Karjanha'),
(168, 2, 'SIRAHA', 'Lahan'),
(169, 2, 'SIRAHA', 'Laxmipur Patari'),
(170, 2, 'SIRAHA', 'Mirchaiya'),
(171, 2, 'SIRAHA', 'Naraha'),
(172, 2, 'SIRAHA', 'Nawarajpur'),
(173, 2, 'SIRAHA', 'Sakhuwanankarkatti'),
(174, 2, 'SIRAHA', 'Siraha'),
(175, 2, 'SIRAHA', 'Sukhipur'),
(176, 2, 'DHANUSHA', 'Aaurahi'),
(177, 2, 'DHANUSHA', 'Bateshwor'),
(178, 2, 'DHANUSHA', 'Bideha'),
(179, 2, 'DHANUSHA', 'Chhireshwornath'),
(180, 2, 'DHANUSHA', 'Dhanauji'),
(181, 2, 'DHANUSHA', 'Dhanusadham'),
(182, 2, 'DHANUSHA', 'Ganeshman Charnath'),
(183, 2, 'DHANUSHA', 'Hansapur'),
(184, 2, 'DHANUSHA', 'Janaknandani'),
(185, 2, 'DHANUSHA', 'Janakpur'),
(186, 2, 'DHANUSHA', 'Kamala'),
(187, 2, 'DHANUSHA', 'Lakshminiya'),
(188, 2, 'DHANUSHA', 'Mithila'),
(189, 2, 'DHANUSHA', 'Mithila Bihari'),
(190, 2, 'DHANUSHA', 'Mukhiyapatti Musarmiya'),
(191, 2, 'DHANUSHA', 'Nagarain'),
(192, 2, 'DHANUSHA', 'Sabaila'),
(193, 2, 'DHANUSHA', 'Sahidnagar'),
(194, 2, 'MAHOTTARI', 'Aurahi'),
(195, 2, 'MAHOTTARI', 'Balwa'),
(196, 2, 'MAHOTTARI', 'Bardibas'),
(197, 2, 'MAHOTTARI', 'Bhangaha'),
(198, 2, 'MAHOTTARI', 'Ekdanra'),
(199, 2, 'MAHOTTARI', 'Gaushala'),
(200, 2, 'MAHOTTARI', 'Jaleswor'),
(201, 2, 'MAHOTTARI', 'Loharpatti'),
(202, 2, 'MAHOTTARI', 'Mahottari'),
(203, 2, 'MAHOTTARI', 'Manra Siswa'),
(204, 2, 'MAHOTTARI', 'Matihani'),
(205, 2, 'MAHOTTARI', 'Pipra'),
(206, 2, 'MAHOTTARI', 'Ramgopalpur'),
(207, 2, 'MAHOTTARI', 'Samsi'),
(208, 2, 'MAHOTTARI', 'Sonama'),
(209, 2, 'SARLAHI', 'Bagmati'),
(210, 2, 'SARLAHI', 'Balara'),
(211, 2, 'SARLAHI', 'Barahathawa'),
(212, 2, 'SARLAHI', 'Basbariya'),
(213, 2, 'SARLAHI', 'Bishnu'),
(214, 2, 'SARLAHI', 'Bramhapuri'),
(215, 2, 'SARLAHI', 'Chakraghatta'),
(216, 2, 'SARLAHI', 'Chandranagar'),
(217, 2, 'SARLAHI', 'Dhankaul'),
(218, 2, 'SARLAHI', 'Godaita'),
(219, 2, 'SARLAHI', 'Haripur'),
(220, 2, 'SARLAHI', 'Haripurwa'),
(221, 2, 'SARLAHI', 'Hariwan'),
(222, 2, 'SARLAHI', 'Ishworpur'),
(223, 2, 'SARLAHI', 'Kabilasi'),
(224, 2, 'SARLAHI', 'Kaudena'),
(225, 2, 'SARLAHI', 'Lalbandi'),
(226, 2, 'SARLAHI', 'Malangawa'),
(227, 2, 'SARLAHI', 'Parsa'),
(228, 2, 'SARLAHI', 'Ramnagar'),
(229, 3, 'SINDHULI', 'Dudhouli'),
(230, 3, 'SINDHULI', 'Ghanglekh'),
(231, 3, 'SINDHULI', 'Golanjor'),
(232, 3, 'SINDHULI', 'Hariharpurgadhi'),
(233, 3, 'SINDHULI', 'Kamalamai'),
(234, 3, 'SINDHULI', 'Marin'),
(235, 3, 'SINDHULI', 'Phikkal'),
(236, 3, 'SINDHULI', 'Sunkoshi'),
(237, 3, 'SINDHULI', 'Tinpatan'),
(238, 3, 'RAMECHHAP', 'Doramba'),
(239, 3, 'RAMECHHAP', 'Gokulganga'),
(240, 3, 'RAMECHHAP', 'Khadadevi'),
(241, 3, 'RAMECHHAP', 'Likhu'),
(242, 3, 'RAMECHHAP', 'Manthali'),
(243, 3, 'RAMECHHAP', 'Ramechhap'),
(244, 3, 'RAMECHHAP', 'Sunapati'),
(245, 3, 'RAMECHHAP', 'Umakunda'),
(246, 3, 'DOLAKHA', 'Baiteshwor'),
(247, 3, 'DOLAKHA', 'Bhimeshwor'),
(248, 3, 'DOLAKHA', 'Bigu'),
(249, 3, 'DOLAKHA', 'Gaurishankar'),
(250, 3, 'DOLAKHA', 'Jiri'),
(251, 3, 'DOLAKHA', 'Kalinchok'),
(252, 3, 'DOLAKHA', 'Melung'),
(253, 3, 'DOLAKHA', 'Sailung'),
(254, 3, 'DOLAKHA', 'Tamakoshi'),
(255, 3, 'SINDHUPALCHOK', 'Balefi'),
(256, 3, 'SINDHUPALCHOK', 'Barhabise'),
(257, 3, 'SINDHUPALCHOK', 'Bhotekoshi'),
(258, 3, 'SINDHUPALCHOK', 'Chautara SangachokGadhi'),
(259, 3, 'SINDHUPALCHOK', 'Helambu'),
(260, 3, 'SINDHUPALCHOK', 'Indrawati'),
(261, 3, 'SINDHUPALCHOK', 'Jugal'),
(262, 3, 'SINDHUPALCHOK', 'Lisangkhu Pakhar'),
(263, 3, 'SINDHUPALCHOK', 'Melamchi'),
(264, 3, 'SINDHUPALCHOK', 'Panchpokhari Thangpal'),
(265, 3, 'SINDHUPALCHOK', 'Sunkoshi'),
(266, 3, 'SINDHUPALCHOK', 'Tripurasundari'),
(267, 3, 'KABHREPALANCHOK', 'Banepa'),
(268, 3, 'KABHREPALANCHOK', 'Bethanchowk'),
(269, 3, 'KABHREPALANCHOK', 'Bhumlu'),
(270, 3, 'KABHREPALANCHOK', 'Chaurideurali'),
(271, 3, 'KABHREPALANCHOK', 'Dhulikhel'),
(272, 3, 'KABHREPALANCHOK', 'Khanikhola'),
(273, 3, 'KABHREPALANCHOK', 'Mahabharat'),
(274, 3, 'KABHREPALANCHOK', 'Mandandeupur'),
(275, 3, 'KABHREPALANCHOK', 'Namobuddha'),
(276, 3, 'KABHREPALANCHOK', 'Panauti'),
(277, 3, 'KABHREPALANCHOK', 'Panchkhal'),
(278, 3, 'KABHREPALANCHOK', 'Roshi'),
(279, 3, 'KABHREPALANCHOK', 'Temal'),
(280, 3, 'LALITPUR', 'Bagmati'),
(281, 3, 'LALITPUR', 'Godawari'),
(282, 3, 'LALITPUR', 'Konjyosom'),
(283, 3, 'LALITPUR', 'Lalitpur'),
(284, 3, 'LALITPUR', 'Mahalaxmi'),
(285, 3, 'LALITPUR', 'Mahankal'),
(286, 3, 'BHAKTAPUR', 'Bhaktapur'),
(287, 3, 'BHAKTAPUR', 'Changunarayan'),
(288, 3, 'BHAKTAPUR', 'Madhyapur Thimi'),
(289, 3, 'BHAKTAPUR', 'Suryabinayak'),
(290, 3, 'KATHMANDU', 'Budhanilakantha'),
(291, 3, 'KATHMANDU', 'Chandragiri'),
(292, 3, 'KATHMANDU', 'Dakshinkali'),
(293, 3, 'KATHMANDU', 'Gokarneshwor'),
(294, 3, 'KATHMANDU', 'Kageshwori Manahora'),
(295, 3, 'KATHMANDU', 'Kathmandu'),
(296, 3, 'KATHMANDU', 'Kirtipur'),
(297, 3, 'KATHMANDU', 'Nagarjun'),
(298, 3, 'KATHMANDU', 'Shankharapur'),
(299, 3, 'KATHMANDU', 'Tarakeshwor'),
(300, 3, 'KATHMANDU', 'Tokha'),
(301, 3, 'NUWAKOT', 'Belkotgadhi'),
(302, 3, 'NUWAKOT', 'Bidur'),
(303, 3, 'NUWAKOT', 'Dupcheshwar'),
(304, 3, 'NUWAKOT', 'Kakani'),
(305, 3, 'NUWAKOT', 'Kispang'),
(306, 3, 'NUWAKOT', 'Likhu'),
(307, 3, 'NUWAKOT', 'Meghang'),
(308, 3, 'NUWAKOT', 'Panchakanya'),
(309, 3, 'NUWAKOT', 'Shivapuri'),
(310, 3, 'NUWAKOT', 'Suryagadhi'),
(311, 3, 'NUWAKOT', 'Tadi'),
(312, 3, 'NUWAKOT', 'Tarkeshwar'),
(313, 3, 'NUWAKOT', 'Shivapuri Watershed and Wildlife Reserve'),
(314, 3, 'NUWAKOT', 'Langtang National Park'),
(315, 3, 'RASUWA', 'Gosaikunda'),
(316, 3, 'RASUWA', 'Kalika'),
(317, 3, 'RASUWA', 'Naukunda'),
(318, 3, 'RASUWA', 'Parbati Kunda'),
(319, 3, 'RASUWA', 'Uttargaya'),
(320, 3, 'DHADING', 'Benighat Rorang'),
(321, 3, 'DHADING', 'Dhunibesi'),
(322, 3, 'DHADING', 'Gajuri'),
(323, 3, 'DHADING', 'Galchi'),
(324, 3, 'DHADING', 'Gangajamuna'),
(325, 3, 'DHADING', 'Jwalamukhi'),
(326, 3, 'DHADING', 'Khaniyabash'),
(327, 3, 'DHADING', 'Netrawati'),
(328, 3, 'DHADING', 'Nilakantha'),
(329, 3, 'DHADING', 'Rubi Valley'),
(330, 3, 'DHADING', 'Siddhalek'),
(331, 3, 'DHADING', 'Thakre'),
(332, 3, 'DHADING', 'Tripura Sundari'),
(333, 3, 'MAKAWANPUR', 'Bagmati'),
(334, 3, 'MAKAWANPUR', 'Bakaiya'),
(335, 3, 'MAKAWANPUR', 'Bhimphedi'),
(336, 3, 'MAKAWANPUR', 'Hetauda'),
(337, 3, 'MAKAWANPUR', 'Indrasarowar'),
(338, 3, 'MAKAWANPUR', 'Kailash'),
(339, 3, 'MAKAWANPUR', 'Makawanpurgadhi'),
(340, 3, 'MAKAWANPUR', 'Manahari'),
(341, 3, 'MAKAWANPUR', 'Raksirang'),
(342, 3, 'MAKAWANPUR', 'Thaha'),
(343, 3, 'MAKAWANPUR', 'Parsa Wildlife Reserve'),
(344, 3, 'MAKAWANPUR', 'Chitawan National Park'),
(345, 2, 'RAUTAHAT', 'Baudhimai'),
(346, 2, 'RAUTAHAT', 'Brindaban'),
(347, 2, 'RAUTAHAT', 'Chandrapur'),
(348, 2, 'RAUTAHAT', 'Dewahhi Gonahi'),
(349, 2, 'RAUTAHAT', 'Durga Bhagwati'),
(350, 2, 'RAUTAHAT', 'Gadhimai'),
(351, 2, 'RAUTAHAT', 'Garuda'),
(352, 2, 'RAUTAHAT', 'Gaur'),
(353, 2, 'RAUTAHAT', 'Gujara'),
(354, 2, 'RAUTAHAT', 'Ishanath'),
(355, 2, 'RAUTAHAT', 'Katahariya'),
(356, 2, 'RAUTAHAT', 'Madhav Narayan'),
(357, 2, 'RAUTAHAT', 'Maulapur'),
(358, 2, 'RAUTAHAT', 'Paroha'),
(359, 2, 'RAUTAHAT', 'Phatuwa Bijayapur'),
(360, 2, 'RAUTAHAT', 'Rajdevi'),
(361, 2, 'RAUTAHAT', 'Rajpur'),
(362, 2, 'RAUTAHAT', 'Yemunamai'),
(363, 2, 'BARA', 'Adarshkotwal'),
(364, 2, 'BARA', 'Baragadhi'),
(365, 2, 'BARA', 'Bishrampur'),
(366, 2, 'BARA', 'Devtal'),
(367, 2, 'BARA', 'Jitpur Simara'),
(368, 2, 'BARA', 'Kalaiya'),
(369, 2, 'BARA', 'Karaiyamai'),
(370, 2, 'BARA', 'Kolhabi'),
(371, 2, 'BARA', 'Mahagadhimai'),
(372, 2, 'BARA', 'Nijgadh'),
(373, 2, 'BARA', 'Pacharauta'),
(374, 2, 'BARA', 'Parwanipur'),
(375, 2, 'BARA', 'Pheta'),
(376, 2, 'BARA', 'Prasauni'),
(377, 2, 'BARA', 'Simraungadh'),
(378, 2, 'BARA', 'Suwarna'),
(379, 2, 'BARA', 'Parsa Wildlife Reserve'),
(380, 2, 'PARSA', 'Bahudaramai'),
(381, 2, 'PARSA', 'Bindabasini'),
(382, 2, 'PARSA', 'Birgunj'),
(383, 2, 'PARSA', 'Chhipaharmai'),
(384, 2, 'PARSA', 'Dhobini'),
(385, 2, 'PARSA', 'Jagarnathpur'),
(386, 2, 'PARSA', 'Jirabhawani'),
(387, 2, 'PARSA', 'Kalikamai'),
(388, 2, 'PARSA', 'Pakahamainpur'),
(389, 2, 'PARSA', 'Parsagadhi'),
(390, 2, 'PARSA', 'Paterwasugauli'),
(391, 2, 'PARSA', 'Pokhariya'),
(392, 2, 'PARSA', 'SakhuwaPrasauni'),
(393, 2, 'PARSA', 'Thori'),
(394, 2, 'PARSA', 'Chitwan National Park'),
(395, 3, 'CHITAWAN', 'Bharatpur'),
(396, 3, 'CHITAWAN', 'Ichchhyakamana'),
(397, 3, 'CHITAWAN', 'Kalika'),
(398, 3, 'CHITAWAN', 'Khairahani'),
(399, 3, 'CHITAWAN', 'Madi'),
(400, 3, 'CHITAWAN', 'Rapti'),
(401, 3, 'CHITAWAN', 'Ratnanagar'),
(402, 3, 'CHITAWAN', 'Chitawan National Park'),
(403, 4, 'GORKHA', 'Aarughat'),
(404, 4, 'GORKHA', 'Ajirkot'),
(405, 4, 'GORKHA', 'Bhimsen'),
(406, 4, 'GORKHA', 'Chum Nubri'),
(407, 4, 'GORKHA', 'Dharche'),
(408, 4, 'GORKHA', 'Gandaki'),
(409, 4, 'GORKHA', 'Gorkha'),
(410, 4, 'GORKHA', 'Palungtar'),
(411, 4, 'GORKHA', 'Sahid Lakhan'),
(412, 4, 'GORKHA', 'Siranchok'),
(413, 4, 'GORKHA', 'Sulikot'),
(414, 4, 'LAMJUNG', 'Besishahar'),
(415, 4, 'LAMJUNG', 'Dordi'),
(416, 4, 'LAMJUNG', 'Dudhpokhari'),
(417, 4, 'LAMJUNG', 'Kwholasothar'),
(418, 4, 'LAMJUNG', 'MadhyaNepal'),
(419, 4, 'LAMJUNG', 'Marsyangdi'),
(420, 4, 'LAMJUNG', 'Rainas'),
(421, 4, 'LAMJUNG', 'Sundarbazar'),
(422, 4, 'TANAHU', 'Anbukhaireni'),
(423, 4, 'TANAHU', 'Bandipur'),
(424, 4, 'TANAHU', 'Bhanu'),
(425, 4, 'TANAHU', 'Bhimad'),
(426, 4, 'TANAHU', 'Byas'),
(427, 4, 'TANAHU', 'Devghat'),
(428, 4, 'TANAHU', 'Ghiring'),
(429, 4, 'TANAHU', 'Myagde'),
(430, 4, 'TANAHU', 'Rhishing'),
(431, 4, 'TANAHU', 'Shuklagandaki'),
(432, 4, 'SYANGJA', 'Aandhikhola'),
(433, 4, 'SYANGJA', 'Arjunchaupari'),
(434, 4, 'SYANGJA', 'Bhirkot'),
(435, 4, 'SYANGJA', 'Biruwa'),
(436, 4, 'SYANGJA', 'Chapakot'),
(437, 4, 'SYANGJA', 'Galyang'),
(438, 4, 'SYANGJA', 'Harinas'),
(439, 4, 'SYANGJA', 'Kaligandagi'),
(440, 4, 'SYANGJA', 'Phedikhola'),
(441, 4, 'SYANGJA', 'Putalibazar'),
(442, 4, 'SYANGJA', 'Waling'),
(443, 4, 'KASKI', 'Annapurna'),
(444, 4, 'KASKI', 'Machhapuchchhre'),
(445, 4, 'KASKI', 'Madi'),
(446, 4, 'KASKI', 'Pokhara Lekhnath'),
(447, 4, 'KASKI', 'Rupa'),
(448, 4, 'MANANG', 'Chame'),
(449, 4, 'MANANG', 'Narphu'),
(450, 4, 'MANANG', 'Nashong'),
(451, 4, 'MANANG', 'Neshyang'),
(452, 4, 'MUSTANG', 'Barhagaun Muktikhsetra'),
(453, 4, 'MUSTANG', 'Dalome'),
(454, 4, 'MUSTANG', 'Gharapjhong'),
(455, 4, 'MUSTANG', 'Lomanthang'),
(456, 4, 'MUSTANG', 'Thasang'),
(457, 4, 'MYAGDI', 'Annapurna'),
(458, 4, 'MYAGDI', 'Beni'),
(459, 4, 'MYAGDI', 'Dhaulagiri'),
(460, 4, 'MYAGDI', 'Malika'),
(461, 4, 'MYAGDI', 'Mangala'),
(462, 4, 'MYAGDI', 'Raghuganga'),
(463, 4, 'MYAGDI', 'Dhorpatan Hunting Reserve'),
(464, 4, 'PARBAT', 'Bihadi'),
(465, 4, 'PARBAT', 'Jaljala'),
(466, 4, 'PARBAT', 'Kushma'),
(467, 4, 'PARBAT', 'Mahashila'),
(468, 4, 'PARBAT', 'Modi'),
(469, 4, 'PARBAT', 'Painyu'),
(470, 4, 'PARBAT', 'Phalebas'),
(471, 4, 'BAGLUNG', 'Badigad'),
(472, 4, 'BAGLUNG', 'Baglung'),
(473, 4, 'BAGLUNG', 'Bareng'),
(474, 4, 'BAGLUNG', 'Dhorpatan'),
(475, 4, 'BAGLUNG', 'Galkot'),
(476, 4, 'BAGLUNG', 'Jaimuni'),
(477, 4, 'BAGLUNG', 'Kanthekhola'),
(478, 4, 'BAGLUNG', 'Nisikhola'),
(479, 4, 'BAGLUNG', 'Taman Khola'),
(480, 4, 'BAGLUNG', 'Tara Khola'),
(481, 4, 'BAGLUNG', 'Dhorpatan Hunting Reserve'),
(482, 5, 'GULMI', 'Chandrakot'),
(483, 5, 'GULMI', 'Chatrakot'),
(484, 5, 'GULMI', 'Dhurkot'),
(485, 5, 'GULMI', 'Gulmidarbar'),
(486, 5, 'GULMI', 'Isma'),
(487, 5, 'GULMI', 'Kaligandaki'),
(488, 5, 'GULMI', 'Madane'),
(489, 5, 'GULMI', 'Malika'),
(490, 5, 'GULMI', 'Musikot'),
(491, 5, 'GULMI', 'Resunga'),
(492, 5, 'GULMI', 'Ruru'),
(493, 5, 'GULMI', 'Satyawati'),
(494, 5, 'PALPA', 'Bagnaskali'),
(495, 5, 'PALPA', 'Mathagadhi'),
(496, 5, 'PALPA', 'Nisdi'),
(497, 5, 'PALPA', 'Purbakhola'),
(498, 5, 'PALPA', 'Rainadevi Chhahara'),
(499, 5, 'PALPA', 'Rambha'),
(500, 5, 'PALPA', 'Rampur'),
(501, 5, 'PALPA', 'Ribdikot'),
(502, 5, 'PALPA', 'Tansen'),
(503, 5, 'PALPA', 'Tinau'),
(504, 5, 'NAWALPARASI_W', 'Bardaghat'),
(505, 5, 'NAWALPARASI_W', 'Palhi Nandan'),
(506, 5, 'NAWALPARASI_W', 'Pratappur'),
(507, 5, 'NAWALPARASI_W', 'Ramgram'),
(508, 5, 'NAWALPARASI_W', 'Sarawal'),
(509, 5, 'NAWALPARASI_W', 'Sunwal'),
(510, 5, 'NAWALPARASI_W', 'Susta'),
(511, 5, 'RUPANDEHI', 'Butwal'),
(512, 5, 'RUPANDEHI', 'Devdaha'),
(513, 5, 'RUPANDEHI', 'Gaidahawa'),
(514, 5, 'RUPANDEHI', 'Kanchan'),
(515, 5, 'RUPANDEHI', 'Kotahimai'),
(516, 5, 'RUPANDEHI', 'Lumbini Sanskritik'),
(517, 5, 'RUPANDEHI', 'Marchawari'),
(518, 5, 'RUPANDEHI', 'Mayadevi'),
(519, 5, 'RUPANDEHI', 'Omsatiya'),
(520, 5, 'RUPANDEHI', 'Rohini'),
(521, 5, 'RUPANDEHI', 'Sainamaina'),
(522, 5, 'RUPANDEHI', 'Sammarimai'),
(523, 5, 'RUPANDEHI', 'Siddharthanagar'),
(524, 5, 'RUPANDEHI', 'Siyari'),
(525, 5, 'RUPANDEHI', 'Sudhdhodhan'),
(526, 5, 'RUPANDEHI', 'Tillotama'),
(527, 5, 'RUPANDEHI', 'Lumbini Sanskritik Development Area'),
(528, 5, 'KAPILBASTU', 'Banganga'),
(529, 5, 'KAPILBASTU', 'Bijayanagar'),
(530, 5, 'KAPILBASTU', 'Buddhabhumi'),
(531, 5, 'KAPILBASTU', 'Kapilbastu'),
(532, 5, 'KAPILBASTU', 'Krishnanagar'),
(533, 5, 'KAPILBASTU', 'Maharajgunj'),
(534, 5, 'KAPILBASTU', 'Mayadevi'),
(535, 5, 'KAPILBASTU', 'Shivaraj'),
(536, 5, 'KAPILBASTU', 'Suddhodhan'),
(537, 5, 'KAPILBASTU', 'Yashodhara'),
(538, 5, 'ARGHAKHANCHI', 'Bhumekasthan'),
(539, 5, 'ARGHAKHANCHI', 'Chhatradev'),
(540, 5, 'ARGHAKHANCHI', 'Malarani'),
(541, 5, 'ARGHAKHANCHI', 'Panini'),
(542, 5, 'ARGHAKHANCHI', 'Sandhikharka'),
(543, 5, 'ARGHAKHANCHI', 'Sitganga'),
(544, 5, 'PYUTHAN', 'Ayirabati'),
(545, 5, 'PYUTHAN', 'Gaumukhi'),
(546, 5, 'PYUTHAN', 'Jhimruk'),
(547, 5, 'PYUTHAN', 'Mallarani'),
(548, 5, 'PYUTHAN', 'Mandavi'),
(549, 5, 'PYUTHAN', 'Naubahini'),
(550, 5, 'PYUTHAN', 'Pyuthan'),
(551, 5, 'PYUTHAN', 'Sarumarani'),
(552, 5, 'PYUTHAN', 'Sworgadwary'),
(553, 5, 'ROLPA', 'Duikholi'),
(554, 5, 'ROLPA', 'Lungri'),
(555, 5, 'ROLPA', 'Madi'),
(556, 5, 'ROLPA', 'Rolpa'),
(557, 5, 'ROLPA', 'Runtigadi'),
(558, 5, 'ROLPA', 'Sukidaha'),
(559, 5, 'ROLPA', 'Sunchhahari'),
(560, 5, 'ROLPA', 'Suwarnabati'),
(561, 5, 'ROLPA', 'Thawang'),
(562, 5, 'ROLPA', 'Tribeni'),
(563, 6, 'RUKUM_W', 'Aathbiskot'),
(564, 6, 'RUKUM_W', 'Banfikot'),
(565, 6, 'RUKUM_W', 'Chaurjahari'),
(566, 6, 'RUKUM_W', 'Musikot'),
(567, 6, 'RUKUM_W', 'Sani Bheri'),
(568, 6, 'RUKUM_W', 'Tribeni'),
(569, 6, 'SALYAN', 'Bagchaur'),
(570, 6, 'SALYAN', 'Bangad Kupinde'),
(571, 6, 'SALYAN', 'Chhatreshwori'),
(572, 6, 'SALYAN', 'Darma'),
(573, 6, 'SALYAN', 'Dhorchaur'),
(574, 6, 'SALYAN', 'Kalimati'),
(575, 6, 'SALYAN', 'Kapurkot'),
(576, 6, 'SALYAN', 'Kumakhmalika'),
(577, 6, 'SALYAN', 'Sharada'),
(578, 6, 'SALYAN', 'Tribeni'),
(579, 5, 'DANG', 'Babai'),
(580, 5, 'DANG', 'Banglachuli'),
(581, 5, 'DANG', 'Dangisharan'),
(582, 5, 'DANG', 'Gadhawa'),
(583, 5, 'DANG', 'Ghorahi'),
(584, 5, 'DANG', 'Lamahi'),
(585, 5, 'DANG', 'Rajpur'),
(586, 5, 'DANG', 'Rapti'),
(587, 5, 'DANG', 'Shantinagar'),
(588, 5, 'DANG', 'Tulsipur'),
(589, 5, 'BANKE', 'Baijanath'),
(590, 5, 'BANKE', 'Duduwa'),
(591, 5, 'BANKE', 'Janki'),
(592, 5, 'BANKE', 'Khajura'),
(593, 5, 'BANKE', 'Kohalpur'),
(594, 5, 'BANKE', 'Narainapur'),
(595, 5, 'BANKE', 'Nepalgunj'),
(596, 5, 'BANKE', 'Rapti Sonari'),
(597, 5, 'BARDIYA', 'Badhaiyatal'),
(598, 5, 'BARDIYA', 'Bansagadhi'),
(599, 5, 'BARDIYA', 'Barbardiya'),
(600, 5, 'BARDIYA', 'Geruwa'),
(601, 5, 'BARDIYA', 'Gulariya'),
(602, 5, 'BARDIYA', 'Madhuwan'),
(603, 5, 'BARDIYA', 'Rajapur'),
(604, 5, 'BARDIYA', 'Thakurbaba'),
(605, 5, 'BARDIYA', 'Bardiya National Park'),
(606, 6, 'SURKHET', 'Barahtal'),
(607, 6, 'SURKHET', 'Bheriganga'),
(608, 6, 'SURKHET', 'Birendranagar'),
(609, 6, 'SURKHET', 'Chaukune'),
(610, 6, 'SURKHET', 'Chingad'),
(611, 6, 'SURKHET', 'Gurbhakot'),
(612, 6, 'SURKHET', 'Lekbeshi'),
(613, 6, 'SURKHET', 'Panchpuri'),
(614, 6, 'SURKHET', 'Simta'),
(615, 6, 'DAILEKH', 'Aathabis'),
(616, 6, 'DAILEKH', 'Bhagawatimai'),
(617, 6, 'DAILEKH', 'Bhairabi'),
(618, 6, 'DAILEKH', 'Chamunda Bindrasaini'),
(619, 6, 'DAILEKH', 'Dullu'),
(620, 6, 'DAILEKH', 'Dungeshwor'),
(621, 6, 'DAILEKH', 'Gurans'),
(622, 6, 'DAILEKH', 'Mahabu'),
(623, 6, 'DAILEKH', 'Narayan'),
(624, 6, 'DAILEKH', 'Naumule'),
(625, 6, 'DAILEKH', 'Thantikandh'),
(626, 6, 'JAJARKOT', 'Barekot'),
(627, 6, 'JAJARKOT', 'Bheri'),
(628, 6, 'JAJARKOT', 'Chhedagad'),
(629, 6, 'JAJARKOT', 'Junichande'),
(630, 6, 'JAJARKOT', 'Kuse'),
(631, 6, 'JAJARKOT', 'Shiwalaya'),
(632, 6, 'JAJARKOT', 'Tribeni Nalagad'),
(633, 6, 'DOLPA', 'Chharka Tangsong'),
(634, 6, 'DOLPA', 'Dolpo Buddha'),
(635, 6, 'DOLPA', 'Jagadulla'),
(636, 6, 'DOLPA', 'Kaike'),
(637, 6, 'DOLPA', 'Mudkechula'),
(638, 6, 'DOLPA', 'Shey Phoksundo'),
(639, 6, 'DOLPA', 'Thuli Bheri'),
(640, 6, 'DOLPA', 'Tripurasundari'),
(641, 6, 'JUMLA', 'Chandannath'),
(642, 6, 'JUMLA', 'Guthichaur'),
(643, 6, 'JUMLA', 'Hima'),
(644, 6, 'JUMLA', 'Kanakasundari'),
(645, 6, 'JUMLA', 'Patrasi'),
(646, 6, 'JUMLA', 'Sinja'),
(647, 6, 'JUMLA', 'Tatopani'),
(648, 6, 'JUMLA', 'Tila'),
(649, 6, 'KALIKOT', 'Kalika'),
(650, 6, 'KALIKOT', 'Khandachakra'),
(651, 6, 'KALIKOT', 'Mahawai'),
(652, 6, 'KALIKOT', 'Naraharinath'),
(653, 6, 'KALIKOT', 'Pachaljharana'),
(654, 6, 'KALIKOT', 'Palata'),
(655, 6, 'KALIKOT', 'Raskot'),
(656, 6, 'KALIKOT', 'Sanni Tribeni'),
(657, 6, 'KALIKOT', 'Tilagufa'),
(658, 6, 'MUGU', 'Chhayanath Rara'),
(659, 6, 'MUGU', 'Khatyad'),
(660, 6, 'MUGU', 'Mugum Karmarong'),
(661, 6, 'MUGU', 'Soru'),
(662, 6, 'HUMLA', 'Adanchuli'),
(663, 6, 'HUMLA', 'Chankheli'),
(664, 6, 'HUMLA', 'Kharpunath'),
(665, 6, 'HUMLA', 'Namkha'),
(666, 6, 'HUMLA', 'Sarkegad'),
(667, 6, 'HUMLA', 'Simkot'),
(668, 6, 'HUMLA', 'Tanjakot'),
(669, 7, 'BAJURA', 'Badimalika'),
(670, 7, 'BAJURA', 'Budhiganga'),
(671, 7, 'BAJURA', 'Budhinanda'),
(672, 7, 'BAJURA', 'Chhededaha'),
(673, 7, 'BAJURA', 'Gaumul'),
(674, 7, 'BAJURA', 'Himali'),
(675, 7, 'BAJURA', 'Pandav Gupha'),
(676, 7, 'BAJURA', 'Swami Kartik'),
(677, 7, 'BAJURA', 'Tribeni'),
(678, 7, 'BAJURA', 'Khaptad National Park'),
(679, 7, 'BAJHANG', 'Bithadchir'),
(680, 7, 'BAJHANG', 'Bungal'),
(681, 7, 'BAJHANG', 'Chabispathivera'),
(682, 7, 'BAJHANG', 'Durgathali'),
(683, 7, 'BAJHANG', 'JayaPrithivi'),
(684, 7, 'BAJHANG', 'Kanda'),
(685, 7, 'BAJHANG', 'Kedarseu'),
(686, 7, 'BAJHANG', 'Khaptadchhanna'),
(687, 7, 'BAJHANG', 'Masta'),
(688, 7, 'BAJHANG', 'Surma'),
(689, 7, 'BAJHANG', 'Talkot'),
(690, 7, 'BAJHANG', 'Thalara'),
(691, 7, 'BAJHANG', 'Khaptad National Park'),
(692, 7, 'ACHHAM', 'Bannigadhi Jayagadh'),
(693, 7, 'ACHHAM', 'Chaurpati'),
(694, 7, 'ACHHAM', 'Dhakari'),
(695, 7, 'ACHHAM', 'Kamalbazar'),
(696, 7, 'ACHHAM', 'Mangalsen'),
(697, 7, 'ACHHAM', 'Mellekh'),
(698, 7, 'ACHHAM', 'Panchadewal Binayak'),
(699, 7, 'ACHHAM', 'Ramaroshan'),
(700, 7, 'ACHHAM', 'Sanphebagar'),
(701, 7, 'ACHHAM', 'Turmakhad'),
(702, 7, 'ACHHAM', 'Khaptad National Park'),
(703, 7, 'DOTI', 'Adharsha'),
(704, 7, 'DOTI', 'Badikedar'),
(705, 7, 'DOTI', 'Bogtan'),
(706, 7, 'DOTI', 'Dipayal Silgadi'),
(707, 7, 'DOTI', 'Jorayal'),
(708, 7, 'DOTI', 'K I Singh'),
(709, 7, 'DOTI', 'Purbichauki'),
(710, 7, 'DOTI', 'Sayal'),
(711, 7, 'DOTI', 'Shikhar'),
(712, 7, 'DOTI', 'Khaptad National Park'),
(713, 7, 'KAILALI', 'Bardagoriya'),
(714, 7, 'KAILALI', 'Bhajani'),
(715, 7, 'KAILALI', 'Chure'),
(716, 7, 'KAILALI', 'Dhangadhi'),
(717, 7, 'KAILALI', 'Gauriganga'),
(718, 7, 'KAILALI', 'Ghodaghodi'),
(719, 7, 'KAILALI', 'Godawari'),
(720, 7, 'KAILALI', 'Janaki'),
(721, 7, 'KAILALI', 'Joshipur'),
(722, 7, 'KAILALI', 'Kailari'),
(723, 7, 'KAILALI', 'Lamkichuha'),
(724, 7, 'KAILALI', 'Mohanyal'),
(725, 7, 'KAILALI', 'Tikapur'),
(726, 7, 'KANCHANPUR', 'Bedkot'),
(727, 7, 'KANCHANPUR', 'Belauri'),
(728, 7, 'KANCHANPUR', 'Beldandi'),
(729, 7, 'KANCHANPUR', 'Bhimdatta'),
(730, 7, 'KANCHANPUR', 'Krishnapur'),
(731, 7, 'KANCHANPUR', 'Laljhadi'),
(732, 7, 'KANCHANPUR', 'Mahakali'),
(733, 7, 'KANCHANPUR', 'Punarbas'),
(734, 7, 'KANCHANPUR', 'Shuklaphanta'),
(735, 7, 'KANCHANPUR', 'Shuklaphanta National Park'),
(736, 7, 'DADELDHURA', 'Ajaymeru'),
(737, 7, 'DADELDHURA', 'Alital'),
(738, 7, 'DADELDHURA', 'Amargadhi'),
(739, 7, 'DADELDHURA', 'Bhageshwar'),
(740, 7, 'DADELDHURA', 'Ganayapdhura'),
(741, 7, 'DADELDHURA', 'Nawadurga'),
(742, 7, 'DADELDHURA', 'Parashuram'),
(743, 7, 'BAITADI', 'Dasharathchanda'),
(744, 7, 'BAITADI', 'Dilasaini'),
(745, 7, 'BAITADI', 'Dogadakedar'),
(746, 7, 'BAITADI', 'Melauli'),
(747, 7, 'BAITADI', 'Pancheshwar'),
(748, 7, 'BAITADI', 'Patan'),
(749, 7, 'BAITADI', 'Purchaudi'),
(750, 7, 'BAITADI', 'Shivanath'),
(751, 7, 'BAITADI', 'Sigas'),
(752, 7, 'BAITADI', 'Surnaya'),
(753, 7, 'DARCHULA', 'Apihimal'),
(754, 7, 'DARCHULA', 'Byas'),
(755, 7, 'DARCHULA', 'Dunhu'),
(756, 7, 'DARCHULA', 'Lekam'),
(757, 7, 'DARCHULA', 'Mahakali'),
(758, 7, 'DARCHULA', 'Malikaarjun'),
(759, 7, 'DARCHULA', 'Marma'),
(760, 7, 'DARCHULA', 'Naugad'),
(761, 7, 'DARCHULA', 'Shailyashikhar'),
(762, 4, 'NAWALPARASI_E', 'Binayee Tribeni'),
(763, 4, 'NAWALPARASI_E', 'Bulingtar'),
(764, 4, 'NAWALPARASI_E', 'Bungdikali'),
(765, 4, 'NAWALPARASI_E', 'Devchuli'),
(766, 4, 'NAWALPARASI_E', 'Gaidakot'),
(767, 4, 'NAWALPARASI_E', 'Hupsekot'),
(768, 4, 'NAWALPARASI_E', 'Kawasoti'),
(769, 4, 'NAWALPARASI_E', 'Madhyabindu'),
(770, 4, 'NAWALPARASI_E', 'Chitawan National Park'),
(771, 5, 'RUKUM_E', 'Bhume'),
(772, 5, 'RUKUM_E', 'Putha Uttarganga'),
(773, 5, 'RUKUM_E', 'Sisne'),
(774, 5, 'RUKUM_E', 'Dhorpatan Hunting Reserve');

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `important_links`
--

CREATE TABLE `important_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `link` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `important_links`
--

INSERT INTO `important_links` (`id`, `title`, `status`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'link 1', 1, 'www.sabinbajgain.com.np', 2, NULL, '2018-12-06 02:31:45', '2018-12-06 02:31:45'),
(3, 'link2', 1, 'www.codeforcore.com', 2, NULL, '2018-12-06 02:32:30', '2018-12-06 02:32:30'),
(4, 'reodh;l', 1, 'kfdh', 2, 2, '2018-12-06 20:18:37', '2018-12-06 20:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `ID` int(11) NOT NULL,
  `CompanyName` varchar(38) NOT NULL,
  `Address` varchar(46) NOT NULL,
  `Contact` varchar(16) NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Website` varchar(30) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Province` varchar(255) NOT NULL,
  `District` varchar(255) NOT NULL,
  `Local_Government` varchar(255) NOT NULL,
  `Remark` varchar(30) DEFAULT NULL,
  `latitude` double(10,8) DEFAULT NULL,
  `longitude` double(10,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`ID`, `CompanyName`, `Address`, `Contact`, `Email`, `Website`, `Status`, `Province`, `District`, `Local_Government`, `Remark`, `latitude`, `longitude`) VALUES
(1, 'IME Life Insurance Company Limited', 'Kathmandu Kathmandu N.P. 17 Lainchour', '14024071', 'info@imelifeinsurance.com', 'http://www.imelifeinsurance.co', b'1', '3', 'KATHMANDU', 'Kathmandu', 'test', 27.60567000, 85.45166010),
(2, 'Reliable Nepal Life Insurance Limited', 'Kathmandu Kathmandu N.P. 33 Gyaneshwor', '4423630', 'agam@reliablelife.com.np', 'http://www.reliablelife.com.np', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 0.00000000, 0.00000000),
(3, 'Union Life Insurance Co. Ltd', 'Kathmandu Kathmandu N.P. 10', '4784758', 'info@unionlife.com.np', 'http://www.unionlife.com.np', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 0.00000000, 0.00000000),
(4, 'Citizen Life Insurance Company Limited', 'Kathmandu Kathmandu N.P. 11 Thapathali', '01-4101744', 'kabi.phuyal@citizenlifenepal.com', 'http://www.citizenlifenepal.co', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 27.67197460, 85.34111020),
(5, 'Sanima Life Insurance', 'Kathmandu Kathmandu N.P. 1 Kamaladi, Kathmandu', '01-4420364', 'sanima@sanimalife.com', 'http://www.sanimalife.com', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 27.72613030, 85.34892080),
(6, 'Prabhu Life Insurance Limited', 'Kathmandu Kathmandu N.P. 28', '4262223', 'info@prabhulife.com', 'http://prabhulife.com', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 0.00000000, 0.00000000),
(7, 'Reliance Life Insurance Ltd.', 'Kathmandu New Baneshwor 10', '14787224', 'dofe@relifeinsurance.com', 'http://www.relifeinsurance.com', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 27.74021330, 85.33716200),
(8, 'SUN NEPAL LIFE INSURANCE COMPANY LTD', 'Kathmandu Putalisadak 1 kamaladi', '01 4436126/27/28', 'it@sunlife.com.np', 'http://sunlife.com.np', b'1', '3', 'KATHMANDU', 'Kathmandu', '', 27.73990950, 85.33707610),
(9, 'Prime Life Insurance', 'Kathmandu Kathmandu N.P. 34 130/23, HATTISAR, ', '014441414/166001', 'info@primelifenepal.com', 'http://primelifenepal.com/', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000),
(10, 'National Life Insurance', 'Kathmandu Kathmandu N.P. 2 Lazimpat', '9851033514', 'nlgilife@mail.com.np', 'http://www.nationallife.com.np', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000),
(11, 'Nepal Life Insurance', 'Kathmandu Putalisadak 1 kamaladi', '', 'info@nepallife.com.np', 'http://nepallife.com.np/', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000),
(12, 'Life Insurance Corporation', 'Kathmandu Kathmandu N.P. 1 Naxal', '', 'http://www.licnepal.com.np/', 'http://www.licnepal.com.np/', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000),
(13, 'Surya Life Insurance', 'Kathmandu Kathmandu N.P. 13 Gyaneshwor', '01-4423743', 'info@suryalife.com', 'http://suryalife.com/', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000),
(14, 'Gurash Life Insurance', 'Kathmandu Kathmandu N.P. 35 Tinkune', '51,991,055,199,0', 'info@guranslife.com', 'http://www.guranslife.com/', b'1', '3', 'KATHMANDU', 'Kathmandu', NULL, 0.00000000, 0.00000000);

-- --------------------------------------------------------

--
-- Table structure for table `jana_gunasos`
--

CREATE TABLE `jana_gunasos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jana_gunasos`
--

INSERT INTO `jana_gunasos` (`id`, `name`, `email`, `phone`, `address`, `message`, `image`, `created_at`, `updated_at`) VALUES
(1, 'sabin', 'sabin8peace@gmail.com', 983294834, 'ktm', '<p>uidsakgf disuf seridig fdisg</p>', '5c0f62c97fc99_th.jpg', '2018-12-11 01:25:01', '2018-12-11 01:25:01'),
(3, 'sabin', 'sabin@sabin.com', 2147483647, 'kathmansu', 'sajkd dsfi dsgbb ddsgd g', '5c0f9d894b021_th.jpg', '2018-12-11 05:35:41', '2018-12-11 05:35:41'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 05:44:05', '2018-12-11 05:44:05'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-14 03:15:37', '2018-12-14 03:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manual_tables`
--

CREATE TABLE `manual_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `table_name` text COLLATE utf8mb4_unicode_ci,
  `general_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manual_tables`
--

INSERT INTO `manual_tables` (`id`, `title`, `table_name`, `general_id`, `description`, `created_at`, `updated_at`) VALUES
(5, 'first table', 'first_table', 1, '<p>table 1</p>', '2018-12-10 00:38:14', '2018-12-10 00:38:14'),
(6, 'sectable', 'second_table', 1, '<p>uiasdg ewigasd aesdg&nbsp;</p>', '2018-12-10 05:15:46', '2018-12-10 05:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `rank` int(11) DEFAULT NULL,
  `joined_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `image`, `description`, `rank`, `joined_from`, `address`, `phone`, `email`, `position`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Ivan Lopez', '5bfba2a5d6fa1_3894133980_7e38f821fa.jpg', 'wiuakfs', 1, '2018-11-09', '300 Popip Place', '7179323072', 'gar@host.local', 'hazupo', 1, 2, NULL, '2018-11-26 01:52:09', '2018-11-26 01:52:09'),
(2, 'Ivajvwevsaf', '5bfbaadb82a9a_well-images-for-wallpaper-desktop-24.jpg', 'wiuakfs', 1, '2018-11-09', '300 Popip Place', '7179323072', 'gar@host.local', 'hazupo', 1, 2, 2, '2018-11-26 02:24:31', '2018-11-26 02:27:11'),
(3, 'sabin', '5bfcdbda69508_images.jpg', 'this is written for ceo', 3, '2018-11-10', 'kathmandu nepal', '98703738378', 's@g.com', 'CEO', 1, 2, NULL, '2018-11-27 00:08:30', '2018-11-27 00:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_13_152715_create_product_categories_table', 2),
(4, '2018_11_24_022402_create_mails_table', 2),
(5, '2018_10_07_035208_create_settings_table', 3),
(6, '2018_10_07_034806_create_sliders_table', 4),
(7, '2018_11_25_143226_create_abouts_table', 5),
(8, '2018_11_25_153815_create_board_works_table', 6),
(9, '2018_11_25_162046_create_mobile_apps_table', 7),
(10, '2018_11_25_164354_create_blogs_table', 8),
(11, '2018_11_25_165913_create_extras_table', 9),
(12, '2018_10_07_033851_create_contacts_table', 10),
(13, '2018_11_26_061143_create_members_table', 11),
(14, '2018_11_28_084205_create_board_sachibalayas_table', 12),
(15, '2018_11_28_090618_create_yojana_karyakrams_table', 13),
(16, '2018_11_28_133015_create_multimedia_table', 14),
(17, '2018_11_29_102024_create_banners_table', 15),
(18, '2018_12_02_073108_create_simples_table', 16),
(21, '2018_12_06_080524_create_important_links_table', 18),
(22, '2018_12_07_093152_create_excel_files_table', 19),
(25, '2018_12_08_043224_create_manual_tables_table', 20),
(26, '2018_12_08_050312_create_table_entries_table', 20),
(27, '2018_12_11_064513_create_jana_gunasos_table', 21),
(28, '2018_12_12_111124_create_trainings_table', 22),
(29, '2018_12_13_015919_create_charts_table', 23),
(30, '2018_12_04_065317_create_generals_table', 24),
(31, '2018_12_04_065343_create_descriptions_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_apps`
--

CREATE TABLE `mobile_apps` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobile_apps`
--

INSERT INTO `mobile_apps` (`id`, `title`, `link`, `image`, `mobile_image`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'This is the title of the project', 'https://play.google.com/store/apps/details?id=com.Braindigit.DOFEApp', '5c00a1d5b10cb_mobile.jpg', '5c00a1d5b1e29_mobimg2.png', 1, '<ul>\r\n	<li>This App is very good</li>\r\n	<li>this is list2</li>\r\n	<li>tdsif is 3</li>\r\n	<li>ysd gserdg 4</li>\r\n	<li>weaskdgbe sdg 5</li>\r\n	<li>sdkjgbfdslkg 9</li>\r\n</ul>', 'this is short description o fteh project', 2, 2, '2018-11-25 10:55:48', '2018-11-29 20:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `multimedia`
--

CREATE TABLE `multimedia` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `multimedia`
--

INSERT INTO `multimedia` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'audio', 'siakf', NULL, '5bfe9e9299f79_logo.png', 'audio', 1, 'dslkngs sgdk', NULL, 2, NULL, '2018-11-28 08:11:34', '2018-11-28 08:11:34'),
(2, 'video 1', 'vidksadbjf', '5c08f22cf3508_5bfcdab0b5d3b_images.jpg', NULL, 'video', 1, 'ths is description of viedo', 'https://www.youtube.com/watch?v=xadQwMU4qtU', 2, 2, '2018-11-28 08:14:08', '2018-12-06 04:47:10'),
(3, 'phphp', 'ksafb', '5bfe9f3dcabc5_mobimg2.png', NULL, 'photo', 1, 'daskg dsgk', NULL, 2, NULL, '2018-11-28 08:14:25', '2018-11-28 08:14:25'),
(6, 'wefvoami', 'zoffikbi', '5bffc3b71c289_news1.jpg', NULL, 'photo', 1, 'datuod', NULL, 2, NULL, '2018-11-29 05:02:19', '2018-11-29 05:02:19'),
(7, 'ewtoova', 'fimteg', '5bffc3c42302c_news.jpg', NULL, 'photo', 1, 'vejo', NULL, 2, NULL, '2018-11-29 05:02:32', '2018-11-29 05:02:32'),
(8, 'ivapup', 'nocjob', NULL, '5bffc3e6d3ecd_5b80eafe113db_Banana Minions Ringtone -) - YouTube[via torchbrowser.com] (1).aac', 'audio', 1, 'mape', NULL, 2, NULL, '2018-11-29 05:03:06', '2018-11-29 05:03:06'),
(9, 'iwnebe', 'vacidod', NULL, '5bffc3f7a9d8b_5b80ef1cd796f_Banana Minions Ringtone -) - YouTube[via torchbrowser.com].aac', 'audio', 1, 'lew', NULL, 2, NULL, '2018-11-29 05:03:23', '2018-11-29 05:03:23'),
(10, 'pgp', 'kasf234', '5c08ee966c191_5bfcdab0b5d3b_images (1).jpg', NULL, 'photo', 1, 'poasflnasf', NULL, 2, NULL, '2018-12-06 03:55:38', '2018-12-06 03:55:38'),
(11, 'FAQ SECTION', NULL, NULL, NULL, 'faq', 1, '<p>answere of qtn</p>', 'question is here', 2, NULL, '2018-12-12 04:34:09', '2018-12-12 04:34:09'),
(12, 'sahayogi sanstha here', NULL, NULL, NULL, 'institute', 1, '<p>desc of sahayogi sanstha here</p>\r\n\r\n<ul>\r\n	<li>ioadsgkf</li>\r\n	<li>feasd</li>\r\n	<li>g</li>\r\n	<li>sddgd</li>\r\n	<li>g</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', NULL, 2, 2, '2018-12-12 04:34:48', '2018-12-13 04:25:47'),
(13, 'kamdar le dhyan dinu parni ko title', NULL, NULL, NULL, 'basicnote', 1, '<p>description of kamdar le dhyan dinu ar</p>\r\n\r\n<ul>\r\n	<li>this si point</li>\r\n	<li>tsf eaofidfsg osdig</li>\r\n	<li>fsdogil erosdg&nbsp;</li>\r\n	<li>reesdbobgil resgd</li>\r\n	<li>redsdbg sregdf</li>\r\n</ul>', NULL, 2, 2, '2018-12-12 04:35:19', '2018-12-13 04:20:56'),
(14, 'FAQ SECTION', NULL, NULL, NULL, 'faq', 1, '<p>This is second question ko answer ho</p>', 'second question', 2, NULL, '2018-12-13 04:14:36', '2018-12-13 04:14:36');

-- --------------------------------------------------------

--
-- Table structure for table `orientation_done`
--

CREATE TABLE `orientation_done` (
  `ID` int(11) NOT NULL,
  `Permission` int(11) NOT NULL,
  `OrName` varchar(91) NOT NULL,
  `Address` varchar(56) NOT NULL,
  `Contact` varchar(32) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Website` varchar(30) DEFAULT NULL,
  `Status` varchar(8) DEFAULT NULL,
  `Province` varchar(33) NOT NULL,
  `District` varchar(9) NOT NULL,
  `Local_Government` varchar(16) NOT NULL,
  `Remark` varchar(30) DEFAULT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `longitude` decimal(7,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orientation_done`
--

INSERT INTO `orientation_done` (`ID`, `Permission`, `OrName`, `Address`, `Contact`, `Email`, `Website`, `Status`, `Province`, `District`, `Local_Government`, `Remark`, `latitude`, `longitude`) VALUES
(136, 157, 'BAGESHWORI ORIENTATION AND TRAINING CENTER PVT.LTD', 'Banke nepalgunjn.p. 1 NEPALGUNG', '9841595112', 'baggeshworiotc@gmail.com', '', 'Active', '5', 'BANKE', 'Nepalgunj', '', '0.00000000', '0.0000'),
(146, 169, 'Karnali Training Center pvt. ltd.', 'Banke nepalgunjn.p. 13', '081-527682', 'karnaliorient@gmail.com', '', 'Active', '5', 'BANKE', 'Nepalgunj', '', '0.00000000', '0.0000'),
(129, 150, 'AMBIKESHWORI ORIENTATION AND TRAINING CENTER PVT.LTD', 'Banke nepalgunjn.p. 14 NEPALJUNG', '9851063157', 'ambikeshwori.traning@gmail.com', '', 'Active', '5', 'BANKE', 'Nepalgunj', '', '0.00000000', '0.0000'),
(111, 129, 'BAGESHWORI INTERNATIONAL INSTITUTE PVT.LTD', 'Banke nepalgunjn.p. 2 Setubika Chowk', '', '', '', '', '5', 'BANKE', 'Nepalgunj', '', NULL, NULL),
(114, 133, 'SAMRAJ TRAINING INSTITUTE', 'Dhanusha dhalkebar 8', '041-560214/9841043584', 'samrajtraining@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Mithila', '', '0.00000000', '0.0000'),
(121, 142, 'Sangam Shram Bikash Thatha Soochana Kendra', 'Dhanusha dhalkebar 8 Lalgadh', '9851061005', 'sangambikash1@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Mithila', '', '0.00000000', '0.0000'),
(117, 137, 'PRIYA ORIENTATION AND TRAINING CENTRE PVT. LTD.', 'Dhanusha dhanusadham 7 SARSA', '9851090900', 'priyaor61@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Dhanusadham', '', '0.00000000', '0.0000'),
(2, 134, 'Dhanusha Orientation and Training Center pvt.Ltd', 'Dhanusha Janakpur N.P. 1', '9851033979', 'dhanushaorient@gmail.com', '', 'Inactive', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(115, 135, 'DRISTI ORIENTATION TRAINING CENTER PVT.LTD', 'Dhanusha Janakpur N.P. 1 JANAKPUR', '9840016595', 'dristioriantations@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(112, 131, 'Sarbashree Training And Orientation Center Pvt. Ltd.', 'Dhanusha Janakpur N.P. 11 Mills Area Hanuman Mandir', '41525973', 'sarbashreeorientation@gmail.com', 'http://www.sarbashreetraining.', 'Active', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(3, 141, 'BARSHA ORIENTATION TRAINING CENTER', 'Dhanusha Janakpur N.P. 14', '9851026983', 'barshaorientation@gmail.com', '', 'Inactive', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(116, 136, 'BHUTTI HR SOLUTION PVT.LTD', 'Dhanusha Janakpur N.P. 8 JANAKPUR', '9851091727', 'bhuttihr@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Janakpur', 'error', '26.75542090', '85.9406'),
(120, 140, 'RAJDEVI ORIENTATION AND TRAINING CERNTER PVT.LTD', 'Dhanusha Janakpur N.P. 8 JANAKPUR', '9851046102', 'rajdeviore12@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(5, 164, 'SOCIAL DEVELOPMENT CENTRE PVT.LTD.', 'Dhanusha Janakpur N.P. 9', '9855025718', 'sdc.npl@gmail.com', '', 'Inactive', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(118, 138, 'JEEVANDIP ORIENTATION AND TRAINING CENTER PVT.LTD', 'Dhanusha Janakpur N.P. 9', '9851050869', 'jeevandeep041@gmail.com', '', 'Active', '2', 'DHANUSHA', 'Janakpur', '', '0.00000000', '0.0000'),
(130, 151, 'National Future Foundation Pvt. Ltd.', 'Jhapa birtamod 1 Atithi Sadan Road', '23546511', 'nationalfuturefoundation@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', 'No website', '26.64344793', '87.9886'),
(145, 168, 'Sherpa Orentation Coultancy Pvt Ltd', 'Jhapa birtamod 1 binayek tole', '023-545967', 'sherpajhapa@gmail.com', 'http://www.sherpajhapa.com', 'Active', '1', 'JHAPA', 'Birtamod', '', '0.00000000', '0.0000'),
(125, 146, 'Asia Link Orientation Training Center Pvt. Ltd.', 'Jhapa birtamod 1 Birtamod', '23541861', 'asialinkorientation@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', '', '0.00000000', '0.0000'),
(143, 166, 'Sudha Training and Orientatoin Center Pvt Ltd', 'Jhapa birtamod 1 Birtamod bazar', '9851070388', 'sudhaorientationjhapa@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', 'no', '26.64277426', '87.9892'),
(126, 147, 'PRADHAN ORIENTATION AND TRAINING CENTER', 'Jhapa birtamod 2 JHAPA', '9841416720', 'pradhan.orientationandtraining@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', 'birtamod anarmani', '26.63027347', '87.9922'),
(134, 155, 'PURBAANCHAL SUNRISE TRAINING CENTER PVT.LTD', 'Jhapa birtamod 4 BIRTAMOD', '9851059685', 'purbanchalsunrise2017@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', 'done', '26.64376678', '87.9886'),
(123, 144, 'SOUTHASIAN ORIENTATON AND TRAINING CENTER PVT.LTD', 'Jhapa birtamod 7 BIRTAMOD', '23546569', 'southasian4@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', '', '0.00000000', '0.0000'),
(131, 152, 'INODAY EDUCATION SKILL CONSULTANCY PVT.LTD.', 'Jhapa birtamod 7 BIRTAMOD ROAD', '23546525', 'inoday.orientation@gmail.com', '', 'Active', '1', 'JHAPA', 'Birtamod', '', '0.00000000', '0.0000'),
(138, 159, 'MUNAL TECHINICAL TRAINING CENTER PVT.LTD', 'Jhapa damakn.p. 12 DAMAK', '9852048472', 'munaltechnical1@gmail.com', '', 'Active', '1', 'JHAPA', 'Damak', '', '0.00000000', '0.0000'),
(135, 156, 'N.B. GLOBAL CONSULTANCY PVT.LTD', 'Jhapa damakn.p. 13 DAMAK', '9851050730', 'nbglobalconsultancy@gmail.com', '', 'Active', '1', 'JHAPA', 'Damak', 'damak-05', NULL, NULL),
(4, 162, 'Sana Orientation and Traning center Pvt.Ltd', 'Kaski dhital 17', '61540446', 'sanaorientation@gmail.com', '', 'Inactive', '4', 'KASKI', 'Machhapuchchhre', '', '0.00000000', '0.0000'),
(141, 163, 'PRAMISHA MULTI TRAINING INSTITUTE PVT.LTD', 'Kaski pokharan.p. 8 POKHARA', '9851086358', 'pramishaorientation55@gmail.com', '', 'Active', '4', 'KASKI', 'Pokhara Lekhnath', '', '0.00000000', '0.0000'),
(1, 130, 'S.A training center', 'Kaski pokharan.p. 8 Prithivichok', '61535142', 'sha.mohammad6@gmail.com', '', 'Inactive', '4', 'KASKI', 'Pokhara Lekhnath', 'test', '27.43028974', '85.5396'),
(119, 139, 'Times Work (P) Ltd', 'Kaski pokharan.p. 8 Prithivichowk', '061-541214', 'timeswork555@gmail.com', '', 'Active', '4', 'KASKI', 'Pokhara Lekhnath', '', '0.00000000', '0.0000'),
(127, 148, 'SIDDHARTHA ORIENTATION AND TRAINING CENTER PVT.LTD.', 'Kaski pokharan.p. 9', '9851084526', 'siddhartha.orientation@gmail.com', '', 'Active', '4', 'KASKI', 'Pokhara Lekhnath', '', '0.00000000', '0.0000'),
(73, 80, 'MAHAKALI ORIENTATION AND SKILL DEVELOPMENT PVT.LTD.', 'Kathmandu Basundhara 7', '01-4116605', '', '', '', '3', 'KATHMANDU', 'Tokha', '', NULL, NULL),
(96, 109, 'INDRENI LANGUAGE CENTER PVT. LTD.', 'Kathmandu dhapasi 8', '4359011', 'indreniltc08@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Tokha', '', '0.00000000', '0.0000'),
(11, 8, 'Vision Orientation Service Pvt. Ltd.', 'Kathmandu dhapasi 9', '4365986', 'sosphurwa@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Tokha', 'Done', '27.74031073', '85.3268'),
(21, 18, 'OCEAN TECHNICAL INSTITUTE PVT. LTD.', 'Kathmandu dhapasi 9', '4353115', 'dipendra.ocean@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Tokha', '', '0.00000000', '0.0000'),
(9, 5, 'UNIQUE TECHNICAL TRAINING CENTER', 'Kathmandu dhapasi 9', '4386951', 'uniquettc@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Tokha', '', '0.00000000', '0.0000'),
(100, 114, 'MELISHA EDUCATION FOUNDETION PVT. LTD.', 'Kathmandu dhapasi 9 Basundhara', '01-4384372', 'melishaedu@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Tokha', '', '0.00000000', '0.0000'),
(82, 91, 'MOUNT EVEREST TECHNICAL INSTUTIUTE PRIVATE LIMITED', 'Kathmandu gaushala 9', '9851005185', 'mounteverest91@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'kathmandu', NULL, NULL),
(86, 95, 'J B R ORIENTATION CENTRE', 'Kathmandu gaushala 9', '14112644', 'jbrorientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(93, 106, 'JALPADEVI TRAINING INSTITUTE', 'Kathmandu gaushala 9', '4112581', 'jalpadevi2013@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(88, 98, 'PRASHARMSA TRAINING CENTER PVT.LTD', 'Kathmandu gonggabu 29 samakhushi', '014351143/9851015240', 'orientation.prashamsa@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(63, 67, 'Tower Info Foreign Job Orientation Pvt. Ltd.', 'Kathmandu gonggabu 3 samakhusi', '4363875', 'tower.orientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(8, 3, 'SAMIKSHYA ORIENTATION CENTER PVT.LTD', 'Kathmandu Kathmandu N.P. 0', '212803', 'samiksha.orientation.center@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(15, 12, 'SAMYOG TRAINING INSTITUTE PVT.LTD', 'Kathmandu Kathmandu N.P. 0 0', '0', 'samyoginstitute@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(42, 44, 'Cosmic Training Centre Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 10', '9851057199/9801057199', 'cosmictrainingcenter2071@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Active', '27.70177736', '85.3513'),
(25, 22, 'GLOBAL CAREERS PVT.LTD.', 'Kathmandu Kathmandu N.P. 10 BANESHWOR', '12094200', 'gc.globalcareers@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(24, 21, 'HAMRO SHIKSHA TALIM TATHA PARMARSHA PVT.LTD.', 'Kathmandu Kathmandu N.P. 10 bijulibajar', '4784035', 'hamroshiksha958@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(20, 17, 'NATIONAL FOREIGN TRAINING &PARAMERS DEPARTMENT PVT .LTD.', 'Kathmandu Kathmandu N.P. 10 buddhanagar', '4785049', 'nationalbaidesiktalim@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Active', '27.68435464', '85.3343'),
(106, 123, 'TEMPLE ORIENTATION CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 10 Buddhanagar', '4783177/4782042', 'templeorientationcenter@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'vist our  orientation', '27.70199465', '85.3508'),
(23, 20, 'WORLD WIDE TRAINERS PVT.LTD.', 'Kathmandu Kathmandu N.P. 11', '4218371', 'trainers.worldwide20@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(43, 45, 'T.S.TRANING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 11', '4228913', 'tscentre1@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(95, 108, 'PACHHIMANCHAL MULTIPAL TRANING CENTER PVT. LTD', 'Kathmandu Kathmandu N.P. 13', '4275818', 'pmtc.orientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(10, 7, 'ALLIANCE TECHNICAL CONSULT PVT.LTD.', 'Kathmandu Kathmandu N.P. 16', '4484846', 'alliancetechnical07@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'basundhara 3 ktm', '27.70298374', '85.3198'),
(53, 57, 'Bright Orientation Center Pvt. Ltd.(NEPAL FOREIGN EMPLOYMENT MANAGEMENT INSTITUTE PVT.LTD.)', 'Kathmandu Kathmandu N.P. 16', '6202664', 'brightorientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(101, 116, 'KATHMANDU ORIENTATION PVT. LTD.', 'Kathmandu Kathmandu N.P. 16 Basantanagar', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(41, 41, 'WORK FORCE ORIENTATION GROUP PVT.LTD.', 'Kathmandu Kathmandu N.P. 2', '4412587', 'managementforcework20@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(107, 124, 'GENUINE ORIENTATION TRAINING & RESEARCH CENTER', 'Kathmandu Kathmandu N.P. 2 Gairidhara', '01-4004770/9851064896', 'genuine.otrc@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(50, 53, 'GOLDEN MANAGEMENT SERVICES PVT.LTD.', 'Kathmandu Kathmandu N.P. 26', '2111043', 'goldenmgmts@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Done', '27.73044421', '85.3163'),
(81, 90, 'NICD TRAINING PVT.LTD', 'Kathmandu Kathmandu N.P. 26 Samakhushi', '14357754', 'nicd.tot@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(19, 16, 'JOB ORIENTED TRAINING INSTITUTE PVT LTD', 'Kathmandu Kathmandu N.P. 29 ranibari', '4384998', 'jobtraining7@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(68, 73, 'H.R.MATTERS RESEARCH PVT. LTD.', 'Kathmandu Kathmandu N.P. 29 Samakhushi', '1-4382507/ 9841329932/9813865203', 'hrmatters.edu@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Samakhushi', '27.73269844', '85.3162'),
(72, 79, 'KARMASHIL TRAINING CENRER PVT. LTD', 'Kathmandu Kathmandu N.P. 29 Samakhushi', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(84, 93, 'ASLESHA INSTITUTE FOR PROFASSION DEVELOPMENT PVT.LTD', 'Kathmandu Kathmandu N.P. 29 Samakhushi', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(31, 30, 'JUCHHE MULTIPURPOSE TRAINING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 29 samakhusi', '9841606471/9851001600', 'juchhemtc@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'b', '27.67253287', '85.3409'),
(46, 49, 'INSTITUTE FOR UNIVERSAL EDUCATION AND TRAINING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 29 samakhusi', '4021005', 'inistituteforuniversalore@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(87, 96, 'ANUSHREE MULTIPORPOSE TRENING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 29 SAMAKHUSI', '4360327', 'orientation.anushree@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'samakhushi', '27.95309026', '85.2795'),
(6, 1, 'SUNGABHA ORIENTATION TRAINING CENTER.PVT.LTD.', 'Kathmandu Kathmandu N.P. 3 basundhara', '2082006/9851078476', 'sungavaotc@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'sungava  orientation training', '27.73911070', '85.3305'),
(28, 27, 'BAGA BHAIRAB TRAINING INSTITUTE PVT.LTD', 'Kathmandu Kathmandu N.P. 3 Basundhara', '014016587/9851186062', 'bagbhairab@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(78, 87, 'PEOPLES ORIENTATION PVT.LTD', 'Kathmandu Kathmandu N.P. 3 BASUNDHARA', '01-4385660', 'peoplesorientation87@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 's', '27.73863946', '85.3254'),
(16, 13, 'BIGKIRAN ASSOCIATES PVT LTD', 'Kathmandu Kathmandu N.P. 3 chakrapath', '4380986/9851087778', 'bigkirana@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Color bank House', '27.67603061', '85.3159'),
(74, 83, 'R. R. ORIENTATION AND TRANING CENTRE PVT LTD.', 'Kathmandu Kathmandu N.P. 3 Chakrapath Chowk', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(26, 23, 'EVEREST TRAINING & CONSULTANCY PVT.LTD.', 'Kathmandu Kathmandu N.P. 3 gongabu', '4381558', 'everesttraining373@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(103, 119, 'PATHIBHARA ORIENTATION & TRAINING CENTRE PVT. LTD.', 'Kathmandu Kathmandu N.P. 3 Maharajgunj', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(83, 92, 'AGNI ORIENTATION AND TRANING CENTER PVT.LTD', 'Kathmandu Kathmandu N.P. 3 Narayan Gopal Tower 6th floor', '14016769', 'agniorientation65@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(51, 54, 'UNIVERSAL TECHNICAL TRAINING&ORIENTATION CENTER PVT.LTD', 'Kathmandu Kathmandu N.P. 3 pipalbot', '4650797', 'universalktm@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'kapanmarg Maharajgunj chakrapa', '27.74096000', '85.3413'),
(85, 94, 'LG MULTIPLE TRANING CENTER PVT. LTD', 'Kathmandu Kathmandu N.P. 3 Samakhusi', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(76, 85, 'KENTECH INTERNATIONAL INSTITUTE PVT. LTD', 'Kathmandu Kathmandu N.P. 32 Koteshwor', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(80, 89, 'GORKHA EDUCATIONAL TRANING CENTRE PVT. LTD', 'Kathmandu Kathmandu N.P. 33 MAITIDEVI', '01-4420577/9851075274', 'getc.edu2013@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '27.70298374', '85.3308'),
(29, 28, 'ADVANCE ORIENTATION TRAINING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 33 ratopul', '4478455/2071266', 'orientationadvance@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(37, 37, 'Rajnish Training Consultant', 'Kathmandu Kathmandu N.P. 34', '01-4365183/9841626352', 'rajneshtrainingconsultant5@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'orientation', '27.73444339', '85.3116'),
(55, 59, 'MALIKA INSTITUTE OF TRAINING & DEVLOPMENT PVT. LTD.', 'Kathmandu Kathmandu N.P. 34', '4111974', 'malikaitad66@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(57, 61, 'Chetana Orientation Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 34', '4620477', 'chetanaorientation123@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'chetana orientation', '27.44004047', '85.3638'),
(58, 62, 'Lumbini Multiple Prashikshan Kendra Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 34', '9801073761/9851004815', 'lumbinimultiple@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(13, 10, 'ORIENT TECHNICAL INSTITUTE & CONSULTANCY', 'Kathmandu Kathmandu N.P. 34  baneshwor', '2041350', 'otic2071@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(90, 100, 'FACE TO FACE ORIENTATION & TRAINING PVT. LTD.', 'Kathmandu Kathmandu N.P. 34 Baneshwr', '9841202796', 'face2faceorientation22@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(66, 71, 'AXIS CONSULTANCY PVT. LTD', 'Kathmandu Kathmandu N.P. 34 Bhimsengola', '2043888/4487320', 'axis2016@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(35, 34, 'EMPLOYMENT TRAINING & ORIENTATION CENTER PVT LTD', 'Kathmandu Kathmandu N.P. 34 milanchowk', '6222121', 'orientation.etoc@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(32, 31, 'RAJ FOREIGN EMPLOYEMNT', 'Kathmandu Kathmandu N.P. 34 Naya baneswor', '4461624', 'rajorientation11@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(147, 999, 'FOREIGN EMPLOYMENT PROMOTION BOARD', 'Kathmandu Kathmandu N.P. 34 New Baneshwor', '01-4105058', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(34, 33, 'S&S TRAINING INSTITUTE PVT.LTD.', 'Kathmandu Kathmandu N.P. 34 new basneswor', '4104092', 'sandschandra25@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(33, 32, 'SANGAM MANAGEMENT SERVICE PVT.LTD.', 'Kathmandu Kathmandu N.P. 34 tinkune', '411646/4117505', 'sangammgmt5@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'SANGAM MGMT PVT LTD', '27.68708365', '85.3461'),
(62, 66, 'United Institute Of Training And Research Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 35', '9841492118', 'unitedort02@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(18, 15, 'INTERNATIONAL TRAINING CENTER.PVT.LTD.', 'Kathmandu Kathmandu N.P. 35 Swoyambhu', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(47, 50, 'FAR WEST MULTI SKILLS PVT.LTD.', 'Kathmandu Kathmandu N.P. 35 TINKUNE', '14117581', 'farwestmultiskills@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'far west', '27.68799925', '85.3502'),
(27, 26, 'COSMOGRAPHY TRAINING & RESEARCH CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 4', '9851079276', 'cosmogtr26@gmail.com', '', 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(61, 65, 'Welcome Orientation Center Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 4', '01-4371278', 'welcomeort@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(92, 105, 'SYNERGY EMPLOYMENT PVT. LTD', 'Kathmandu Kathmandu N.P. 4', '4370664', 'synergy145@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'synergy', '27.73207524', '85.3447'),
(48, 51, 'SAMIK ORIENTATION & TRAINING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 4 bansbari', '4379011', 'samikorientation200@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(12, 9, 'S.V.CONSULTANCY PVT.LTD.', 'Kathmandu Kathmandu N.P. 4 Bashidhar Marg', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(104, 120, 'SUCCESS ORIENTATION & TRINING CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 4 basundhara', '014382565/9851197707', 'successorientation7@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(108, 126, 'NEPAL MANPOWER TRAINING ACADEMY', 'Kathmandu Kathmandu N.P. 4 Furtiman Marga', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(36, 35, 'SHINING MOON TRAINING CENTER PVT.LTD.', 'Kathmandu Kathmandu N.P. 4 LAZIMPAT', '16914211', 'shiningmoon2009@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Active', '27.70223213', '85.3516'),
(49, 52, 'SKY BLUE WAY TECHNICAL INSTITUTE PVT.LTD.', 'Kathmandu Kathmandu N.P. 4 maharajgunj', '4370663/4377666', 'blueway0052@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'done', '27.73987986', '85.3386'),
(17, 14, 'MOONDROPS ORIENTATION &TRAINING CENTER PVT LTD', 'Kathmandu Kathmandu N.P. 4 maharajung', '4016185', 'moondropsorientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Done', '27.69557665', '85.3549'),
(94, 107, 'NEPAL CHHAHARI SKILL DEVELOPMENT PVT. LTD.', 'Kathmandu Kathmandu N.P. 5', '4383269', 'nepalchhahari5@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(7, 2, 'MERAPEAK MULTIPLE PVT.LTD.', 'Kathmandu Kathmandu N.P. 7 BATTISPUTALI', '014474351/2052512', 'merapeakmultiple@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(70, 75, 'ELITE EMPLOYMENT TRAINING CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 7 Chabahil', '014823489/9851059658', 'elitetraining75@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(40, 40, 'SIGNOR INTERNATIONAL TRAINING INSTITUTE P.LTD.', 'Kathmandu Kathmandu N.P. 9', '4475226/4475229/4475230', 'signoriti@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'In the same building of mother', '27.69587351', '85.3524'),
(59, 63, 'Alliance Orientation And Training Center Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 9', '9851072231', 'allianceorientation63@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(60, 64, 'Asian Orientation Training Center Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 9', '01-4113579', 'asianorientationnp@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(64, 68, 'R. K. Orientation and Training Center Pvt. Ltd.', 'Kathmandu Kathmandu N.P. 9', '01-4472676', 'rkorientation001@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(45, 48, 'TASK TRAINING ASSOCIATION PRIVATE LTD', 'Kathmandu Kathmandu N.P. 9 24/4 J', '4110362', 'tasktraining.asso@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(75, 84, 'ELMIRA MULTIPLE TRAINING INSTITUTE PVT. LTD', 'Kathmandu Kathmandu N.P. 9 Basudhara Chowk', '014016788/9849843175', 'elmiramultiple@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(77, 86, 'MOUNTAIN GROUP TECHNICAL AND ORENTATION TRANING PVT.LTD', 'Kathmandu Kathmandu N.P. 9 Basudhara Chowk', '014383983/ 9851211188', 'mggrouptot88@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(39, 39, 'DOBHAN TECHNICAL TRINING CENTER', 'Kathmandu Kathmandu N.P. 9 basundhara', '4386952', 'dobhan39@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(105, 121, 'SAAN TRAINING CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 battisputali', '4496712', 'saantrainingcenter@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(89, 99, 'WORLD WIDE ORIENTATION & TRAINING CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 Chapalkarkhana Chowk', '014420616/9851186062', 'worldwideorientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 's', '27.73413239', '85.3418'),
(44, 47, 'R.M.INTERNATIONAL TRAINING CENTER PVT LTD', 'Kathmandu Kathmandu N.P. 9 dhapasi', '4352803/9851279279', 'rminternationalt@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Domestic Helper Training', NULL, NULL),
(56, 60, 'Paurakhi Srijansheel ship and Orientation Training Center', 'Kathmandu Kathmandu N.P. 9 Dhapasi-9', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(67, 72, 'ACCESS CAREER SOLUTION PVT LTD.', 'Kathmandu Kathmandu N.P. 9 Gaushala', '', '', '', '', '3', 'KATHMANDU', 'Kathmandu', '', NULL, NULL),
(54, 58, 'TRIPPLE S ORIENTATION & LANGUAGE TRAINING CENTRE PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 Manpower Bazar', '014482672/9851177074', 'tripplesssorientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(99, 112, 'WHITE HILLS RESEARCH AND CONSULTANCY P. LTD.', 'Kathmandu Kathmandu N.P. 9 Manpower Bazar', '014112813/9851050869', 'whitehillsresearch@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(71, 77, 'AKBAR TRAINING CENTER', 'Kathmandu Kathmandu N.P. 9 Manpower Bazzar', '9842396513', 'akbartcpl2014@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(102, 117, 'SAHALESH BABA ABHIMUKHIKARAN TALIM KENDRA PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 pingalasthan gausala', '01-4470263/ 9841626717', 'nabinkarki2045@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(109, 127, 'PASHUPATI ORIENTATION & TRANNING CENTER PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 Pingalsthan', '4113727', 'pashupatiorientation015@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(22, 19, 'AROUND THE WORD TTAINING CENTER PVT.LTDL', 'Kathmandu Kathmandu N.P. 9 Sinamangal', '411510', 'aroundtheworld891orientation@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(52, 55, 'LEE TRADE SCHOOL PVT.LTD.', 'Kathmandu Kathmandu N.P. 9 sinamangal', '4110512', 'leetrade.school@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(69, 74, 'SUKARMA INTERNATIONAL TRAINING INSTITUTE PVT.', 'Kathmandu Kathmandu N.P. 9 sinamangal', '4110599', 'sukarmatrainingcenter@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(91, 101, 'POWER CONSULTANCY PVT. LTD.', 'Kathmandu Kathmandu N.P. 9 Sinamangal', '9851050730', 'powr2070@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(97, 110, 'BUDDHA DARSHAN BYABASAYIK TALIM KENDRA', 'Kathmandu Kathmandu N.P. 9 Tokha', '', '', '', '', '3', 'KATHMANDU', 'Tokha', '', NULL, NULL),
(30, 29, 'BINDABASINI INTERNATIONAL INSTITUTE PVT.LTD', 'Kathmandu New Baneshwor 31 panchakumari marga', '014489147/9851003610', 'bindabasini.inst@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', '', '0.00000000', '0.0000'),
(79, 88, 'NISHAN MULTIPLE TRAINING PVT.LTD', 'Kathmandu Sinamangal 9 Kathmandu', '014493278/9851006158', 'nishan.training@gmail.com', NULL, 'Active', '3', 'KATHMANDU', 'Kathmandu', 'Active', '27.69515392', '85.3548'),
(65, 69, 'SOLID ORIENTATION PVT LTD.', 'Kathmandu Tokha Municipality-05', '', '', '', '', '3', 'KATHMANDU', 'Tokha', '', NULL, NULL),
(38, 38, 'BLUE SKY TRAINING RESEARCES & EDUCATIONAL CONSULTANCY  PVT LTD', 'Lalitpur lalitpurn.p. 10', '554669', 'blueskytrec@gmail.com', NULL, 'Active', '3', 'LALITPUR', 'Lalitpur', 'Blue Sky TrainingResearch And', '27.68535339', '85.3203'),
(14, 11, 'AVENUES TRAINING AND RESEARCH PVT LTD', 'Lalitpur lalitpurn.p. 10 kupandol', '5523771', 'avenuestraining@gmail.com', NULL, 'Active', '3', 'LALITPUR', 'Lalitpur', '', '0.00000000', '0.0000'),
(98, 111, 'VALLEY ORIENTATION AND MULTI TRINING CENTER PVT.LTD.', 'Lalitpur lalitpurn.p. 10 Kupandole', '01-4352811', 'vomtcnepal@gmail.com', NULL, 'Active', '3', 'LALITPUR', 'Lalitpur', 'Valley Orientation and Multi t', '27.73539420', '85.3181'),
(122, 143, 'BIRAT ORIENTATON AND TRAINING CENTER PVT.LTD', 'Morang Sundar Dulari 10 SUNDARDULARI', '25581949', 'biratoriantation@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 'S', '26.65770316', '87.2895'),
(137, 158, 'PURBAANCHAL ORIENTATION TRAINING CENTER PVT.LTD', 'Morang Sundar Dulari 11 SUNDAR DULARI', '9852054756', 'purbaanchalore@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 's', '26.65830723', '87.2877'),
(140, 161, 'RAUTA PROFESSIONAL SKILL DEVELOPMENT PVT.LTD.', 'Morang Sundar Dulari 6 MORANG', '25588803', 'rautattpes@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 'S', '26.65904193', '87.2897'),
(113, 132, 'PEACE NEPAL ORIENTATION CENTER PVT.LTD', 'Morang Sundarharaincha 11 baliya', '021-431089', 'peacenepalpvt@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 'S', '26.65808909', '87.2895'),
(128, 149, 'Idea Home Consultancy Pvt. Ltd.', 'Morang Sundarharaincha 4 Baliya', '021-431007', 'ideahomeitahari@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 's', '26.65810707', '87.2895'),
(139, 160, 'MUNAL ORIENTATION COMPANY PVT.LTD', 'Morang Sundarharaincha 4 baliya chock', '021-431103', 'munalorientation1@gmail.com', '', 'Active', '1', 'MORANG', 'Sundarharaicha', 'baliya chock morang', '26.65841510', '87.2893'),
(144, 167, 'All Asian Orientation Centre Pvt Ltd', 'Parsa birgunjn.p. 10 ghantaghar', '051-530771', 'alasianbng@gmail.com', 'http://www.alasianbng.com', 'Active', '2', 'PARSA', 'Birgunj', 's', '27.01573789', '84.8775'),
(133, 154, 'SUDAM ORIENTATION PVT.LTD', 'Rupandehi butawaln.p. 10 BUTWAL', '71547153', 'sudamorientation123@gmail.com', '', 'Active', '5', 'RUPANDEHI', 'Butwal', '', '0.00000000', '0.0000'),
(124, 145, 'Rudrawati orientation cernter pvt.ltd', 'Rupandehi butawaln.p. 11', '9851176314', 'rudrawatibtl@gmail.com', '', 'Active', '5', 'RUPANDEHI', 'Butwal', '', '0.00000000', '0.0000'),
(110, 128, 'GORAKHNATH INTERNATIONAL INSTITUTE PVT.LTD', 'Rupandehi butawaln.p. 7 BUTWAL', '9851003610', 'gorakhnathinstitute123@gmail.com', '', 'Active', '5', 'RUPANDEHI', 'Butwal', '', '0.00000000', '0.0000'),
(142, 165, 'Sudha Development and Orientation Pvt Ltd', 'Rupandehi butawaln.p. 8 Butwal Bazar', '9851070388', 'sudhaorientationbutwal@gmail.com', '', 'Active', '5', 'RUPANDEHI', 'Butwal', '', '0.00000000', '0.0000'),
(132, 153, 'LUCKY ORIENTATION AND TRAINING CENTRE PVT.LTD', 'Rupandehi siddharthnagarn.p. 8 BHAIRABAHA', '9849884065', 'dipendra1.lucky@gmail.com', '', 'Active', '5', 'RUPANDEHI', 'Siddharthanagar', '', '0.00000000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('fepbadmin@admin.com', '$2y$10$yHnRh/CPS8bSn.tu3suxIuWP4oM8HUfIHaJIfWATgCHRasPKz9Ld.', '2018-11-23 20:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maincategory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `maincategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 'product1', '0', 1, '2018-11-24 23:32:08', '2018-11-24 23:32:08'),
(2, 'Blog', '0', 1, '2018-11-25 11:40:00', '2018-11-25 11:40:00'),
(4, 'samachar', 'Blog', 1, '2018-11-25 11:41:12', '2018-11-26 21:08:18'),
(5, 'suchana', 'Blog', 1, '2018-11-25 11:42:05', '2018-11-26 21:08:44'),
(6, 'Other', '0', 1, '2018-11-25 11:54:43', '2018-11-25 11:54:43'),
(7, 'yojana', 'Other', 1, '2018-11-25 11:54:53', '2018-11-25 11:54:53'),
(8, 'karyakram', 'Other', 1, '2018-11-25 11:55:20', '2018-11-25 11:55:20'),
(9, 'audio', 'Other', 1, '2018-11-25 11:55:31', '2018-11-25 11:55:31'),
(11, 'press', 'Blog', 1, '2018-11-26 21:08:58', '2018-11-26 21:08:58'),
(12, 'bill', 'Blog', 1, '2018-11-26 21:09:05', '2018-11-26 21:09:05'),
(13, 'report', 'Other', 1, '2018-11-27 00:22:39', '2018-11-27 00:22:39'),
(14, 'video', 'Other', 1, '2018-11-27 00:25:23', '2018-11-27 00:25:23'),
(15, 'Album', '0', 1, '2018-12-01 23:43:39', '2018-12-01 23:43:39'),
(16, 'album1', 'Album', 1, '2018-12-01 23:43:49', '2018-12-01 23:43:49'),
(17, 'album2', 'Album', 1, '2018-12-01 23:51:15', '2018-12-01 23:51:15'),
(18, 'Service', '0', 1, '2018-12-03 23:35:18', '2018-12-03 23:35:18'),
(19, 'Board Welfare Work', 'Service', 1, '2018-12-03 23:40:35', '2018-12-03 23:40:35'),
(20, 'मृतक कामदारको नजिकका हकवालालाई आर्थिक सहायता प्र्रदान', 'Board Welfare Work', 1, '2018-12-03 23:42:13', '2018-12-03 23:42:13'),
(21, 'अङ्गभङ्ग वा अशक्त भई बिदेशमा काम गर्न नसकेको कारण नेपाल फर्की निवेदन गर्ने कामदारलाई आर्थिक सहायता प्रदान', 'Board Welfare Work', 1, '2018-12-03 23:42:27', '2018-12-03 23:42:27'),
(22, 'कामदारका परिवारालई स्वास्थ्य उपचार सहयोग', 'Board Welfare Work', 1, '2018-12-03 23:42:38', '2018-12-03 23:42:38'),
(23, 'शव व्यवस्थापन', 'Board Welfare Work', 1, '2018-12-03 23:42:47', '2018-12-03 23:42:47'),
(24, 'उद्धार सम्बन्धी कार्य', 'Board Welfare Work', 1, '2018-12-03 23:42:55', '2018-12-03 23:42:55'),
(25, 'आप्रवासी स्रोतकेन्द्र', 'Service', 1, '2018-12-03 23:43:27', '2018-12-03 23:43:27'),
(26, 'Free Quently Asked Question', 'Service', 1, '2018-12-03 23:43:37', '2018-12-03 23:43:37'),
(27, 'कामदारहरुले ध्यान दिनु पर्ने कुराहरु', 'Service', 1, '2018-12-03 23:43:45', '2018-12-03 23:43:45'),
(28, 'खुल्ला गरिएका देशहरु', 'Service', 1, '2018-12-03 23:43:54', '2018-12-03 23:43:54'),
(29, 'Helping Institution', 'Service', 1, '2018-12-03 23:44:01', '2018-12-03 23:44:01'),
(30, 'Skillfull training', '0', 1, '2018-12-12 05:50:31', '2018-12-12 05:50:31'),
(31, 'skilltraining 1', 'Skillfull training', 1, '2018-12-12 05:52:32', '2018-12-12 05:52:32'),
(32, 'skill training 2', 'Skillfull training', 1, '2018-12-12 11:29:04', '2018-12-12 11:29:04'),
(33, 'chart', '0', 1, '2018-12-12 23:41:46', '2018-12-12 23:41:46'),
(34, 'chart1', 'chart', 1, '2018-12-12 23:41:56', '2018-12-12 23:41:56'),
(35, 'chart2', 'chart', 1, '2018-12-12 23:42:03', '2018-12-12 23:42:03'),
(36, 'chart 3', 'chart', 1, '2018-12-13 01:07:06', '2018-12-13 01:07:06'),
(37, 'मृतक कामदारका परिवारलाई आर्थिक सहायता', 'chart', 1, '2018-12-13 01:14:26', '2018-12-13 01:14:26'),
(38, 'जानकारीमूलक सामग्री प्रकाशन एवं वितरण', 'Skillfull training', 1, '2018-12-13 01:40:58', '2018-12-13 01:40:58'),
(39, 'महिलाले अभिमुखीकरण तालिममा तिरेको शुल्क सोधभर्ना', 'Skillfull training', 1, '2018-12-13 01:41:10', '2018-12-13 01:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'province1', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(2, 'province2', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(3, 'province3', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(4, 'province4', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(5, 'province5', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(6, 'province6', '2018-12-10 04:09:10', '0000-00-00 00:00:00'),
(7, 'province7', '2018-12-10 04:09:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `second_table`
--

CREATE TABLE `second_table` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` text,
  `name` text,
  `phone` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tollfree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `logo`, `name`, `address`, `phone1`, `phone2`, `tollfree`, `fax`, `email`, `facebook_link`, `twitter_link`, `youtube_link`, `linkedin_link`, `website`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, '5c00c27a19e62_logo.png', 'वैदेशिक रोजगार प्रवर्द्धन बोर्ड नयाँबानेश्वर, काठमाडाैं', 'वैदेशिक रोजगार प्रवर्द्धन बोर्ड नयाँबानेश्वर, काठमाडाैं', '००१-४१०५०६८', '४१०५०६२', '१६६००१-५०००५', '०१४१०५०६१', 'info@fepb.gov.np', 'guhnage', 'me', 'badjijtep', 'bezu', 'http://fepb.gov.np', 2, 2, '2018-11-25 04:41:07', '2018-11-29 23:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `simples`
--

CREATE TABLE `simples` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `simples`
--

INSERT INTO `simples` (`id`, `title`, `category`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(9, 'infographic title here', 'infographic', 1, '<p><strong>this is the title for infographic comes from backend</strong></p>', 2, 2, '2018-12-02 05:27:35', '2018-12-06 03:56:52');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'kjshkjsd', '5bfab02f58162_29542849_2196988220318791_5257040631484924734_n.jpg', 1, 'laskgzndlgk', 2, 2, '2018-11-25 08:37:19', '2018-11-25 08:37:39'),
(4, 'second slider ho yo', '5bfca3257f2bc_well-images-for-wallpaper-desktop-24.jpg', 1, 'thsi si second slider&nbsp;', 2, NULL, '2018-11-26 20:06:33', '2018-11-26 20:06:33'),
(5, 'third slider', '5bfca34ad2888_3894133980_7e38f821fa.jpg', 1, 'this is third slider description', 2, NULL, '2018-11-26 20:07:10', '2018-11-26 20:07:10');

-- --------------------------------------------------------

--
-- Table structure for table `table_entries`
--

CREATE TABLE `table_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `manual_table_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_entries`
--

INSERT INTO `table_entries` (`id`, `table_field`, `field_type`, `category`, `status`, `created_by`, `updated_by`, `manual_table_id`, `created_at`, `updated_at`) VALUES
(1, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(2, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(3, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(4, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(5, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(6, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(7, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(8, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(9, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(10, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(11, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(12, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(13, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(14, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(15, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(16, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(17, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(18, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(19, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(20, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(21, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(22, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:23', '2018-12-10 05:35:23'),
(23, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:23', '2018-12-10 05:35:23'),
(24, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:24', '2018-12-10 05:35:24'),
(25, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(26, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(27, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(28, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:08', '2018-12-10 05:36:08'),
(29, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:09', '2018-12-10 05:36:09'),
(30, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:09', '2018-12-10 05:36:09'),
(31, 'name', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(32, 'email', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(33, 'phone', 'DOUBLE', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(34, 'address', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `category_id`, `created_at`, `updated_at`) VALUES
(2, 'this is traiing 2', NULL, NULL, NULL, NULL, 1, '<p>is is for training 2</p>', NULL, 2, 2, 38, '2018-12-12 11:29:40', '2018-12-13 01:55:40'),
(3, 'this is title for training 3', NULL, NULL, NULL, NULL, 1, '<p>thdsi dsfia dodsga&nbsp;</p>\r\n\r\n<ul>\r\n	<li>iukdsajf</li>\r\n	<li>dfsufkjds</li>\r\n	<li>dfsofilksdl</li>\r\n	<li>fdsiugdfgakdsf</li>\r\n	<li>easdviufusadjf</li>\r\n	<li>dsadwasfj</li>\r\n</ul>', NULL, 2, 2, 39, '2018-12-13 01:56:03', '2018-12-13 05:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'fepb admin', 'fepbadmin@admin.com', NULL, '$2y$10$NrdWx3ihpFCdrjxXHRhzSOiH72owuSfNyiA5uuvl4sNIMYkWcVyea', NULL, '2018-11-21 10:11:28', '2018-11-21 10:11:28'),
(2, 'admin', 'admin@admin.com', NULL, '$2y$10$Rb6yZBtNafOYTEfS8Bhsxe7qYi4F.8NtU.p7TurN0u/xavUmfRmfi', 'OwN4aZUwlf2S7mHYAH0FsuDXPXmFcSeyOCG2zd906apGPXndotdj2y9vQN6r', '2018-11-23 20:09:54', '2018-11-23 20:09:54');

-- --------------------------------------------------------

--
-- Table structure for table `yojana_karyakrams`
--

CREATE TABLE `yojana_karyakrams` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `yojana_karyakrams`
--

INSERT INTO `yojana_karyakrams` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'karyakram1', 'askd 32', NULL, '5bff8910167ee_Daily Report (1).pdf', 'karyakram', 1, NULL, NULL, 2, NULL, '2018-11-29 00:52:04', '2018-11-29 00:52:04'),
(2, 'yojana 1', 'aistugd 21', NULL, '5bff892210158_cvmilan-1-4.pdf', 'yojana', 1, NULL, NULL, 2, NULL, '2018-11-29 00:52:22', '2018-11-29 00:52:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abouts_created_by_foreign` (`created_by`),
  ADD KEY `abouts_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_created_by_foreign` (`created_by`),
  ADD KEY `banners_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_created_by_foreign` (`created_by`),
  ADD KEY `blogs_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_sachibalayas_created_by_foreign` (`created_by`),
  ADD KEY `board_sachibalayas_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `board_works`
--
ALTER TABLE `board_works`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_works_created_by_foreign` (`created_by`),
  ADD KEY `board_works_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `charts`
--
ALTER TABLE `charts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charts_created_by_foreign` (`created_by`),
  ADD KEY `charts_updated_by_foreign` (`updated_by`),
  ADD KEY `charts_category_id_foreign` (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `descriptions_created_by_foreign` (`created_by`),
  ADD KEY `descriptions_updated_by_foreign` (`updated_by`),
  ADD KEY `descriptions_category_id_foreign` (`category_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel_files`
--
ALTER TABLE `excel_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `extras_created_by_foreign` (`created_by`),
  ADD KEY `extras_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `first_table`
--
ALTER TABLE `first_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gapanapa`
--
ALTER TABLE `gapanapa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `generals_created_by_foreign` (`created_by`),
  ADD KEY `generals_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `important_links`
--
ALTER TABLE `important_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `important_links_created_by_foreign` (`created_by`),
  ADD KEY `important_links_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jana_gunasos`
--
ALTER TABLE `jana_gunasos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manual_tables`
--
ALTER TABLE `manual_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manual_tables_general_id_foreign` (`general_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_created_by_foreign` (`created_by`),
  ADD KEY `members_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_apps`
--
ALTER TABLE `mobile_apps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile_apps_created_by_foreign` (`created_by`),
  ADD KEY `mobile_apps_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `multimedia`
--
ALTER TABLE `multimedia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `multimedia_created_by_foreign` (`created_by`),
  ADD KEY `multimedia_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `orientation_done`
--
ALTER TABLE `orientation_done`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `second_table`
--
ALTER TABLE `second_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `setting_email_unique` (`email`),
  ADD KEY `setting_created_by_foreign` (`created_by`),
  ADD KEY `setting_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `simples`
--
ALTER TABLE `simples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `simples_created_by_foreign` (`created_by`),
  ADD KEY `simples_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_created_by_foreign` (`created_by`),
  ADD KEY `sliders_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `table_entries`
--
ALTER TABLE `table_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `table_entries_created_by_foreign` (`created_by`),
  ADD KEY `table_entries_updated_by_foreign` (`updated_by`),
  ADD KEY `table_entries_manual_table_id_foreign` (`manual_table_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainings_created_by_foreign` (`created_by`),
  ADD KEY `trainings_updated_by_foreign` (`updated_by`),
  ADD KEY `trainings_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `yojana_karyakrams`
--
ALTER TABLE `yojana_karyakrams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `yojana_karyakrams_created_by_foreign` (`created_by`),
  ADD KEY `yojana_karyakrams_updated_by_foreign` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `board_works`
--
ALTER TABLE `board_works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `charts`
--
ALTER TABLE `charts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `excel_files`
--
ALTER TABLE `excel_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `first_table`
--
ALTER TABLE `first_table`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gapanapa`
--
ALTER TABLE `gapanapa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=775;

--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `important_links`
--
ALTER TABLE `important_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `jana_gunasos`
--
ALTER TABLE `jana_gunasos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manual_tables`
--
ALTER TABLE `manual_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `mobile_apps`
--
ALTER TABLE `mobile_apps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orientation_done`
--
ALTER TABLE `orientation_done`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `second_table`
--
ALTER TABLE `second_table`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `simples`
--
ALTER TABLE `simples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_entries`
--
ALTER TABLE `table_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `yojana_karyakrams`
--
ALTER TABLE `yojana_karyakrams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `abouts`
--
ALTER TABLE `abouts`
  ADD CONSTRAINT `abouts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `abouts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `banners_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blogs_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  ADD CONSTRAINT `board_sachibalayas_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `board_sachibalayas_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_works`
--
ALTER TABLE `board_works`
  ADD CONSTRAINT `board_works_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `board_works_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `charts`
--
ALTER TABLE `charts`
  ADD CONSTRAINT `charts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `charts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `charts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `descriptions`
--
ALTER TABLE `descriptions`
  ADD CONSTRAINT `descriptions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `extras`
--
ALTER TABLE `extras`
  ADD CONSTRAINT `extras_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `extras_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `generals`
--
ALTER TABLE `generals`
  ADD CONSTRAINT `generals_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `generals_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `important_links`
--
ALTER TABLE `important_links`
  ADD CONSTRAINT `important_links_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `important_links_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `members_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
