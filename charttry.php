<!DOCTYPE html>
<html lang="en">

<!--head start-->

<head>

    <style>
        .canvasjs-chart-canvas {

            position: inherit !important;
            width: 550px !important;
            height: 350px !important;

            -webkit-tap-highlight-color: transparent;
            cursor: default;
        }
    </style>

    <!--meta tag start-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--title-->
    <title>वैदेशिक रोजगार प्रवर्द्धन बोर्ड</title>

    <!-- faveicon start   -->
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">

    <!-- stylesheet start -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <!-- Other CSS includes plugins - Cleanedup unnecessary CSS -->
    <!-- Chartist css -->
</head>
<!--head end-->

<body>


 
                                                    <div class="content-list">

                                                        <canvas id="myChart"></canvas>

                                                        <canvas id="myChart0"></canvas>
                                                    </div>

                                                    
                                                    <div class="content-list">
                                                        <canvas id="myChart1"></canvas>

                                                    </div>
                                             
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- Load d3.js and c3.js -->
    <script src="js/chart.min.js"></script>
    <script src="js/script.js"></script>
    <script>

        window.onload = function () {

           var ctx = document.getElementById("myChart").getContext('2d');

        window.onload = function () {  
            <?php $count=1 ?>

           for(i=0;i<=<?php echo $count?>;i++)
           {
            var chartid='myChart'+i; 
           var ctx = document.getElementById(chartid).getContext('2d');

            Chart.defaults.global.legend.display = false;
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["६५/६६", "६६/६७", " ६७/६८", "६८/६९", "६९/७०", "७०/७१", "जम्मा"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 3310],
                        backgroundColor: [
                            'purple',
                            'red',
                            'yellow',
                            'green',
                            'orange',
                            'blue',
                            'maroon'
                        ],
                        borderColor: [
                            'purple',
                            'red',
                            'yellow',
                            'green',
                            'orange',
                            'blue',
                            'maroon'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });



          
           } 
        //    var chartid='myChart'+i; 
        //    var ctx = document.getElementById("myChart").getContext('2d');
        //     Chart.defaults.global.legend.display = false;
        //     var myChart = new Chart(ctx, {
        //         type: 'bar',
        //         data: {
        //             labels: ["६५/६६", "६६/६७", " ६७/६८", "६८/६९", "६९/७०", "७०/७१", "जम्मा"],
        //             datasets: [{
        //                 label: '',
        //                 data: [90, 418, 549, 646, 727, 880, 3310],
        //                 backgroundColor: [
        //                     'purple',
        //                     'red',
        //                     'yellow',
        //                     'green',
        //                     'orange',
        //                     'blue',
        //                     'maroon'
        //                 ],
        //                 borderColor: [
        //                     'purple',
        //                     'red',
        //                     'yellow',
        //                     'green',
        //                     'orange',
        //                     'blue',
        //                     'maroon'
        //                 ],
        //                 borderWidth: 1
        //             }]
        //         },
        //         options: {
        //             scales: {
        //                 yAxes: [{
        //                     ticks: {
        //                         beginAtZero: true
        //                     }
        //                 }],
        //                 xAxes: [{
        //                     gridLines: {
        //                         display: false
        //                     }
        //                 }],
        //             }
        //         }
        //     });


           
            

        }
    </script>

</body>

</html>