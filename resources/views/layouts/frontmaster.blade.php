<!DOCTYPE html>
<html lang="en">
<!--head start-->
<head>
    <!--meta tag start-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--title-->
    {{--  <title>वैदेशिक रोजगार प्रवर्द्धन बोर्ड</title>  --}}
    <title>{{__("fepb")}}</title>

    <!-- faveicon start   -->
    <link rel="icon" type="image/png" href="{{asset('assets/front/images/favicon/favicon-32x32.png' )}}" sizes="32x32">

    <!-- stylesheet start -->

    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/bootstrap.min.css' )}}">

    {{--  <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/fontawesome.min.css' )}}">  --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/owl.carousel.min.css' )}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css' )}}">

    <!-- Other CSS includes plugins - Cleanedup unnecessary CSS -->
    <!-- Chartist css -->
    <link href="{{asset('assets/front/vendor/chartist/css/chartist.min.css' )}}" rel="stylesheet" />
    <link href="{{asset('assets/front/vendor/chartist/css/chartist-custom.css' )}}" rel="stylesheet" />
    <link href="{{asset('assets/front/css/c3.css' )}}" rel="stylesheet">
    @yield('css')
    @stack('css')
</head>
<!--head end-->

<body>
    @php
    $setting=App\Setting::orderBy('id','desc')->first();
    $links=App\ImportantLink::where('status',1)->orderBy('id','desc')->take(6)->get();

    @endphp

    <header class="site-header">
        <div class="container">
            <div id="master-head" class="master-head">
                <div class="page-toggle-button">
                    <div class="hamburger-box">
                        <span class="harmburger-inner"></span>
                    </div>
                </div>
                <div class="menu-wrap">
                    <a class="logo-link" href="{{route('front.index')}}">
                        {{--  <img src="{{asset('assets/front/images/logo.png' )}}" alt="logo">  --}}
                        <img src="{{asset('/setting_upload/'.$setting->logo)}}" alt="logo">
                    </a>
                    <div class="menu">
                        <span class="close-toggle">
                            <i class="fa fa-times-circle-o"></i>
                        </span>
                        <ul class="primary-menu">
                            <li class="menu-item-has-current">
                                {{--  <a href="{{route('front.index')}}">गृहपृष्ठ</a>  --}}
                                <a href="{{route('front.index')}}"> @lang('messages.Home')</a>

                            </li>
                            <li class="menu-item-has-children ">
                                {{--  <a href="{{route('front.about')}}#about">हाम्रो बारेमा </a>  --}}
                                <a href="{{route('front.about')}}#about"> {{__("messages.About")}} </a>
                                <ul>
                                    <li>
                                        <a href="{{route('front.about')}}#about">बोर्डको परिचय</a>
                                    </li>
                                    <li>
                                        <a href="{{route('front.about')}}#impwork">बोर्डको गठन</a>
                                    </li>
                                    <li class="">
                                        <a href="{{route('front.about')}}#workofwoda">बोर्डको काम</a>
                                    </li>
                                    <li class="">
                                        <a href="{{route('front.about')}}#sachiwalay">बोर्डको सचिवालय</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{route('front.yojanakaryakram')}}#yojana">योजना तथा कार्यक्रम</a>
                                <ul>

                                    <li>
                                        <a href="{{route('front.yojanakaryakram')}}#yojana">योजना</a>
                                    </li>
                                    <li>
                                        <a href="{{route('front.yojanakaryakram')}}#karyakram">कार्यक्रम</a>
                                    </li>

                                </ul>
                            </li>

                            <li class="menu-item-has-children">
                                <a href="{{route('front.media')}}#news">संचार</a>
                                <ul>
                                    <li>
                                        <a href="{{route('front.media')}}#news">सूचना तथा समाचारहरु</a>
                                    </li>
                                    <li>
                                        <a href="{{route('front.media')}}#press">प्रेस विज्ञप्ति</a>
                                    </li>
                                    <li class="">
                                        <a href="{{route('front.media')}}#image">तस्वीर संग्रह</a>
                                    </li>
                                    <li class="">
                                        <a href="{{route('front.media')}}#video">भिडीयो संग्रह</a>
                                    </li>
                                </ul>
                            </li>
                               <li class="menu-item-has-children">
                                <a href="{{route('front.download')}}#rules">डाउनलोड</a>
                                <ul>

                                    <li>
                                        <a href="{{route('front.download')}}#rules">ऐन तथा नियमावली </a>
                                    </li>
                                    <li>
                                            <a href="{{route('front.download')}}#publication">निर्देशिका/कार्यविधि/ मापदण्ड </a>
                                    </li>
                                    <li>
                                                <a href="{{route('front.download')}}#adhyan">अध्ययन प्रतिवेदन</a>
                                   </li>
                                    <li>
                                        <a href="{{route('front.download')}}#bulletin">बुलेटिन </a>
                                    </li>
                                    <li>
                                            <a href="{{route('front.download')}}#prakashan"> प्रकाशन</a>
                                        </li>
                                    <li>
                                        <a href="{{route('front.pratibedhantype')}}">प्रगति प्रतिवेदन</a>
                                    </li>

                                    {{--  <li>
                                        <a href="{{route('front.download')}}#publication">रिपोर्ट</a>
                                    </li>  --}}
                                 
                                    <li>
                                            <a href="{{route('front.download')}}#nebadan">निवेदन फारमहरु</a>
                                    </li>
                                    <li>
                                            <a href="{{route('front.download')}}#downloads">अन्य डाउनलोड</a>
                                    </li>

                                </ul>
                            </li>
                            <li>

                                <a href="#" class="gunaso-link">गुनासो दर्ता</a>

                                <a href="#" class="gunaso-link"> सुझाव तथा गुनासो</a>

                            </li>
                            <li>
                                <a href="{{route('front.contact')}}">सम्पर्क</a>
                            </li>

                            {{--  <li class="menu-item-has-children">
                                <a href="#">{{config('app.locale')}}</a>

                                <ul>

                                    <li>
                                        <a href="{{route('front.index','en')}}">En</a>
                                    </li>
                                    <li>
                                            <a href="{{route('front.index','hi')}}">Ne</a>
                                    </li>
                                </ul>

                            </li>  --}}

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </header>
    <div class="gunaso">
        <div class="popup-body">
            <div class="card-header">
                <h4>गुनासो दर्ता</h4>
            </div>
            <div class="content">
                <form method="post" action="{{route('front.gunasostore')}}"  enctype="multipart/form-data" id="valid_form" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>नाम</label>
                        <input type="text" placeholder="नाम" id="name" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>ठेगाना</label>
                        <input type="text" placeholder="ठेगाना" id="address" name="address" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>फोन न</label>
                        <input type="number" placeholder="फोन न" id="phone" name="phone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>इमेल</label>
                        <input type="email" placeholder="इमेल" id="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>समस्या</label>
                        <textarea type="text" placeholder="समस्या" id="message" name="message" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>फाइल अपलोड गर्नुहोस्</label>
                        <input type="file" id="image" name="image">
                    </div>
                    <div class="read-btn">
                        <button type="submit" class="btn drone-btn">Submit</button>
                    </div>
                </form>
            </div>
            <span class="close-icon gunaso-close"><i class="fa fa-close"></i></span>
        </div>
    </div>
    @yield('content')

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <h4>सम्पर्क-ठेगाना</h4>
                        <div class="content">
                            <address>
                                <p>{{$setting->name}}</p>
                            </address>
                            <address>
                                <p>फोन नं. {{$setting->phone1}},{{$setting->phone2}}</p>
                                <p>फ्याक्स नं. {{$setting->fax}}</p>
                                <p>र्इमेलः-{{$setting->email}}</p>
                                <p>टोल फ्रि नं‌‌. {{$setting->tollfree}}</p>
                                <p>वेवसाइटः- {{$setting->website}}</p>
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <h4>सामाजिक संजाल</h4>
                        <div class="content">
                            <div class="footer-social">
                                <a href="{{$setting->twitter_link}}" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="{{$setting->youtube_link}}" target="_blank">
                                    <i class="fa fa-youtube"></i>
                                </a>
                                <a href="{{$setting->facebook_link}}" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </div>
                            <form>
                                <div class="input-group">
                                    <input type="text" placeholder="Your Email address..." class="form-control">
                                    <button type="submit">Subscribe</button>
                                </div>
                            </form>
                            <div class="visited-site">
                                <ul class="footer-link">
                                    <li>
                                        <a href="#">Total visited:<span>
                                                <!-- Start of CuterCounter Code -->
                                                <a href="http://www.cutercounter.com/" target="_blank"><img src="http://www.cutercounter.com/hit.php?id=gmfnoaa&nd=6&style=78" border="0" alt="visitor counter"></a>
                                                <!-- End of CuterCounter Code -->

                                        </span></a>
                                    </li>

                                </ul>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget mrt">
                        <h4>महत्वपूर्ण लिङ्कहरु</h4>
                        <div class="content">
                            <ul class="footer-link">
                                @foreach ($links as $item)
                                <li>
                                        <a href="{{$item->link}}" target="_blank">{{$item->title}}</a>
                                    </li>
                                

                                @endforeach
                                <li class="">
                                        <a href="{{route('front.chetanamulak')}}#implink">View All</a>
                                    </li>

                                {{--  <li>
                                    <a href="#">बोर्डको गठन</a>
                                </li>
                                <li class="">
                                    <a href="#">बोर्डको काम</a>
                                </li>
                                <li class="">
                                    <a href="#">बोर्डको सचिवालय</a>
                                </li>  --}}
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <div class="text">
                Copyright © 2018. वैदेशिक रोजगार प्रवर्द्धन बोर्ड
            </div>
        </div>
    </div>

    <!-- jquery library section  -->

    <script src="{{asset('assets/front/js/jquery-3.3.1.min.js' )}}"></script>
    <script src="{{asset('assets/front/js/popper.min.js' )}}"></script>
    <script src="{{asset('assets/front/js/bootstrap.min.js' )}}"></script>
    <script src="{{asset('assets/front/js/owl.carousel.min.js' )}}"></script>

    <script src="{{asset('assets/front/vendor/chartist/js/chartist.min.js' )}}"></script>
    <script src="{{asset('assets/front/vendor/chartist/js/custom/donut-chart2.js' )}}"></script>

    <!-- Load d3.js and c3.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.js"></script>
    <script src="{{asset('assets/front/js/c3.js' )}}"></script>

    <script src="{{asset('assets/front/js/script.js' )}}"></script>




    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    @yield('js')
    @stack('js')
</body>

</html>
