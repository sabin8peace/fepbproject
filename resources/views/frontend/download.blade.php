@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/publication.css')}}">

@endpush
@push('js')
<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='rules')
        {

            $('#rules').addClass('in active');
            $('#rules1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='bulletin')
        {
            $('#bulletin').addClass('in active');
            $('#bulletin1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='prakashan')
        {
            $('#prakashan').addClass('in active');
            $('#prakashan1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='pragati')
        {
            $('#pragati').addClass('in active');
            $('#pragati1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='publication')
        {
            $('#publication').addClass('in active');
            $('#publication1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='downloads')
        {
            $('#downloads').addClass('in active');
            $('#downloads1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='nebadan')
        {
            $('#nebadan').addClass('in active');
            $('#nebadan1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='adhyan')
        {
            $('#adhyan').addClass('in active');
            $('#adhyan1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }

        })
</script>

@endpush


@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}
        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '>'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="side-menu">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " id="rules1" href="#rules" role="tab" data-toggle="tab">ऐन तथा नियमावली </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#bulletin" id="bulletin1" role="tab" data-toggle="tab">बुलेटिन </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#prakashan" id="prakashan1" role="tab" data-toggle="tab">प्रकाशन </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#adhyan" id="adhyan1" role="tab" data-toggle="tab">अध्ययन प्रतिवेदन</a>
                        </li>
                        <li class="nav-item">


                            <a class="nav-link" href="#bill" id="bill1" role="tab" data-toggle="tab">विल सार्वजनिकरण</a>
                        </li>
                        <li class="nav-item">

                            {{--  <a class="nav-link" href="#pragati" id="pragati1" role="tab" data-toggle="tab">प्रगति प्रतिवेदन</a>  --}}
                            <a class="nav-link" href="{{route('front.pratibedhantype')}}">प्रगति प्रतिवेदन</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#publication" id="publication1" role="tab" data-toggle="tab">निर्देशिका/कार्यविधि/ मापदण्ड</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#downloads" id="downloads1" role="tab" data-toggle="tab">अन्य डाउनलोड्स</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#nebadan" id="nebadan1" role="tab" data-toggle="tab">निवेदन फारमहरु</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="menu-content">
                    <div class="card">
                        <div class="card-body">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="publication">
                                    <div class="custom-accordion">
                                        <div class="card-header">
                                            <h4>निर्देशिका/कार्यविधि/ मापदण्ड</h4>
                                        </div>
                                        <section class="publicationWrapper">
                                            <div class="row">
                                                    @foreach ($publications as $item)
                                                    <div class="col-lg-4 col-md-6 thumb">
                                                            <div class="thumbBox">
                                                                <a class="thumbnail">
                                                                    <img class="img-thumbnail" src="{{asset('/extra_upload/'.$item->image)}}" alt="Another alt text">
                                                                </a>
                                                                <div class="content">
                                                                    <h3>{{$item->title}}</h3>
                                                                    <p>{{$item->date}}</p>
                                                                    <div class="read-btn">
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" download class="btn">डाउनलोड
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach

                                                {{--  <div class="col-lg-4 col-md-6 thumb">
                                                    <div class="thumbBox">
                                                        <a class="thumbnail">
                                                            <img class="img-thumbnail" src="https://images.pexels.com/photos/853168/pexels-photo-853168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                                                        </a>
                                                        <div class="content">
                                                            <h3>Social Cost Report</h3>
                                                            <p>2018-07-17</p>
                                                            <div class="read-btn">
                                                                <a href="../folder/Nepali Report.pdf" class="btn">Download
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 thumb">
                                                    <div class="thumbBox">
                                                        <a class="thumbnail">
                                                            <img class="img-thumbnail" src="https://images.pexels.com/photos/853168/pexels-photo-853168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                                                        </a>
                                                        <div class="content">
                                                            <h3>महिला हिंसा सम्बन्धी अध्ययन रिपोर्ट (नेपाली)</h3>
                                                            <p>2018-07-17</p>
                                                            <div class="read-btn">
                                                                <a href="../folder/Nepali Report.pdf" class="btn">Download
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 thumb">
                                                    <div class="thumbBox">
                                                        <a class="thumbnail">
                                                            <img class="img-thumbnail" src="https://images.pexels.com/photos/853168/pexels-photo-853168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                                                        </a>
                                                        <div class="content">
                                                            <h3>महिला हिंसा सम्बन्धी अध्ययन रिपोर्ट </h3>
                                                            <p>2018-07-17</p>
                                                            <div class="read-btn">
                                                                <a href="../folder/Nepali Report.pdf" class="btn">Download
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  --}}


                                            </div>


                                            <!-- <div class="modal fade infoModal" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="image-gallery-title"></h4>
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                                                                    </button>

                                                                    <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                        </section>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="pragati">
                                    <div class="card-header">
                                        <h4>प्रगति प्रतिवेदन
                                        </h4>
                                    </div>
                                    <section class="publicationWrapper">
                                        <div class="content-list">

                                            <div class="table table-responsive">

                                                <table class="table redtbl table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>क्र.स.</th>
                                                            <th>विवरण</th>
                                                            <th class="dt">प्रकाशित मिति</th>
                                                            <th class="btnHeader">डाउनलोड</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php($s=1)
                                                        @foreach ($pragatis as $item)
                                                        <tr>
                                                                <td>{{$s++}}</td>
                                                                <td>{{$item->title}}</td>
                                                                <td>{{$item->date}}</td>
                                                                <td class="action-td">
                                                                    <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>




                                                        @endforeach



                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                                <div role="tabpanel" class="tab-pane " id="bill">
                                        <div class="card-header">
                                            <h4>विल सार्वजनिकरण</h4>
                                        </div>
                                        <section class="publicationWrapper">
                                            <div class="content-list">
    
                                                <div class="table table-responsive">
    
                                                    <table class="table redtbl table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>क्र.स.</th>
                                                                <th>विवरण</th>
                                                                <th class="dt">प्रकाशित मिति</th>
                                                                <th class="btnHeader">डाउनलोड</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php($s=1)
                                                            @foreach ($bills as $item)
                                                            <tr>
                                                                    <td>{{$s++}}</td>
                                                                    <td>{{$item->title}}</td>
                                                                    <td>{{$item->date}}</td>
                                                                    <td class="action-td">
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
    
                                                                    </td>
                                                                </tr>
    
    
    
    
                                                            @endforeach
    
    
    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </section>
    
                                    </div>
                                <!-- this is going to be -->

                                <div role="tabpanel" class="tab-pane " id="rules">
                                    <div class="custom-accordion">
                                        <div class="card-header">
                                            <h4>ऐन तथा नियमावली </h4>
                                        </div>
                                        <section class="publicationWrapper">
                                            <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th width="5%">क्र.स.</th>
                                                                <th width="65%">विवरण</th>
                                                                <th lass="dt" width="15%">प्रकाशित मिति</th>
                                                                <th class="btnHeader" width="20%">डाउनलोड</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php($i=1)
                                                            @foreach ($ains as $item)
                                                            <tr>
                                                                    <td>{{$i++}}</td>
                                                                    <td>{{$item->title}}</td>
                                                                    <td>{{$item->date}}</td>
                                                                    <td class="action-td">
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>

                                                                    </td>
                                                                </tr>

                                                            @endforeach





                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                </div>



                                <!-- bulletin -->

                                <div role="tabpanel" class="tab-pane " id="bulletin">
                                    <div class="custom-accordion">
                                        <div class="card-header">
                                            <h4>बुलेटिन</h4>
                                        </div>
                                        <section class="publicationWrapper">
                                            <div class="row">

                                                @foreach ($bulletins as $item)
                                                <div class="col-lg-4 col-md-6 thumb">
                                                        <div class="thumbBox">
                                                            <a class="thumbnail">
                                                                <img class="img-thumbnail" src="{{asset('/extra_upload/'.$item->image)}}" alt="Another alt text">
                                                            </a>
                                                            <div class="content">
                                                                <h3>{{$item->title}}</h3>
                                                                <p>{{$item->date}}</p>
                                                                <div class="read-btn">
                                                                    <a href="{{asset('/extra_upload/'.$item->other_file)}}" download class="btn">डाउनलोड
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach




                                            </div>


                                                          </section>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane " id="prakashan">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>प्रकाशन</h4>
                                            </div>
                                            <section class="publicationWrapper">
                                                <div class="row">

                                                    @foreach ($prakashans as $item)
                                                    <div class="col-lg-4 col-md-6 thumb">
                                                            <div class="thumbBox">
                                                                <a class="thumbnail">
                                                                    <img class="img-thumbnail" src="{{asset('/extra_upload/'.$item->image)}}" alt="Another alt text">
                                                                </a>
                                                                <div class="content">
                                                                    <h3>{{$item->title}}</h3>
                                                                    <p>{{$item->date}}</p>
                                                                    <div class="read-btn">
                                                                        <a href="{{asset('/extra_upload/'.$item->other_file)}}" download class="btn">डाउनलोड
                                                                            <i class="fa fa-download"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    @endforeach


                                                    @endforeach





                                                </div>


                                                              </section>


                                                </div>


                                                              </section>


                                        </div>
                                    </div>




                                <div role="tabpanel" class="tab-pane " id="downloads">
                                    <div class="card-header">
                                        <h4>अन्य डाउनलोड्स
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        <div class="table table-responsive">

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>क्र.स.</th>
                                                        <th>विवरण</th>
                                                        <th class="dt">प्रकाशित मिति</th>
                                                        <th class="btnHeader">डाउनलोड</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php($j=1)
                                                    @foreach ($others as $item)
                                                    <tr>
                                                            <td>{{$j++}}</td>
                                                            <td>{{$item->title}}</td>

                                                            <td>{{$item->date}}</td>
                                                            <td class="action-td">
                                                                <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                            </td>
                                                        </tr>

                                                    @endforeach



                                                    {{--  <tr>
                                                        <td>2</td>
                                                        <td>अभिमुखिकरण तालिम प्रदान गर्न इजाजत प्राप्त संस्थाहरू
                                                        </td>
                                                        <td>Cha</td>
                                                        <td class="action-td">
                                                            <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                <i class="fa fa-download"></i>
                                                            </a>
                                                            <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                <i class="fa fa-eye"></i>
                                                            </a>

                                                        </td>
                                                    </tr>  --}}


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="nebadan">
                                    <div class="card-header">
                                        <h4>निवेदन फारमहरु
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        <div class="table table-responsive">

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>क्र.स.</th>
                                                        <th>विवरण</th>
                                                        <th class="dt">प्रकाशित मिति</th>
                                                        <th class="btnHeader">डाउनलोड</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                        @php($k=1)
                                                        @foreach ($nebadans as $item)
                                                        <tr>
                                                                <td>{{$k++}}</td>
                                                                <td>{{$item->title}}</td>

                                                                <td>{{$item->date}}</td>
                                                                <td class="action-td">
                                                                    <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>

                                                        @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane " id="adhyan">
                                    <div class="card-header">
                                        <h4>अध्ययन प्रतिवेदन
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        <div class="table table-responsive">

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>क्र.स.</th>
                                                        <th>विवरण</th>
                                                        <th class="dt">प्रकाशित मिति</th>
                                                        <th class="btnHeader">डाउनलोड</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php($a=1)
                                                    @foreach ($adhyans as $item)
                                                    <tr>
                                                            <td>{{$a++}}</td>
                                                            <td>{{$item->title}}</td>

                                                            <td>{{$item->date}}</td>
                                                            <td class="action-td">
                                                                <a href="{{asset('/extra_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a href="{{asset('/extra_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                            </td>
                                                        </tr>


                                                    @endforeach




                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane " id="regulation">
                                    <div class="card-header">
                                        <h4>नीति/कार्यविध
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        <div class="table">

                                            <table class="table table-responsive table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>क्र.स.</th>
                                                        <th>Name of Country</th>
                                                        <th>Embassy (Cha/Chaina)</th>
                                                        <th>Contact Person</th>
                                                        <th>Contact Details</th>
                                                        <th>डाउनलोड</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UAE</td>
                                                        <td>Cha</td>
                                                        <td>Information Officer</td>
                                                        <td>Phone/email</td>
                                                        <td class="action-td">
                                                            <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                <i class="fa fa-download"></i>
                                                            </a>
                                                            <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                <i class="fa fa-eye"></i>
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
