@extends('layouts.frontmaster')
@push('css')

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="mobileapp">
        <div class="mobile-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 text-center">
                            <div class="app-phone">
                                <img class="img-responsive" alt="codeforcore" title="mobile app" src="{{asset('/mobileApp_upload/'.$mobile->mobile_image)}}">
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="app-content">
                                <div class="text">
                                    <h3>{{$mobile->title}}</h3>
                                    <p>{!!$mobile->short_description!!}</p>
                                </div>
                                <div class="gplay-btn">
                                    {{--  <a target="_blank" href=" https://play.google.com/store/apps/details?id=com.Braindigit.DOFEApp" title="Google Play">  --}}
                                    <a target="_blank" href="{{$mobile->link}}" title="Google Play">
                                        <span class="play-icon"><img src="{{asset('assets/front/images/playstore.png')}}" style="max-width:21%;"></span>
                                        <div class="text">
                                            <span>Get it</span> Google Play
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




        <div class="inner-page">
            <div class="menu-content app-features">
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-4 col-lg-6 ">
                                    <div class="fnc-img">
                                        <img src="{{asset('/mobileApp_upload/'.$mobile->image)}}" alt="mobile app" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-xl-8 col-lg-6">
                                    <div class="card-header">
                                        <h4>एपका बिशेषताहरु</h4>
                                    </div>
                                    <div class="content-list">
                                        <ul>
                                            <li>
                                                    {!!$mobile->description!!}
                                            </li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
