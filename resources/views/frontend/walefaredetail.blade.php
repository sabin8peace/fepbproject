@extends('layouts.frontmaster')
@push('css')
    
@endpush
@push('js')
    
@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>

<div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="menu-content">
                        @foreach ($walefares as $item)
                        <div class="card">
                                <div class="card-body">
                                    <div class="tab-content">
    
                                            <div class="custom-accordion walefare">
                                                <div class="card-header">
                                                    <h4>{!!$item->maintitle!!}</h4>
                                                    <p>
                                                        {!!$item->title!!}
                                                       </p>
                                                </div>
                                                <ul>
                                                    <li>
                                                        <div class="acc-title">
                                                            <div class="atitle-content">
                                                                <h4>प्रकृया :-</h4>
                                                            </div>
                                                            <i class="fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="acc-content">
                                                            <div class="content-list">
                                                                {{--  <div class="process">
                                                                    <h5>रोगहरु :-</h5>
                                                                    <ul>
                                                                        <li>क्यान्सर</li>
                                                                        <li>मृगौला प्रत्यारोपण</li>
                                                                        <li>मुटुको शल्यकृया</li>
                                                                        <li>पार्किनसन</li>
                                                                        <li>अलजाइमर</li>
                                                                    </ul>
                                                                </div>  --}}
                                                                <ul>                                                                 
                                                                   {!!$item->short_description!!}
                                                                   
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="acc-title">
                                                            <div class="atitle-content">
                                                                <h4>निवेदनसाथ पेश गर्नुपर्ने काजगातहरु :-</h4>
                                                            </div>
                                                            <i class="fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="acc-content">
                                                            <div class="process">
                                                                <div class="content-list">
                                                                    <ul>
                                                                     {!!$item->description!!}
                                                                    </ul>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="process">
                                                   {!!$item->note!!}
                                                </div>
                                                {{--  <div class="process-note">
                                                    <span>नोट ः</span>निवेदनको ढाँचा बोर्ड सचिवालयमा उपलव्ध छ ।
                                                </div>  --}}
    
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                        @endforeach
                       
                    </div>

                </div>
            </div>
        </div>
    </div>
    
@endsection
