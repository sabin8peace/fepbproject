@extends('layouts.frontmaster')
@push('css')
<link href="{{asset('assets/front/css/aboutus.css')}}" rel="stylesheet">
@endpush
@push('js')
<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='about')
        {

            $('#about').addClass('in active');
            $('#about1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='impwork')
        {
            $('#impwork').addClass('in active');
            $('#impwork1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='workofwoda')
        {
            $('#workofwoda').addClass('in active');
            $('#workofwoda1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='sachiwalay')
        {
            $('#sachiwalay').addClass('in active');
            $('#sachiwalay1').addClass('in active');
            {{--  $("#teamnav").css("display", "inline");  --}}

        }
        if(check=='board-team')
        {
            $('#board-team').addClass('in active');
            $('#board-team1').addClass('in active');
            {{--  $("#teamnav").css("display", "inline");  --}}

        }

        })
</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}
        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '>'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="side-menu">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " href="#about" id="about1" role="tab" data-toggle="tab">बोर्डको परिचय</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#impwork" id="impwork1" role="tab" data-toggle="tab">बोर्डको गठन
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#workofwoda" id="workofwoda1" role="tab" data-toggle="tab">बोर्डको काम</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#sachiwalay" id="sachiwalay1" role="tab" data-toggle="tab">बोर्डको सचिवालय</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#board-team" id="board-team1" role="tab" data-toggle="tab">संगठन संरचना</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="menu-content">
                    <div class="card">
                        <div class="card-body">

                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane " id="about">
                                    <div class="card-header">
                                        <h4>बोर्डको परिचय
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        <ul>
                                            <li>
                                                {!!$about->description!!}

                                                {{--  नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार प्रवर्द्धन बोर्डको व्यवस्था
                                                गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ । माननीय श्रम तथा रोजगार मन्त्रीको
                                                अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा अध्यक्ष सहित
                                                २५ जना सदस्यहरु रहनु भएको छ । वैदेशिक रोजगार ऐन २०६४, बैदेशिक रोजगार नियमावली २०६४ वोर्डका निर्णय र निर्देशन बमोजिमका कार्यहरु वोर्डले सम्पादन गर्दछ । वोर्डवाट हालसम्म मूख्यरुपमा मृतकका परिवारलाई आर्थिक
                                                सहायता, बिरामी एंव अंगभंग भएकालाई उपचार खर्च, बिदेशमा अलपत्र परेका शव ल्याउन सहयोग, रोजगारदाता मुलुकमा आन्तरिक विद्रोह, आर्थिकमन्दीका कारण अलपत्र परेका कामदारलाई उद्धार गर्ने, जनचेतनामुलक कार्यक्रमहरु
                                                संचालन, बिदेशमा सुरक्षित गृह संचालन गर्ने कार्य गर्दै आएको छ । उपरोक्त कार्यहरु संचालन गर्न वोर्डको एक कल्याणकारी कोष रहने व्यवस्था छ । उक्त कोषमा मूख्य रुपमा बैदेशिक रोजगारमा जाने कामदारहरुले प्रचलित
                                                कानूनले तोके बमोजिम जम्मा गरेको रकम रहने व्यवस्था छ । मुलुकको कूल ग्राहस्थ उत्पादनको करीव २३ प्रतिशत हिस्सा ‌ओगटेको यस क्षेत्र अर्थतन्त्रको मेरुदण्डका रुपमा स्थापित हुदै गएको छ । तसर्थ बोर्ड यस क्षेत्रलाई
                                                सुरक्षित व्यवस्थित र मर्यादित बनाउन कामदार, व्यवसायी, नागरिक समाज र अन्य सबै सम्वद्ध निकायहरु वीच समन्वय, सहयोग र सहकार्य रहनु पर्ने दृष्टिकोण राख्दछ ।
                                              --}}
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane " id="impwork">
                                    <div class="card-header">
                                        <h4>बोर्डको गठन
                                        </h4>

                                    </div>
                                    <div class="content-list">
                                        {{--  <h5>
                                            {!!$gathan->description!!}
                                        </h5>  --}}






                                        <ul>
                                            @foreach($boardgathans as $gathan)
                                            <li>
                                                {!!$gathan->description!!}
                                                {{--  <i class="fa fa-angle-right"></i> {!!$gathan->description!!}  --}}
                                            </li>
                                            @endforeach
                                            {{--  <li>
                                                <i class="fa fa-angle-right"></i> श्रम तथा रोजगार राज्यमन्त्री/सहायक मन्त्री (उपाध्यक्ष)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सदस्य, राष्ट्रिय योजना आयोग -सम्बन्धित क्षेत्र हेर्ने (सदस्य)।
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सचिव, श्रम तथा रोजगार मन्त्रालय (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सचिव वा निजले तोकेको रा.प.प्रथम श्रेणीको प्रतिनिधि, गृह मन्त्रालय(सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सचिव वा निजले तोकेको रा.प.प्र. श्रेणीको प्रतिनिधि,परराष्ट्र मन्त्रालय(सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सचिव वा निजले तोकेको रा.प.प्रथम श्रेणीको प्रतिनिधि, अर्थ मन्त्रालय(सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सचिव वा निजले तोकेको रा.प.प्रथम श्रेणीको प्रतिनिधि, कानून तथा न्याय मन्त्रालय (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> बै सचिव वा निजले तोकेको रा.प.प्रथम श्रेणीको प्रतिनिधि, महिला, बालबालिका तथा समाजकल्याण मन्त्रालय (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> महानिर्देशक, वैदेशिक रोजगार बिभाग (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> प्रतिनिधि (प्रथम श्रेणी सरह), नेपाल राष्ट्र बैङ्क (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगारका विशेषज्ञहरुमध्येबाट नेपाल सरकारद्वारा मनोनीत एकजना महिला समेत दुई जना (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार व्यवसायी संघको अध्यक्ष र सोही संघले मनोनयन गरी पठाएको एकजना महिला प्रतिनिधि गरी दुई जना (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i>वैदेशिक रोजगारसम्बन्धी सीपमूलक र अभिमुखीकरण तालिम सञ्चालकहरुमध्येबाट नेपाल सरकारद्वारा मनोनीत एकजना महिला सहित दुई जना (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> स्वीकृति प्राप्त स्वास्थ्य संस्थाका कम्तीमा एम.बी.बी.एस.उत्तर्ण चिकित्सकमध्ये नेपाल सरकारद्वारा मनोनीत एक जना (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> नेपाल सरकारले तोकेका कुनै चारवटा मान्यताप्राप्त ट्रेड युनियन महासंघको अध्यक्ष वा यस्ता महासंघले मनोनयन गरी पठाएका एक-एकजना प्रतिनिधि गरी चार जना (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> प्रतिनिधि, नेपाल उद्योग वाणिज्य महासंघ (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> प्रतिनिधि, प्राविधिक शिक्षा तथा व्यावसायिक तालिम परिषद् (सदस्य)
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> कार्यकारी निर्देशक (सदस्य-सचिव)
                                            </li>  --}}
                                        </ul>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane " id="workofwoda">
                                    <div class="card-header">
                                        <h4>बोर्डको काम
                                        </h4>
                                    </div>
                                    <div class="content-list">
                                        {{--  <h5>
                                            {!!$boardwork->description!!}

                                        </h5>  --}}
                                        <ul>
                                                @foreach($boardworks as $work)
                                                <li>
                                                    {!!$work->description!!}
                                                    {{--  <i class="fa fa-angle-right"></i> {!!$work->description!!}  --}}
                                                </li>
                                                @endforeach
{{--
                                            <li>
                                                <i class="fa fa-angle-right"></i> नयाँ अन्तर्राष्ट्रिय श्रम बजारको खोजीका लागि अध्ययन गर्ने, गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार प्रवर्द्धनको लागि सूचना सङ्कलन, प्रशोधन र प्रकाशन गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार कल्याणकारी कोषको परिचालन गर्ने गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगारीमा गएका कामदारको हित संरक्षणसम्बन्धी आवश्यक कार्य गर्ने गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> विदेशबाट फर्की आएका कामदारको सीप, पूँजी र निजले सिकेको प्रविधि उपयोग गरी राष्ट्रहितमा लगाउने कार्यक्रम तर्जुमा, कार्यान्वयन, अनुगमन र मूल्याङ्कन गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> विभिन्न मुलुकसँग गरिने श्रम सम्झौतासम्बन्धी आवश्यक कार्य गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार अभिमुखीकरण तालिम दिने संस्था दर्ता गर्न योग्यता तोक्नुका साथै तालिमको पाठ्यक्रम तर्जुमा र स्वीकृत गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा कामदार र व्यवसायीको हकहितको संरक्षण गर्ने सम्बन्धमा अवलम्बन गर्नुपर्ने अल्पकालिन र दीर्घकालिन नीति
                                                तर्जुमा गरी नेपाल सरकार समक्ष पेश गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगार ऐनको कार्यान्वयन सम्बन्धमा समष्टीगत रुपमा अध्ययन गरीनेपाल सरकारलाई सुझाव दिने साथै कानूनको पुनरावलोकन गर्नुपर्ने देखिएमा सोको पुनरावलोकन गरी आवश्यक सुधारको
                                                लागि नेपाल सरकारलाई सुझाव दिने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> सेवा शुल्क तथा प्रवर्द्धनको खर्च निर्धारण गर्ने सम्बन्धमा नेपाल सरकारलाई परामर्श दिने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> नेपाली कामदारले विदेशमा कमाएको आय सरल र सुलभ तरिकाले नेपाल भित्र्याउने सम्बन्धमा आवश्यक प्रबन्ध मिलाउनको लागि नेपाल सरकारलाई परामर्श दिने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगारसम्बन्धी कानून विपरीत कुनै काम कारबाही कसैबाट भए गरेको पाइएमा आवश्यक कारबाहीको लागि सम्बन्धित निकायमा लेखी पठाउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i>इजाजतपत्रप्राप्त वैदेशिक रोजगार व्यवसायी, अभिमुखीकरण र सीपमूलक तालिम सञ्चालन गर्नेसंस्थाको अनुगमन गर्ने गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> खण्ड -ण) बमोजिम अनुगमन गर्दा वा गराउँदा कसैबाट ऐन वा यस ऐन अर्न्तर्गत बनेको नियम विपरीत कुनै काम भएको पाइएमा आवश्यक कारबाहीको लागि सम्बन्धित निकायमा लेखी पठाउने ।
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> आफूले सञ्चालन गरेको कार्यहरुको वाषिर्क प्रतिवेदन नेपाल सरकारसमक्ष पेश गर्ने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगारको अवसर तथा जोखिम वारे अध्ययन अनुसन्धान गर्ने, गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> प वैदेशिक रोजगारसम्बन्धी चेतनामूलक कार्यक्रमहरु रेडियो, टेलिभिजन तथा पत्रपत्रिकाद्वारा नियमित रुपमा प्रचार, प्रसार गर्ने, गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> कामदारको सामाजिक सुरक्षा रणनीति तथा कार्य योजना तयार गर्ने, गराउने,
                                            </li>
                                            <li>
                                                <i class="fa fa-angle-right"></i> वैदेशिक रोजगारमा गएका महिला कामदारका निमित्त सुरक्षित गृहको स्थापना गरी सञ्चालन गर्ने, गराउने ।
                                            </li>  --}}
                                        </ul>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane " id="sachiwalay">
                                    <div class="card-header">
                                        <h4>बोर्डको सचिवालय
                                        </h4>
                                    </div>
                                    <div class="content-list">

                                        <h5>वोर्डको सचिवालयको स्वीकृत दरवन्दी विवरण
                                        </h5>

                                        {{--  <p> {!!$sachibalaya->description!!}</p>  --}}


                                        <div class="table table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>क्र.स.</th>
                                                        <th>पद</th>
                                                        <th>संख्या</th>
                                                        <th>कैफियत</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php($i=1)
                                                    @foreach($boardsachabalayas as $sacbibalaya)

                                                    <tr>
                                                        <td>{{$i++}} </td>
                                                        <td>{{$sacbibalaya->post}}</td>
                                                        <td>{{$sacbibalaya->resource}}</td>
                                                        <td>{!!$sacbibalaya->description!!}</td>
                                                    </tr>
                                                    @endforeach
                                                    {{--  <tr>
                                                        <td>१ </td>
                                                        <td>सहायक निर्देशक</td>
                                                        <td>३</td>
                                                        <td>४</td>
                                                    </tr>  --}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="board-team">
                                    <div class="card-header">
                                        <h4>संगठन संरचना
                                        </h4>
                                    </div>
                                    <div class="content-list">

                                        <div class="table fepb-member">
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/2.jpg')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">श्री राजन प्रसाद श्रेष्ठ</a></h4>
                                                        <span>कार्यकारी निर्देशक</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- director -->
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">निर्देशक</a></h4>
                                                        <span>निर्देशक</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">निर्देशक नाम</a></h4>
                                                        <span>निर्देशक</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">निर्देशक नाम</a></h4>
                                                        <span>निर्देशक</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- ass. director -->
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">सहायक नाम</a></h4>
                                                        <span>सहायक निर्देशक</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="#">सहायक नाम</a></h4>
                                                        <span>सहायक निर्देशक</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">सहायक नाम</a></h4>
                                                        <span>सहायक निर्देशक</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- na su -->
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">ना.सु नाम</a></h4>
                                                        <span>ना.सु</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">ना.सु नाम</a></h4>
                                                        <span>ना.सु</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">ना.सु नाम</a></h4>
                                                        <span>ना.सु</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">ना.सु नाम</a></h4>
                                                        <span>ना.सु</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- accountant -->
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">लेखापाल नाम</a></h4>
                                                        <span>लेखापाल</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">अपरेटर नाम</a></h4>
                                                        <span>कम्प्युटर अपरेटर</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">चालक नाम</a></h4>
                                                        <span>सवारी चालक</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- helper -->
                                            <div class="member-wrap">
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="#">सहयोगी नाम</a></h4>
                                                        <span>कार्यालय सहयोगी</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">सहयोगी नाम</a></h4>
                                                        <span>कार्यालय सहयोगी</span>
                                                    </div>
                                                </div>
                                                <div class="member-content">
                                                    <div class="member-thumb">
                                                        <a href="profile.html"> <img src="{{asset('assets/front/images/team/4.png')}}" alt="member"></a>
                                                    </div>
                                                    <div class="content">
                                                        <h4><a href="profile.html">सहयोगी नाम</a></h4>
                                                        <span>कार्यालय सहयोगी</span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
