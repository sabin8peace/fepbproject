@extends('layouts.frontmaster')
@push('css')
<style>
    .canvasjs-chart-canvas {

        position: inherit !important;
        width: 550px !important;
        height: 350px !important;

        -webkit-tap-highlight-color: transparent;
        cursor: default;
    }
</style>


@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>



<script>
    var count=document.getElementById('count').value;



    window.onload = function () {
        for(i=0;i<count;i++)
    {
     //   @php
       // $label=array();
        //$count=count($charts);
        //for($i=0;$i<count($labeltmp);$i++)
//{
  //  for($j=0;$j<count($labeltmp[$i]);$j++)
   // {
        
    //    array_push($label,$labeltmp[$i][$j]);


   // }
//}
     
      //  @endphp

        var tmpname='tmpchart'+0;
        var ctx = document.getElementById(tmpname).getContext('2d');
        Chart.defaults.global.legend.display = false;
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {               
                labels: [ @php
                $label=array();
                $count=count($charts);
                for($i=0;$i<count($labeltmp);$i++)
        {
            for($j=0;$j<count($labeltmp[$i]);$j++)
            {
                
                array_push($label,$labeltmp[0][$j]);
        
        
            }
        }
                

                            for($j=0;$j<count($label);$j++)
                            {
                            echo "'$label[$j]',";
                            }
                            @endphp],
                // labels: ["६५/६६", "६६/६७", " ६७/६८", "६८/६९", "६९/७०", "७०/७१", "जम्मा"],
                datasets: [{
                    label: '',
                     data: [90, 418, 549, 646, 727, 880, 3310],
                   
                    backgroundColor: [
                        'purple',
                        'red',
                        'yellow',
                        'green',
                        'orange',
                        'blue',
                        'maroon'
                    ],
                    borderColor: [
                        'purple',
                        'red',
                        'yellow',
                        'green',
                        'orange',
                        'blue',
                        'maroon'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                    }],
                }
            }
        });

<script>
        window.onload = function () {

           var ctx = document.getElementById("myChart").getContext('2d');
            Chart.defaults.global.legend.display = false;
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310,90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var dpt = document.getElementById("countChart").getContext('2d');
            var countChart = new Chart(dpt, {
                type: 'bar',
               data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var rtn = document.getElementById("returnChart").getContext('2d');
            var countChart = new Chart(rtn, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var dbc = document.getElementById("deadbodyChart").getContext('2d');
            var countChart = new Chart(dbc, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });


            var ddc = document.getElementById("ddChart").getContext('2d');
            var countChart = new Chart(ddc, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

           var adtc = document.getElementById("admitChart").getContext('2d');
            var countChart = new Chart(adtc, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var ldtc = document.getElementById("ldtrainChart").getContext('2d');
            var countChart = new Chart(ldtc, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var rdo = document.getElementById("radioChart").getContext('2d');
            var countChart = new Chart(rdo, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var rdoprd = document.getElementById("radioprdChart").getContext('2d');
            var countChart = new Chart(rdoprd, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var rdoshow = document.getElementById("radioshowChart").getContext('2d');
            var countChart = new Chart(rdoshow, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var tvprd = document.getElementById("tvprdChart").getContext('2d');
            var countChart = new Chart(tvprd, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });

            var tvch = document.getElementById("tvchanelChart").getContext('2d');
            var countChart = new Chart(tvch, {
                type: 'bar',
                data: {
                    labels: ["वैशाख", "जेष्ठ", " अषाढ", "श्रावण", "भाद्र", "आश्विन", "कात्तिक", " मङसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"],
                    datasets: [{
                        label: '',
                        data: [90, 418, 549, 646, 727, 880, 310, 90, 418, 549, 646, 727, 880,],
                        backgroundColor: [
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple',
                            'purple'
                        ],
                        borderWidth: []
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }],
                    }
                }
            });
            


    

    }







    }
</script>


@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">

    <div class="container">
        <div class="ecotab">
            <div class="economy-nav">
                <ul>
                    <li class="economy-link current" data-tab="ecohelp">आर्थिक सहायता</li>

                    <li class="economy-link" data-tab="otherhelp">सीपमूलक तालिम</li>
                </ul>
            </div>
            <div class="tab-pane">
                <div class="eco-content current" id="ecohelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">

                                    @foreach ($charts as $k => $items)

                                                            @foreach ($items as $i => $item)

                                                            @if ($i==0)

                                                            <li class="nav-item">
                                                                @if ($k==0)
                                                            <a class="nav-link active" href="#chart{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                                        @else
                                                                        <a class="nav-link" href="#chart{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>

                                                                @endif

                                                            </li>
                                                            @endif


                                                            @endforeach



                                            @endforeach

                                    {{--  <li class="nav-item">
                                         <a class="nav-link" href="#worker" role="tab" data-toggle="tab">मृतक कामदारका परिवारलाई
                                            आर्थिक सहायता </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#handicapped" role="tab" data-toggle="tab">अङ्गभङ्गलाई आर्थिक सहायता</a>
                                    </li>  --}}

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                         <input type="number" id="count" value="{{count($charts)}}">
                                        @foreach ($charts as $k => $items)
                                            @foreach ($items as $i => $item)
                                            @if ($k==0&&$i==0)
                                        <div role="tabpanel" class="tab-pane  in active" id="chart{{$i}}">
                                            @else
                                        <div role="tabpanel" class="tab-pane" id="chart{{$i}}">
                                            @endif
                                                    <div class="card-header">
                                                        <h4>{{$item->title}}</h4>
                                                    </div>
                                                    <div class="content-list">
                                                    <canvas id="tmpchart{{$i}}"></canvas>
                                                    </div>
                                                </div>

                    <li class="economy-link" data-tab="otherhelp">अन्य कार्यहरु </li>
                </ul>
            </div>
            <div class="tab-pane">
                    <div class="eco-content current" id="ecohelp">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="side-menu">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#worker" role="tab" data-toggle="tab">मृतक कामदारका परिवारलाई आर्थिक सहायता </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#handicapped" role="tab" data-toggle="tab">अङ्गभङ्गलाई आर्थिक सहायता</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#returnchart" role="tab" data-toggle="tab">बैदेशिक रोजगारवाट विचल्ली भई फर्किएका कामदार
                                                    एवं तिनका परिवारलाई आर्थिक सहायता</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#deadbodychart" role="tab" data-toggle="tab">गन्तव्य मुलुकवाट शव झिकाएको</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#ddchart" role="tab" data-toggle="tab">मृतक कामदारको शव जिल्ला पुर्याउन</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#admitchart" role="tab" data-toggle="tab">महिलाले अभिमुखीकरण तालिममा तिरेको शुल्क
                                                    सोधभर्ना</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#radioprg" role="tab" data-toggle="tab">रेडियो नेपालवाट प्रशारण हुने रेडियो कार्यक्रम ‘सात समुन्द्र पारी’ उत्पादन र प्रशारण</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#radioprd" role="tab" data-toggle="tab">रेडियो जिङ्गल उत्पादन</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#radioshow" role="tab" data-toggle="tab">रेडियो जिङ्गल प्रशारण</a>
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a class="nav-link" href="#tvprd" role="tab" data-toggle="tab">टि.भि.स्पट उत्पादन</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#tvchanel" role="tab" data-toggle="tab">टि.भि. स्पट प्रसारण विभिन्न च्यानलवाट</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-8 col-lg-8">
                                    <div class="menu-content">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane  in active" id="worker">
                                                        <div class="card-header">
                                                            <h4>मृतक कामदारका परिवारलाई आर्थिक सहायता</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="myChart"></canvas>
                                                        </div>
                                                    </div>
                            
                                                    <div role="tabpanel" class="tab-pane" id="handicapped">
                                                        <div class="card-header">
                                                            <h4>अङ्गभङ्गलाई आर्थिक सहायता</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="countChart"></canvas>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="returnchart">
                                                        <div class="card-header">
                                                            <h4>बैदेशिक रोजगारवाट विचल्ली भई फर्किएका कामदार एवं तिनका परिवारलाई आर्थिक सहायता</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="returnChart"></canvas>
                            
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="deadbodychart">
                                                        <div class="card-header">
                                                            <h4>गन्तव्य मुलुकवाट शव झिकाएको</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="deadbodyChart"></canvas>
                            
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="ddchart">
                                                        <div class="card-header">
                                                            <h4>मृतक कामदारको शव जिल्ला पुर्याउन</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="ddChart"></canvas>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="admitchart">
                                                        <div class="card-header">
                                                            <h4>मृतक कामदारको शव जिल्ला पुर्याउन</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="admitChart"></canvas>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="ldtrain">
                                                        <div class="card-header">
                                                            <h4>महिलाले अभिमुखीकरण तालिममा तिरेको शुल्क
                                                            सोधभर्ना</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="ldtrainChart"></canvas>
                                                    
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="radioprg">
                                                        <div class="card-header">
                                                            <h4>रेडियो नेपालवाट प्रशारण हुने रेडियो कार्यक्रम ‘सात समुन्द्र पारी’ उत्पादन र प्रशारण</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="radioChart"></canvas>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="radioprd">
                                                        <div class="card-header">
                                                            <h4>रेडियो जिङ्गल उत्पादन</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="radioprdChart"></canvas>
                                                    
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="radioshow">
                                                        <div class="card-header">
                                                            <h4>रेडियो जिङ्गल प्रशारण</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="radioshowChart"></canvas>
                                                    
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="tvprd">
                                                        <div class="card-header">
                                                            <h4>टि.भि.स्पट उत्पादन</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="tvprdChart"></canvas>
                                                    
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="tvchanel">
                                                        <div class="card-header">
                                                            <h4>टि.भि. स्पट प्रसारण विभिन्न च्यानलवाट</h4>
                                                            <select class="custom-select bar-select">
                                                                <option selected="">२०७४</option>
                                                                <option>२०७४</option>
                                                                <option> २०७३ </option>
                                                                <option>२०७२</option>
                                                                <option>२०७१</option>
                                                                <option>२०७०</option>
                                                            </select>
                                                        </div>
                                                        <div class="content-list">
                                                            <canvas id="tvchanelChart"></canvas>
                                                    
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                            
                                            </div>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                        </div>


                      <div class="eco-content " id="otherhelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach ($trainings as $i=>$item)
                                    <li class="nav-item">
                                            @if ($i==0)
                                    <a class="nav-link active" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                    @else
                                                    <a class="nav-link" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>


                                            @endforeach
                                            @endforeach

                                            {{-- <div role="tabpanel" class="tab-pane" id="handicapped">
                                                <div class="card-header">
                                                    <h4>अङ्गभङ्गलाई आर्थिक सहायता</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="countChart"></canvas>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="returnchart">
                                                <div class="card-header">
                                                    <h4>बैदेशिक रोजगारवाट विचल्ली भई फर्किएका कामदार एवं तिनका परिवारलाई आर्थिक सहायता</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="returnChart"></canvas>

                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="deadbodychart">
                                                <div class="card-header">
                                                    <h4>गन्तव्य मुलुकवाट शव झिकाएको</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="deadbodyChart"></canvas>

                                                </div>
                                            </div> --}}

                                        </div>

                                    </div>
                                </div>
                            </div>

                                            @endif

                                        </li>
                                    @endforeach



                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach ($trainings as $i=>$item)
                                            @if($i==0)
                                                <div role="tabpanel" class="tab-pane in active" id="training{{$i}}">
                                                  @else
                                                  <div role="tabpanel" class="tab-pane" id="training{{$i}}">

                                                    @endif
                                                        <div class="card-header">
                                                            <h4>{{$item->title}}</h4>
                                                        </div>
                                                        <div class="content-list">
                                                            <ul>
                                                             {!!$item->description!!}
                                                            </ul>

                                                        </div>
                                                    </div>


                                            @endforeach


                        </div>
                    </div>
                </div>
                <div class="eco-content " id="otherhelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach ($trainings as $i=>$item)
                                    <li class="nav-item">
                                            @if ($i==0)
                                    <a class="nav-link active" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                    @else
                                                    <a class="nav-link" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>


                                            @endif

                                        </li>
                                    @endforeach



                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach ($trainings as $i=>$item)
                                            @if($i==0)
                                                <div role="tabpanel" class="tab-pane in active" id="training{{$i}}">
                                                  @else
                                                  <div role="tabpanel" class="tab-pane" id="training{{$i}}">

                                                    @endif
                                                        <div class="card-header">
                                                            <h4>{{$item->title}}</h4>
                                                        </div>
                                                        <div class="content-list">
                                                            <ul>
                                                             {!!$item->description!!}
                                                            </ul>

                                                        </div>
                                                    </div>


                                            @endforeach


                                        </div>


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
