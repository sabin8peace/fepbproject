@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/publication.css')}}">

@endpush
@push('js')
<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='karyakram')
        {

            $('#karyakram').addClass('in active');
            $('#karyakram1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='yojana')
        {
            $('#yojana').addClass('in active');
            $('#yojana1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }

        })
</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '>'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="side-menu">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link " href="#yojana" id="yojana1" role="tab" data-toggle="tab">योजना</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#karyakram" id="karyakram1" role="tab" data-toggle="tab">कार्यक्रम </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="menu-content">
                    <div class="card">
                        <div class="card-body">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane " id="yojana">
                                    <div class="custom-accordion">
                                        <div class="card-header">
                                            <h4>योजना </h4>
                                        </div>
                                        <section class="publicationWrapper">

                                            <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>क्र.स. </th>
                                                                <th>विवरण</th>
                                                                <th lass="dt">प्रकाशित मिति</th>
                                                                <th class="btnHeader">डाउनलोड</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php($i=1)
                                                            @foreach($yojanas as $item)
                                                            <tr>
                                                                <td>{{$i++}}</td>
                                                                <td>{{$item->title}}</td>
                                                                <td>{{$item->date}}</td>
                                                                <td class="action-td">
                                                                    <a href="{{asset('/yojanaKaryakram_upload/'.$item->other_file)}}" download target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="{{asset('/yojanaKaryakram_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>
                                                            @endforeach

                                                            {{--  <tr>
                                                                <td>2</td>
                                                                <td>अा.व.२०७३-०७४ काे वार्षिक प्रगती</td>
                                                                <td>8375</td>
                                                                <td class="action-td">
                                                                    <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>  --}}

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="karyakram">
                                    <div class="card-header">
                                        <h4>कार्यक्रम
                                        </h4>
                                    </div>
                                    <section class="publicationWrapper">
                                        <div class="content-list">

                                            <div class="table table-responsive">

                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>क्र.स.  </th>
                                                            <th>विवरण</th>
                                                            <th class="dt">प्रकाशित मिति</th>
                                                            <th class="btnHeader">डाउनलोड</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                            @php($i=1)
                                                            @foreach($karyakrams as $item)
                                                            <tr>
                                                                <td>{{$i++}}</td>
                                                                <td>{{$item->title}}</td>
                                                                <td>{{$item->date}}</td>
                                                                <td class="action-td">
                                                                    <a href="{{asset('/yojanaKaryakram_upload/'.$item->other_file)}}" download target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="{{asset('/yojanaKaryakram_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>
                                                            @endforeach

                                                        {{--  <tr>
                                                            <td>1</td>
                                                            <td>अा.व.२०७४-०७ काे वार्षिक कार्क्रम</td>
                                                            <td>26-11-2017</td>
                                                            <td class="action-td">
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>2</td>
                                                            <td> अा.व.२०७३-०७४ काे वार्षिक प्रगती</td>
                                                            <td>26-11-2018</td>
                                                            <td class="action-td">
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                            </td>
                                                        </tr>
  --}}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </section>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
