@extends('layouts.frontmaster')
@push('css')
<style>
    .canvasjs-chart-canvas {

        position: inherit !important;
        width: 550px !important;
        height: 350px !important;

        -webkit-tap-highlight-color: transparent;
        cursor: default;
    }
</style>


@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<script>
    var count=document.getElementById('count').value;



    window.onload = function () {
        <?php $count=count($charts) ?>
        for({{$i=0}};{{$i}}<{{$count}};{{$i++}})
    {
          

        var tmpname='tmpchart'+{{$i}};
        var ctx = document.getElementById(tmpname).getContext('2d');
        Chart.defaults.global.legend.display = false;
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {               
               
                 //labels: ["६५/६६", "६६/६७", " ६७/६८", "६८/६९", "६९/७०", "७०/७१", "जम्मा"], 
                 labels: [ @php                 
                 $label=array();                
             
             for($j=0;$j<count($labeltmp[$i]);$j++)
             {
                 
                array_push($label,$labeltmp[$i][$j]);
         
         
             }                                  
                              for($j=0;$j<count($label);$j++)
                             {
                             echo "'$label[$j]',";
                             }
                            
                             @endphp], 
 
                 datasets: [{
                    label: '',
                     data: [90, 418, 549, 646, 727, 880, 900],
                   
                    backgroundColor: [
                        'purple',
                        'red',
                        'yellow',
                        'green',
                        'orange',
                        'blue',
                        'maroon'
                    ],
                    borderColor: [
                        'purple',
                        'red',
                        'yellow',
                        'green',
                        'orange',
                        'blue',
                        'maroon'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                    }],
                }
            }
        });

    

    }

    






    }
</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">

    <div class="container">
        <div class="ecotab">
            <div class="economy-nav">
                <ul>
                    <li class="economy-link current" data-tab="ecohelp">आर्थिक सहायता</li>
                    <li class="economy-link" data-tab="otherhelp">सीपमूलक तालिम</li>
                </ul>
            </div>
            <div class="tab-pane">
                <div class="eco-content current" id="ecohelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">

                                    @foreach ($charts as $k => $items)

                                                            @foreach ($items as $i => $item)

                                                            @if ($i==0)

                                                            <li class="nav-item">
                                                                @if ($k==0)
                                                            <a class="nav-link active" href="#chart{{$k}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                                        @else
                                                                        <a class="nav-link" href="#chart{{$k}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>

                                                                @endif

                                                            </li>
                                                            @endif


                                                            @endforeach

                                            @endforeach

                                    {{--  <li class="nav-item">
                                         <a class="nav-link" href="#worker" role="tab" data-toggle="tab">मृतक कामदारका परिवारलाई
                                            आर्थिक सहायता </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#handicapped" role="tab" data-toggle="tab">अङ्गभङ्गलाई आर्थिक सहायता</a>
                                    </li>  --}}

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                         <input type="number" id="count" value="{{count($charts)}}">
                                        
                                    @foreach ($charts as $k => $items)

                                    @foreach ($items as $i => $item)

                                    @if ($i==0)                                  
                                          
                                   
                                        @if ($k==0)
                                        <div role="tabpanel" class="tab-pane  in active" id="chart{{$k}}">
                                                @else
                                                <div role="tabpanel" class="tab-pane" id="chart{{$k}}">

                                        @endif
                                        <div class="card-header">
                                                <h4>{{$item->title}}</h4>
                                            </div>
                                            <div class="content-list">
                                            <canvas id="tmpchart{{$k}}"></canvas>
                                            </div>
                                        </div>
                                    @endif


                                    @endforeach

                    @endforeach


                                            {{-- <div role="tabpanel" class="tab-pane" id="handicapped">
                                                <div class="card-header">
                                                    <h4>अङ्गभङ्गलाई आर्थिक सहायता</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="countChart"></canvas>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="returnchart">
                                                <div class="card-header">
                                                    <h4>बैदेशिक रोजगारवाट विचल्ली भई फर्किएका कामदार एवं तिनका परिवारलाई आर्थिक सहायता</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="returnChart"></canvas>

                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="deadbodychart">
                                                <div class="card-header">
                                                    <h4>गन्तव्य मुलुकवाट शव झिकाएको</h4>
                                                </div>
                                                <div class="content-list">
                                                    <canvas id="deadbodyChart"></canvas>

                                                </div>
                                            </div> --}}

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="eco-content " id="otherhelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach ($trainings as $i=>$item)
                                    <li class="nav-item">
                                            @if ($i==0)
                                    <a class="nav-link active" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                    @else
                                                    <a class="nav-link" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>

                                            @endif

                                        </li>
                                    @endforeach



                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach ($trainings as $i=>$item)
                                            @if($i==0)
                                                <div role="tabpanel" class="tab-pane in active" id="training{{$i}}">
                                                  @else
                                                  <div role="tabpanel" class="tab-pane" id="training{{$i}}">

                                                    @endif
                                                        <div class="card-header">
                                                            <h4>{{$item->title}}</h4>
                                                        </div>
                                                        <div class="content-list">
                                                            <ul>
                                                             {!!$item->description!!}
                                                            </ul>

                                                        </div>
                                                    </div>


                                            @endforeach


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
