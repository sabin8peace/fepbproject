@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/magnific-popup.css')}}">

@endpush
@push('js')
<script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    Chart.defaults.global.legend.display = false;
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["मृतकका परिवार", "अङ्गभङ्ग", " विचल्ली परिवार", "शुल्क सोधभना", "आर्थिक मन्दी", "शव पुर्याउन"],
            datasets: [{
                label: '',
                data: [10000, 3000, 4000, 5000, 7000, 8000],
                backgroundColor: [
                    'purple',
                    'red',
                    'yellow',
                    'green',
                    'orange',
                    'blue'
                ],
                borderColor: [
                    'purple',
                    'red',
                    'yellow',
                    'green',
                    'orange',
                    'blue'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    });
</script>

@endpush
@section('content')
{{--  <div class="load-popup">
    <div class="popup-body">
        <div class="card-header">
            <h4>{{$banner->title}}</h4><br>
            <p>{!!$banner->description!!}</p>
        </div>
        <div class="content">
            <img src="{{asset('/banner_upload/'.$banner->image)}}" alt="report">
        </div>
        <span class="close-icon"><i class="fa fa-close"></i></span>

    </div>
</div>
<div class="popup-icon">
    <span><i class="fa fa-reply"></i></span>
</div>  --}}


<!-- popup start -->
{{--  <div class="load-popup">
    <div class="popup-body">
        <div class="card-header">
            <h4>बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण</h4>
        </div>
        <div class="content">
            <a href="#"><img src="images/report2.jpg" alt="report"></a>
        </div>
        <span class="close-icon"><i class="fa fa-close"></i></span>
        <div class="popup-btn read-btn">
            <a href="#" class="btn">View all</a>
        </div>
        <div class="popup-btn read-btn">
            <a href="#" class="btn">Download</a>
        </div>

    </div>
</div>  --}}
<div class="load-popup">
    <div class="popup-body">
        <div class="card-header">
            <h4>{{$banner->title}}</h4><br>
            <p>{!!$banner->description!!}</p>
        </div>
        <div class="content">
            <img src="{{asset('/banner_upload/'.$banner->image)}}" alt="report">
        </div>
        <span class="close-icon"><i class="fa fa-close"></i></span>
        <div class="popup-btn read-btn">

            <a href="#" class="btn">View all</a>
        </div>
        <div class="popup-btn read-btn">
            <a href="#" class="btn">Download</a>
        </div>
    </div>

            <a href="{{route('front.media')}}#news" class="btn">View all</a>
        </div>
        <div class="popup-btn read-btn">
            <a href="{{asset('/banner_upload/'.$banner->image)}}" download class="btn">Download</a>
        </div>
    </div>
</div>
<!-- popup end -->



<!-- popup icon right side -->

<div class="popup-icon">
    <span><i class="fa fa-reply"></i></span>
>>>>>>> b7679d171b6ba59eb2b2aefe4cf3ba3bed1ee633
</div>
<!-- popup end -->



<!-- popup icon right side -->

<div class="popup-icon">
    <span><i class="fa fa-reply"></i></span>
</div>

<!-- gunaso popup -->

{{--  <div class="gunaso">
    <div class="popup-body">
        <div class="card-header">
            <h4>गुनासो दर्ता</h4>
        </div>
        <div class="content">
            <form method="post" action="{{route('front.gunasostore')}}"  enctype="multipart/form-data" id="valid_form" >
                {{ csrf_field() }}
                <div class="form-group">
                    <label>नाम</label>
                    <input type="text" placeholder="नाम" id="name" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>ठेगाना</label>
                    <input type="text" placeholder="ठेगाना" id="address" name="address" class="form-control">
                </div>
                <div class="form-group">
                    <label>फोन न</label>
                    <input type="number" placeholder="फोन न" id="phone" name="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label>इमेल</label>
                    <input type="email" placeholder="इमेल" id="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>समस्या</label>
                    <textarea type="text" placeholder="समस्या" id="message" name="message" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>फाइल अपलोड गर्नुहोस्</label>
                    <input type="file" id="image" name="image">
                </div>
                <div class="read-btn">
                    <button type="submit" class="btn drone-btn">Submit</button>
                </div>
            </form>
        </div>
        <span class="close-icon gunaso-close"><i class="fa fa-close"></i></span>
    </div>
</div>  --}}

<!-- popup icon right end -->

<!-- gunaso popup -->

{{--  <div class="gunaso">
    <div class="popup-body">
        <div class="card-header">
            <h4>गुनासो दर्ता</h4>
        </div>
        <div class="content">
            <form method="post" action="{{route('front.gunasostore')}}"  enctype="multipart/form-data" id="valid_form" >
                {{ csrf_field() }}
                <div class="form-group">
                    <label>नाम</label>
                    <input type="text" placeholder="नाम" id="name" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>ठेगाना</label>
                    <input type="text" placeholder="ठेगाना" id="address" name="address" class="form-control">
                </div>
                <div class="form-group">
                    <label>फोन न</label>
                    <input type="number" placeholder="फोन न" id="phone" name="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label>इमेल</label>
                    <input type="email" placeholder="इमेल" id="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>समस्या</label>
                    <textarea type="text" placeholder="समस्या" id="message" name="message" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>फाइल अपलोड गर्नुहोस्</label>
                    <input type="file" id="image" name="image">
                </div>
                <div class="read-btn">
                    <button type="submit" class="btn drone-btn">Submit</button>
                </div>
            </form>
        </div>
        <span class="close-icon gunaso-close"><i class="fa fa-close"></i></span>
    </div>
</div>  --}}

<!-- popup icon right end -->

<div class="hero-banner">
    <div class="hero-slider owl-carousel">
        @foreach($sliders as $slider)
        <div class="hero-item" style="background:url({{asset('/slider_upload/'.$slider->image)}});">
            <div class="overlay"></div>
            <div class="slider-caption">
                <h4>{{$slider->title}}</h4>
            </div>
        </div>
        @endforeach
        {{--  <div class="hero-item" style="background:url({{asset('assets/front/images/bg/1.jpg' )}});">
            <div class="overlay"></div>
            <div class="slider-caption">
                <h4>बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण</h4>
            </div>
        </div>  --}}
    </div>
</div>

<div class="banner-bottom">
    <div class="container">
        <div class="bottom-wrap">
            <div class="btm-item">
                <a href="https://foreignjob.dofe.gov.np/">
                    <span>
                        <img src="{{asset('assets/front/images/bannbt/1.png' )}}" alt="icon">
                    </span> वैदेशिक रोजगारीको खोजी
                </a>
            </div>
            <div class="btm-item">
                {{--  <a href="{{route('front.boardkarya')}}">  --}}

                {{--  <a href="{{route('front.boardkarya')}}" target="_blank">  --}}
                    <a href="http://coderprem.com/fepb-demo/boardKarya.html" target="_blank">

                <a href="{{route('front.boardkarya')}}">
                    {{--  <a href="http://coderprem.com/fepb-demo/boardKarya.html" target="_blank">  --}}

                        <span>
                            <img src="{{asset('assets/front/images/bannbt/2.png' )}}" alt="icon">
                        </span>सम्पादित कार्य विवरण
                    </a>
            </div>
            <div class="btm-item">
                <a href="{{route('front.walefare')}}">
                    {{--  <a href="http://coderprem.com/fepb-demo/walefare-list.html">  --}}
                        <span>
                            <img src="{{asset('assets/front/images/bannbt/3.png' )}}" alt="icon">

                        </span> कल्याणकारी कार्यहरुरु

                        </span> कल्याणकारी कार्यहरु

    
    
                    </a>
            </div>
            <div class="btm-item">
                <a href="{{route('front.mobileapp')}}">
                    <span>
                        <img src="{{asset('assets/front/images/bannbt/4.png' )}}" alt="icon">
                    </span> मोबाइल एप
                </a>
            </div>
            <div class="btm-item">
                {{-- <a href="{{route('front.tathyankabibaran')}}"> --}}
                <a href="{{ route('geo-map') }}">
                    <span>
                        <img src="{{asset('assets/front/images/bannbt/5.png' )}}" alt="icon">
                    </span> सेवा प्रदायक संस्थाहरु
                </a>
            </div>
            <div class="btm-item">
                <a href="{{route('front.chetanamulak')}}#research">
                    <span>
                        <img src="{{asset('assets/front/images/bannbt/6.png' )}}" alt="icon">
                    </span> सूचनामूलक सामाग्री
                </a>
            </div>
        </div>
    </div>
</div>

<div class="about-section section">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="introduct">
                    <div class="title">
                        <h3>हाम्रो बारेमा </h3>
                    </div>
                    <div class="content">
                        <div class="text">
                              {{--  <p> @if (strlen($about->short_description) <=550)
                                {!!$about->short_description!!}
                            @else
                                {!!substr($about->short_description, 0, 550) . '...'!!}
                            @endif</p>  --}}
                            <p>{!!$about->short_description!!}</p>
                            {{--  <p>नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार
                                प्रवर्द्धन बोर्डको व्यवस्था गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ ।
                            </p>
                            <p>माननीय श्रम तथा रोजगार मन्त्रीको अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा
                                अध्यक्ष सहित २५ जना सदस्यहरु रहनु भएको छ ।</p>  --}}

                        </div>
                        <div class="read-btn">
                            <a href="{{route('front.about')}}#about" class="btn">थप हेर्नुहोस्</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="chart">
                    <div class="title">
                        <h3>आर्थिक सहायता विवरण</h3>
                        <select class="custom-select">
                            <option>२०७०</option>
                            <option>२०७१</option>
                            <option>२०७२</option>
                            <option>२०७३</option>
                            <option>२०७४</option>
                        </select>
                    </div>
                    <div class="content">
                        {{--  <div id="chartContainer"></div>  --}}
                        <canvas id="myChart"></canvas>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="major-section section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="major-report">
                      
                        <div class="tab">
                            <div class="tab-nav">
                                <a href="{{route('front.pratibedhantype')}}" class="title-all">सबै
                                    <i class="fa fa-ellipsis-v"></i>
                                </a>
                                <ul>
                                    <li class="report-link current" data-tab="progress">प्रतिबेदन </li>
                                    <li class="report-link" data-tab="publication">प्रकाशन </li>
                                </ul>
                            </div>
    
                            <div class="tab-pane">
                                <div class="report-content current" id="progress">
                                    <div class="major-slider owl-carousel">
                                        @foreach($reports as $report)
                                        <div class="major-item">
                                            <div class="major-img">
                                                <a href="{{asset('/extra_upload/'.$report->other_file)}}" target="_blank" ><img src="{{asset('/extra_upload/'.$report->image)}}" alt="team"></a>
                                                <div class="major-caption">
                                                    {{$report->title}}
                                                </div>
                                            </div>
    
                                        </div>
                                        @endforeach                               
    
                                    </div>
                                </div>
                                <div class="report-content" id="publication">
                                    <div class="pub-slider owl-carousel">
                                            @foreach($publications as $pub)
                                            <div class="major-item">
                                                <div class="major-img">
                                                    <a href="{{asset('/extra_upload/'.$pub->other_file)}}" target="_blank" ><img src="{{asset('/extra_upload/'.$pub->image)}}" alt="team"></a>
                                                    <div class="major-caption">
                                                        {{$pub->title}}
                                                    </div>
                                                </div>
    
                                            </div>
                                            @endforeach                                  
    
                                    </div>
                                </div>
    
                            </div>
                        </div>
    
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                        <div class="teamlist">
                                <div class="title">
                                    <a href="{{route('front.about')}}#board-team">सबै
                                        <i class="fa fa-ellipsis-v"></i>
                                    </a>
                                </div>
                                <ul>
                                    @foreach ($members as $member)
                                    <li>
                                    <div class="team-item">
                                            <div class="team-img">
                                                <img src="{{asset('/member_upload/'.$member->image)}}" alt="team">
                                            </div>
                                            <div class="content">
                                                <h4>{{$member->name}}</h4>
                                                <span>{{$member->position}}</span>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                    
    
                </div>
            </div>
        </div>
    </div>
    


    <div class="tab-section section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="tab">
                    <div class="tab-nav">
                        <ul>
                            <li class="tab-link current" data-tab="latest-update">सूचना तथा समाचार</li>
                            <li class="tab-link" data-tab="bill-release">प्रेस विज्ञप्ति </li>
                            <li class="tab-link" data-tab="publish">विल सार्वजनिकरण</li>
                        </ul>
                    </div>

                    <div class="tab-pane">
                        <div class="tab-content current" id="latest-update">
                            <ul>
                                    @foreach($notices as $notice)
                                    <li>
                                        <span class="date">{{$notice->date}}</span>
                                        <a href="{{route('front.detailpage',[$notice->id,$notice->category])}}">{{$notice->title}}
                                 {{--  <a href="{{route('bus.busseat',[$singleRow->bus_no,$departDate,$resnum])}}" data-toggle="tooltip" title="Select Seat"><button>Select Seat</button></a></td>  --}}

                                        </a>
                                    </li>
                                    @endforeach

                                
                                <div class="read-btn">
                                    <a href="{{route('front.media')}}#news" class="btn">थप हेर्नुहोस्</a>
                                </div>

                            </ul>
                        </div>
                        <div class="tab-content" id="press-release">
                            <ul>
                                    @foreach($news as $new)
                                    <li>
                                        <span class="date">{{$new->date}}</span>
                                        <a href="{{route('front.detailpage',[$new->id,$new->category])}}">{{$new->title}}
                                        </a>
                                    </li>
                                    @endforeach
                               
                                <div class="read-btn">
                                    <a href="{{route('front.media')}}#news" class="btn">थप हेर्नुहोस्</a>
                                </div>

                            </ul>
                        </div>
                        <div class="tab-content" id="bill-release">
                            <ul>
                                    @foreach($presses as $press)
                                    <li>
                                        <span class="date">{{$press->date}}</span>
                                        <a href="{{route('front.detailpage',[$press->id,$press->category])}}">{{$press->title}}
                                        </a>
                                    </li>
                                    @endforeach
                                {{--  <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>
                                <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>
                                <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>  --}}
                                <div class="read-btn">
                                    <a href="{{route('front.media')}}#press" class="btn">थप हेर्नुहोस्</a>
                                </div>

                            </ul>
                        </div>
                        <div class="tab-content" id="publish">
                            <ul>
                                    @foreach($bills as $bill)
                                    <li>
                                        <span class="date">{{$bill->date}}</span>
                                        <a href="{{route('front.detailpage',[$bill->id,$bill->category])}}">{{$bill->title}}
                                        </a>
                                    </li>
                                    @endforeach
                                {{--  <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>
                                <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>
                                <li>
                                    <span class="date">कात्तिक १०, २०७५</span>
                                    <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण
                                    </a>
                                </li>  --}}
                                <div class="read-btn">
                                    <a href="{{route('front.media')}}#video" class="btn">थप हेर्नुहोस्</a>
                                </div>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                    <div class="video">
                            <div class="title">
                                <h3>श्रव्य दृश्य सामाग्री
                                </h3>
                                <a href="{{route('front.chetanamulak')}}#worker">सबै
                                    <i class="fa fa-ellipsis-v"></i>
                                </a>
                            </div>
                            <div class="content">
                                @foreach ($videos as $video)
                                <div class="video-list">
                                        <div class="video-thumb">
                                                {{--  <a class="video-popup" href="https://www.youtube.com/watch?v=xadQwMU4qtU" data-id="xadQwMU4qtU"><img src="{{asset('/multimedia_upload/'.$video->image)}}" data-id="SPVccuO9qbw" alt="video-thumb"></a>  --}}
                                                <a class="video-popup" href="{{$video->short_description}}" data-id="xadQwMU4qtU"><img src="{{asset('/multimedia_upload/'.$video->image)}}" data-id="SPVccuO9qbw" alt="video-thumb"></a>
               
                                        </div>
                                        <div class="video-text">
                                            <span>{{$video->date}}</span>
                                            <h4>
                                                {{--  <a href="{{route('front.detailpage',[$video->id,$video->category])}}">{{$video->title}}</a>  --}}
                                                <a href="{{route('front.chetanamulak')}}#worker">{{$video->title}}</a>
                                            </h4>
                                        </div>
                                    </div>
        
                                @endforeach    
                            
                                <div class="read-btn">
                                    <a href="{{route('front.chetanamulak')}}#worker" class="btn">थप हेर्नुहोस्</a>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>


@endsection
