<html>
    <head>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.js"></script>
            <script src="{{asset('assets/front/js/c3.js')}}"></script>

            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>



                <script>
                        window.onload = function() {
                            new Chart(document.getElementById("bar-chart"), {
                                type: 'bar',
                                data: {
                                  {{--  labels: ["Africa", "Asia", "Europe", "Latin America"],  --}}
                                  labels: [ @php

                                  for($i=0;$i<count($label);$i++)
                                  {
                                     echo "'$label[$i]',";
                                  }
                                  @endphp],

                                  datasets: [
                                    {
                                      label: "Population (millions)",
                                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                      {{--  data:[60,400,200,300]  --}}
                                      data:[ @php
                                      for($i=0;$i<count($value);$i++)
                                      {
                                         echo "$value[$i],";
                                      }
                                      @endphp]
                                    }
                                  ]
                                },
                                options: {
                                  legend: { display: false },
                                  title: {
                                    display: true,
                                    text: 'Predicted world population (millions) in 2050'
                                  }
                                }
                            });


                            var chart = new CanvasJS.Chart("chartContainer", {
                                animationEnabled: true,
                                theme: "light2", // "light1", "light2", "dark1", "dark2"
                                title: {
                                    text: ""
                                },
                                axisY: {
                                    title: ""
                                },
                                axisX: {
                                    title: ""
                                },
                                data: [
                                    {
                                    type: "column",
                                    showInLegend: true,
                                    legendMarkerColor: "grey",
                                    legendText: "",
                                    dataPoints: [
                                        @php
                                        for($i=0;$i<count($label);$i++)
                                        {
                                            echo"{label:'{$label[$i]}',y:{$value[$i]}},";

                                        }
                                        @endphp
                                    ]
                                }]
                            });
                            chart.render();






                        var chart = c3.generate({
                            data: {

                                columns: [
                                    @php
                                    echo "['पुरुष'";
                                    for($i=0;$i<count($value);$i++)
                                    {
                                       echo ",$value[$i]";
                                    }

                                    echo "]";
                                    @endphp

                                    {{--  ['पुरुष	', 200, 0, 90, 240, 130, 220],  --}}
                                    // ['महिला', 130, 120, 150, 140, 160, 150],
                                    // ['आंसिक अपाङ्ग', 300, 200, 160, 400, 250, 250],

                                ],
                                type: 'bar',
                                types: {
                                    रकम: 'spline',
                                },

                                groups: [
                                    ['data1', 'data2']
                                ]
                            },
                            axis: {
                                x: {
                                    type: 'category',
                                    categories: [
                                        @php

                                    for($i=0;$i<count($label);$i++)
                                    {
                                       echo "'$label[$i]',";
                                    }


                                    @endphp
                                        ]
                                }
                            },

                            bindto: '#chart2'
                        });
                    }

                </script>
<style>
    .grid1{
        display:grid;
        grid-template-columns: 1fr 1fr;
        grid-gap:20px;
    }
</style>

    </head>
    <body>
        <div class="grid-container">
            <div class="grid1">
                <div id="chartContainer"></div>

               <div>
                <canvas id="bar-chart" width="800" height="450"></canvas>
            </div>
            <div id="chart2">
            </div>

            </div>

        </div>







    </body>

</html>
