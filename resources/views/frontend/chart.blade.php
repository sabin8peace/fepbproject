<html>
    <head>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            <script>
                    window.onload = function() {

                        var chart = new CanvasJS.Chart("chartContainer", {
                            animationEnabled: true,
                            theme: "light2", // "light1", "light2", "dark1", "dark2"
                            title: {
                                text: ""
                            },
                            axisY: {
                                title: ""
                            },
                            axisX: {
                                title: ""
                            },
                            data: [
                                {
                                type: "column",
                                showInLegend: true,
                                legendMarkerColor: "grey",
                                legendText: "",
                                dataPoints: [
                                    @php
                                    for($i=0;$i<count($label);$i++)
                                    {
                                        echo"{label:'{$label[$i]}',y:{$value[$i]}},";

                                    }
                                    @endphp
                                ]
                            }]
                        });
                        chart.render();

                    }
                </script>




    </head>
    <body>
            <div id="chartContainer"></div>

    </body>

</html>
