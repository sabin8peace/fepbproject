@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" href="{{asset('assets/front/css/multimedia.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/infographics.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/magnific-popup.css')}}">

@endpush
@push('js')
<script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}"></script>

<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='research')
        {

            $('#research').addClass('in active');
            $('#research1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='worker')
        {
            $('#worker').addClass('in active');
            $('#worker1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='faq')
        {
            $('#faq').addClass('in active');
            $('#faq1').addClass('in active');
           
        }
        if(check=='notice')
        {
            $('#notice').addClass('in active');
            $('#notice1').addClass('in active');
           
        }


        if(check=='opencountry')
        {
            $('#opencountry').addClass('in active');
            $('#opencountry1').addClass('in active');
           
        }

        if(check=='implink')
        {
            $('#implink').addClass('in active');
            $('#implink1').addClass('in active');
           
        }
        if(check=='help')
        {
            $('#help').addClass('in active');
            $('#help1').addClass('in active');
           
        }
        if(check=='center-service')
        {
            $('#center-service').addClass('in active');
            $('#center-service1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }


        })
</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}
        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="side-menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " href="#research" id="research1" role="tab" data-toggle="tab">अडियो </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#worker" id="worker1" role="tab" data-toggle="tab">भिडियो </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#center-service" role="tab" id="center-service1" data-toggle="tab">Info-Graphics</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#faq" role="tab" id="faq1" data-toggle="tab"> बारम्बार सोधिने प्रश्न</a>
                            </li>
                            <li class="nav-item">

                                <a class="nav-link" href="#source" role="tab" data-toggle="tab">Diplomatic Table</a>

                                <a class="nav-link" href="#source" role="tab" data-toggle="tab">कुटनीतिक नियोगहरु</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#bank" role="tab" data-toggle="tab">बैंककहरुको विवरण</a>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#notice" role="tab" id="notice1" data-toggle="tab">कामदारहरुले ध्यान दिनु पर्ने कुराहरु</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#help" role="tab" id="help1" data-toggle="tab"> सहयोगी संस्थाहरु</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#implink" id="implink1" role="tab" data-toggle="tab">महत्वपुर्ण लिंकहरु</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" href="#opencountry" id="opencountry1" role="tab" data-toggle="tab">खुल्ला गरिएको देश हरु </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class="menu-content">
                        <div class="card">
                            <div class="card-body">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane " id="research">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>जिङ्गल्</h4>
                                            </div>
                                            <section class="audioWrapper">
                                                <div class="boxes">
                                                    @foreach ($audios as $item)
                                                    <div class="box">
                                                            <div class="titleAudio">
                                                                <h3>{{$item->title}}</h3>
                                                                <p>
                                                                    {{$item->date}}
                                                                </p>
                                                            </div>
                                                            <div class="audioPlayer">
                                                                <audio controls>
                                                                    <source src="{{asset('/multimedia_upload/'.$item->other_file)}}" type="audio/ogg">
                                                                    <source src="{{asset('/multimedia_upload/'.$item->other_file)}}" type="audio/mpeg">
                                                                </audio>
                                                            </div>
                                                            <div class="audioDescription">
                                                                <p>
                                                                    {!!$item->description!!}
                                                                </p>
                                                            </div>
                                                        </div>

                                                    @endforeach




                                                </div>
                                            </section>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="source">
                                        <div class="card-header">
                                            <h4>Diplomatic Table</h4>
                                        </div>
                                        <div class="content-list">
                                            <div class="table table-responsive">
                                                <table class="table  table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>s.n</th>
                                                            <th>Diplomatic Missions</th>


                                                            <th>Country</th>

                                                            <th>email</th>
                                                            <th>website</th>
                                                            <th>telephone</th>
                                                            <th>Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td>2</td>
                                                            <td>Canbera Australian Embassy Of Nepal</td>
                                                            <td>abc@gmail.com </td>
                                                            <td>fepb@gov.np</td>
                                                            <td>01-4115698</td>
                                                            <td>Australia Nepal</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Canbera Australian Embassy Of Nepal</td>
                                                            <td>abc@gmail.com </td>
                                                            <td>fepb@gov.np</td>
                                                            <td>01-4115698</td>
                                                            <td>Australia Nepal</td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Canbera Australian Embassy Of Nepal</td>
                                                            <td>abc@gmail.com </td>
                                                            <td>fepb@gov.np</td>
                                                            <td>01-4115698</td>
                                                            <td>Australia Nepal</td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Canbera Australian Embassy Of Nepal</td>
                                                            <td>abc@gmail.com </td>
                                                            <td>fepb@gov.np</td>
                                                            <td>01-4115698</td>
                                                            <td>Australia Nepal</td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>Canbera Australian Embassy Of Nepal</td>
                                                            <td>abc@gmail.com </td>
                                                            <td>fepb@gov.np</td>
                                                            <td>01-4115698</td>
                                                            <td>Australia Nepal</td>
                                                        </tr>

                                                        @foreach ($neyogs as $i=>$item)
                                                        <tr>
                                                                <td>{{$i+1}}</td>
                                                                <td>{{$item->mission}}</td>
                                                                <td>{{$item->country}}</td>
                                                                <td>{{$item->email}}</td>
                                                                <td>{{$item->website}} </td>
                                                                <td>{{$item->phone}}</td>
                                                                <td>{{$item->address}}</td>
                                                            </tr>
                                                            
                                                        @endforeach
                                                      
                                                  
                                                   
                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="opencountry">
                                        <div class="card-header">
                                            <h4>खुल्ला गरिएको देश हरु </h4>
                                        </div>
                                        <div class="content-list">
                                            <div class="table table-responsive">
                                                <table class="table  table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>s.n</th>                                                            
                                                            <th>Country</th>
                                                          
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($openCountries as $i=>$item)
                                                        <tr>
                                                                <td>{{$i+1}}</td>
                                                                <td>{{$item->country_name}}</td>
                                                              
                                                            </tr>
                                                            
                                                        @endforeach
                                                       
                                                       
                                                    
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="bank">
                                        <div class="card-header">
                                            <h4>कल्याणकारी कोषको रकम जम्मा गर्ने बैंककहरुको विवरण </h4>
                                        </div>
                                        <div class="content-list">
                                            <div class="table table-responsive">
                                                <table class="table  table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>सि.नं.</th>                                                            
                                                            <th>बैंकको नाम</th>
                                                            <th>बैंक हिसाब नं.</th>
                                                          
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($banks as $i=>$item)
                                                        <tr>
                                                                <td>{{$i+1}}</td>
                                                                <td>{{$item->bank_name}}</td>
                                                                <td>{{$item->account}}</td>
                                                              
                                                            </tr>
                                                            
                                                        @endforeach
                                                       
                                                       
                                                    

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="implink">
                                        <div class="card-header">
                                            <h4>महत्वपुर्ण लिंकहरु</h4>
                                        </div>
                                        <div class="content-list">
                                            <ul>
                                                @foreach ($links as $item)
                                                <li>
                                                        <a href="{{$item->link}}" target="_blank">{{$item->title}}</a>
                                                    </li>
                                                @endforeach
                                               
                                       
                                            </ul>
                                        </div>
                                    
                                    </div>
                                <div role="tabpanel" class="tab-pane " id="faq">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4> बारम्बार सोधिने प्रश्न</h4>
                                            </div>
                                            <ul>
                                                @foreach ($faqs as $i=>$item)
                                                @if ($i==0)
                                                <li class="active">
                                                    @else
                                                    <li class="">                                                    
                                                @endif                                             

                                                        <div class="acc-title">
                                                            <h4>{!!$item->short_description!!}</h4>
                                            
                                                            <i class="fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="acc-content">
                                                            <div class="text">
                                                                {!!$item->description!!}
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                @endforeach                                              
                                               
                                            </ul>
                                        
                                        </div>
                                    
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="notice">
                                        
                                        <div class="card-header">
                                            <h4>कामदारहरुले ध्यान दिनु पर्ने कुराहरु</h4>
                                        </div>
                                        <div class="content-list">
                                            <ul>
                                                @foreach ($basicnotes as $item)
                                                
                                                        {!!$item->description!!}
                                                    
                                                    
                                                @endforeach
                                               
                                                
                                            </ul>
                                        </div>
                                    
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="help">
                                            <div class="card-header">
                                                <h4> सहयोगी संस्थाहरु
                                                </h4>
                                            </div>
                                            <div class="content-list">
                                                <ul>
                                                    @foreach ($institutes as $item)
                                                    
                                                        {!!$item->description!!}
                                                          
                                                        
                                                    @endforeach
                                                    
                                                </ul>
                                            </div>
                                        
                                        </div>

                                    <div role="tabpanel" class="tab-pane " id="worker">
                                        <div class="card-header">
                                            <h4>भिडियो
                                            </h4>
                                        </div>
                                        <!-- video -->
                                        <section class="videoWrapper">
                                            <div class="boxes">
                                                @foreach ($videos as $item)
                                                <div class="box">
                                                        <div class="video">
                                                                <a class="video-popup" href="{{$item->short_description}}" target="_blank" data-id="xadQwMU4qtU"><img src="{{asset('/multimedia_upload/'.$item->image)}}" data-id="SPVccuO9qbw" alt="video-thumb"></a>

                                                            {{--  <iframe width="560" height="315" src="{{$item->short_description}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  --}}
                                                        </div>
                                                        <div class="content">
                                                            <h3>{{$item->title}}</h3>
                                                            <p class="date">{{$item->date}}</p>
                                                            <div class="text">
                                                                {!!$item->description!!}

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                                  {{--  <div class="box">
                                                    <div class="video">
                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/0EwXXrZq0kQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="content">
                                                        <h3>Compiled Ads on Television</h3>
                                                        <p class="date">April 22, 2012</p>
                                                        <div class="text">
                                                            विदेशमा काम गरी फर्किएर स्वदेशमा उद्यम गरी बसेका उद्यमीलाई राष्ट्रिय सम्मान तथा पुरस्कार व्यवस्थापन कार्य्विधी
                                                        </div>
                                                    </div>
                                                </div>
                                           --}}

                                            </div>

                                        </section>
                                        <!-- video ends here -->

                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="center-service">
                                        <div class="card-header">
                                            <h4>
                                                {!!$infographic->description!!}
                                                {{--  नेपाल सरकारबाट संस्थागत रुपमा बैदेशिक रोजगारका लागि खुल्ला गरिएका देशहरुको नामावली  --}}
                                            </h4>
                                        </div>
                                        <div class="container infographicsWrapper">
                                            <div class="row">
                                                <div class="row">
                                                    @php($k=1)
                                                    @foreach ($photos as $item)
                                                    <input type="hidden" name="" id="" value="{{$k++}}">
                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                            <a class="thumbnail major-preview" href="{{asset('/multimedia_upload/'.$item->image)}}">
                                                                <img class="img-thumbnail" src="{{asset('/multimedia_upload/'.$item->image)}}" alt="Another alt text">
                                                            </a>
                                                            {{--  <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""  data-target='#imagegallery{{$k}}'>
                                                                <img class="img-thumbnail" src="{{asset('/multimedia_upload/'.$item->image)}}" alt="Another alt text">
                                                            </a>  --}}
                                                        </div>
                                                        <!-- The Modal -->
                                                        <div class="modal fade infoModal" id="imagegallery{{$k}}">
                                                          <div class="modal-dialog">
                                                            <div class="modal-content">

                                                              <!-- Modal Header -->
                                                              <div class="modal-header">
                                                                <h4 class="modal-title">{{$item->title}}</h4>
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              </div>

                                                              <!-- Modal body -->
                                                              <div class="modal-body">
                                                                      <img id="image-gallery-image" class="img-responsive col-md-12" src="{{asset('/multimedia_upload/'.$item->image)}}">
                                                                      <br>
                                                                      <p style="padding:10px;">{!!$item->description!!}</p>

                                                              </div>

                                                              <!-- Modal footer -->
                                                              <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i></button>
                                                              </div>

                                                            </div>
                                                          </div>
                                                        </div>

                                                    @endforeach


                                                </div>


                                                {{--  <div class="modal fade infoModal" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="image-gallery-title"></h4>
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">×</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary float-left" id="show-previous-image">
                                                                    <i class="fa fa-arrow-left"></i>
                                                                </button>

                                                                <button type="button" id="show-next-image" class="btn btn-secondary float-right">
                                                                    <i class="fa fa-arrow-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
