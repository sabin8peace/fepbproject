@extends('layouts.frontmaster')

@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="menu-content">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                <h4>बोर्डबाट प्रदान गरिने सेवाहरु</h4>
                            </div>
                            <div class="wlfr-list">
                                <div class="row">
                                    
                                        @foreach ($walefares as $k => $items)

                                        @foreach ($items as $i => $item)

                                        @if ($i==0)

                                        {{--  <li class="nav-item">
                                             @if ($k==0)  
                                        <a class="nav-link active" href="#chart{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                    @else
                                                    <a class="nav-link" href="#chart{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>

                                            @endif

                                        </li>  --}}
                                        <div class="col-lg-4 col-md-6">
                                                <div class="wlfr-item">
                                                    <div class="wlfr-content">
                                                        <span class="count-icon">{{$k+1}}</span>
                                                        <h4><a href="{{route('front.walefaredetail',$item->category_id)}}">{{$item->product_category->name}}</a></h4>
        
                                                    </div>
        
                                                </div>
                                            </div>
                                        @endif
                                        @endforeach
                        @endforeach
                                  

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
