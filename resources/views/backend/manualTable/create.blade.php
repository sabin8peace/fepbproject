@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('manualTable.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($manualTable->id))
                            <form method="post" action="{{route('manualTable.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('manualTable.update',$manualTable)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$manualTable->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type ="text" class="form-control" name ="title" id="title" value="{{$manualTable->title}}" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <span class="data-label" for="category">Show Table Inside</span>
                                    <select class="form-control myselect" name="general_id" id="general_id" required>
                                            @if(isset($manualTable->id))
                                            <option selected value="{{$manualTable->general_id}}">{{$manualTable->general->title}}</option>
                                            @endif
                                            {{--  <option selected value="{{$manualTable->general_id}}">{{$manualTable->general_id}}</option>  --}}
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>

                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  >{!!$manualTable->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="table_name">Table Name</label>
                                <input type ="text" class="form-control" pattern="[A-Za-z0-9]+" name ="table_name" id="table_name" oninput="this.value=this.value.toLowerCase()" onkeypress="return (event.charCode > 64 &&
                                event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)||(event.charCode==32)"  value="{{$manualTable->table_name}}" required>
                                @if ($errors->has('table_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('table_name') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save manualTable">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
