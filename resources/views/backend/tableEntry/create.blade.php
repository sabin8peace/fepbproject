@extends('adminlte::page')
@push('css')
<style>
    .multi-field
    {
        margin-top: 20px;
    }
</style>
@endpush
@push('js')

<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script>

        $('.multi-field-wrapper').each(function() {
            var $wrapper = $('.multi-fields', this);
            $(".add-field", $(this)).click(function (e) {
                var a =  $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('select').val('').focus();
                //a.attr('id','abcd');

            });
            $('.multi-field .remove-field', $wrapper).click(function () {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent('.multi-field').remove();
            });
        });

    </script>

@endpush

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('tableEntry.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($tableEntry->id))
                            <form method="post" action="{{route('tableEntry.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('tableEntry.update',$tableEntry)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$tableEntry->created_by}}">
                             @endif
                            {{csrf_field()}}
                           <div class="form-group">
                                <span class="data-label" for="category">Table Name</span>
                                <select class="form-control myselect" name="manual_table_id" id="manual_table_id" required>
                                        <option selected value="{{$tableEntry->manual_table_id}}">{{$tableEntry->manual_table_id}}</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->table_name}}</option>
                                    @endforeach
                                </select>

                        </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>

                                <div class="multi-fields">
                                        <div class="multi-field-wrapper">
                                            <div class="col-md-2 col-md-offset-10"> <button type="button" class="add-field btn btn-success"><i class="fa fa-plus"></i> </button>
                                            </div>
                                            <div class="multi-fields">
                                                <div class="multi-field">
                                                        <label>Field Name:</label>
                                                        <input type ="text"  name ="table_field[]"  oninput="this.value=this.value.toLowerCase()" onkeypress="return (event.charCode > 64 &&
                                                        event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)||(event.charCode==32)"  value="{{$tableEntry->table_field}}" required>

                                                        <label>Field Type</label>
                                                         <select class="myselect" name="field_type[]"  required>
                                                            {{--  <option selected value="{{$tableEntry->field_type}}">{{$tableEntry->field_type}}</option>  --}}
                                                            <option value="BIGINT">Integer</option>
                                                            <option value="TEXT" selected>Text</option>
                                                            <option value="DOUBLE">Number</option>
                                                    </select>
                                                    <button type="button" class="remove-field btn btn-danger"><i class="fa fa-times"></i> </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                            <div class="col-md-12 col-lg-12">
                                <input class="btn btn-success form-control" type ="submit" name ="submit" value="Save ">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
