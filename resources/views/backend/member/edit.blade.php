
@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create member</label>
                        <a href="{{route('member.index')}}" class="btn btn-primary pull-right">member List</a>
                    </div>

                    <div class="panel-body col-md-8 col-md-offset-2">

                        <form action="{{route('member.update',$member->id)}}" method="post" enctype="multipart/form-data" id="valid_form">
                            <input type="hidden" name="_method" value="put">
                            {{csrf_field()}}

                            <div class="form-group ">
                                <label for="name">Name</label>
                                <input class="form-control" type="text" name="name" id="name" value="{{$member->name}}" >
                            </div>

                            <div class="form-group">

                                <input type="hidden" name="image" id="image" class="form-control" value="{{$member->image}}" >

                                <label for="image">Change image</label>
                                <input type="file" name="image" id="image" class="form-control dropify" value="{{$member->image}}">
                                <td><img src="{{asset('/member_upload/'.$member->image)}}" height="100" width="100"></td>
                            </div>
                            <div class="form-group ">
                                <label for="position">Position</label>
                                <input class="form-control" type="text" name="position" id="position" value="{{$member->position}}" >
                            </div>
                            <div class="form-group ">
                                <label for="position">Catageory</label>
                                <select class="form-control myselect" name="category" id="category" value="{{$member->category}}">
                                    <option value="normal">YI-Lab Team</option>
                                    <option value="exclusive">YI-Lab Advisor</option>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor"  name="description" id="description" >{{$member->description}}</textarea>
                            </div>
                            {{--<div class="form-group ">--}}
                                {{--<label for="description">Map Location</label>--}}
                                {{--<textarea class="form-control ckeditor"  name="map" id="map" >{{$member->map}}</textarea>--}}
                            {{--</div>--}}
                            {{--  <div class="form-group ">
                                <label for="education">Education</label>
                                <input class="form-control" type="text" name="education" id="education" value="{{$member->education}}" >
                            </div>
                            <div class="form-group ">
                                <label for="address">Address</label>
                                <input class="form-control" type="text" name="address" id="address" value="{{$member->address}}" >
                            </div>
                            <div class="form-group ">
                                <label for="phone">Phone</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="{{$member->phone}}" >
                            </div>
                            <div class="form-group ">
                                <label for="email">email</label>
                                <input class="form-control" type="email" name="email" id="email" value="{{$member->email}}" >
                            </div>
                            <div class="form-group ">
                                <label for="website">Website</label>
                                <input class="form-control" type="text" name="website" id="website" value="{{$member->website}}" >
                            </div>  --}}
                            <div class="form-group ">
                                <label for="fb_link">Facebook Link</label>
                                <input class="form-control" type="text" name="fb_link" id="fb_link" value="{{$member->fb_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="twitter_link">Twitter Link</label>
                                <input class="form-control" type="text" name="twitter_link" id="twitter_link" value="{{$member->twitter_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="linkedin_link">Linkedin Link</label>
                                <input class="form-control" type="text" name="linkedin_link" id="linkedin_link" value="{{$member->linkedin_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="insta_link">Instagram Link</label>
                                <input class="form-control" type="text" name="insta_link" id="insta_link" value="{{$member->insta_link}}" >
                            </div>





                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="radio" name="status" id="status" value="1" checked>Active
                                <input type="radio" name="status" id="status" value="0"  >Inactive

                            </div>

                            <div class="form-group">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                            </div>
                            <div class="form-group col-md-4 col-md-offset-4">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Update member">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection


