
@extends('layouts.backmaster')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create member</label>
                        <a href="{{route('member.index')}}" class="btn btn-primary pull-right">member List</a>
                    </div>

                    <div class="panel-body">
                    @if(!isset($member->id))
                    <form action="{{route('member.store')}}" method="post" enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('member.update',$member)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$member->created_by}}">
                             @endif

                            {{csrf_field()}}
                            <div class="form-group ">
                                <label for="name">Name</label>
                                <input class="form-control" type="text" name="name" id="name" value="{{$member->name}}" >
                            </div>

                            <div class="form-group">

                                <label for="image">Change image</label>
                                <input type="file" name="image" id="image" class="form-control " value="{{$member->image}}">
                                <td><img src="{{asset('/member_upload/'.$member->image)}}" height="100" width="100"></td>
                            </div>
                            <div class="form-group ">
                                <label for="position">Position</label>
                                <input class="form-control" type="text" name="position" id="position" value="{{$member->position}}" >
                            </div>

                            <div class="form-group ">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor"  name="description" id="description" >{{$member->description}}</textarea>
                            </div>

                            <!-- <div class="form-group ">
                                <label for="education">Education</label>
                                <input class="form-control" type="text" name="education" id="education" value="{{$member->education}}" >
                            </div> -->
                            <div class="form-group ">
                                <label for="address">Address</label>
                                <input class="form-control" type="text" name="address" id="address" value="{{$member->address}}" >
                            </div>
                            <div class="form-group ">
                                <label for="phone">Phone</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="{{$member->phone}}" >
                            </div>
                            <div class="form-group ">
                                <label for="email">email</label>
                                <input class="form-control" type="email" name="email" id="email" value="{{$member->email}}" >
                            </div>
                            <div class="form-group ">
                                <label for="joined_from">joined from</label>
                                <input class="form-control" type="date" name="joined_from" id="joined_from" value="{{$member->joined_from}}" >
                            </div>
                            <div class="form-group ">
                                <label for="rank">Rank</label>
                                <input class="form-control" type="text" name="rank" id="rank" value="{{$member->rank}}" >
                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="radio" name="status" id="status" value="1" checked>Active
                                <input type="radio" name="status" id="status" value="0"  >Inactive

                            </div>


                            <div class="form-group col-md-4 col-md-offset-4">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Update member">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection


