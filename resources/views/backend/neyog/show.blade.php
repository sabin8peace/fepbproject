@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>neyog Detail</label>
                        <a href="{{route('neyog.index')}}" class="btn btn-primary pull-right">neyog List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>mission</th>
                                <td>{{$neyog->mission}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($neyog->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>country</th>
                                <td>{{$neyog->country}}</td>
                            </tr>
                            <tr>
                                    <th>website</th>
                                    <td>{{$neyog->website}}</td>
                                </tr>
                                <tr>
                                        <th>email</th>
                                        <td>{{$neyog->email}}</td>
                                    </tr>
                                    <tr>
                                            <th>phone</th>
                                            <td>{{$neyog->phone}}</td>
                                        </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($neyog->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($neyog->updated_by))
                                        {{\App\User::find($neyog->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$neyog->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$neyog->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('neyog.edit',$neyog)}}" class="btn btn-warning">Edit neyog</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('neyog.destroy',$neyog)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

