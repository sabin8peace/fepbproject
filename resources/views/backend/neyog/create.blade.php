@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('neyog.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($neyog->id))
                            <form method="post" action="{{route('neyog.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('neyog.update',$neyog)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$neyog->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="mission">Mission</label>
                                    <input type ="text" class="form-control" name ="mission" id="mission" value="{{$neyog->mission}}" required>
                                    @if ($errors->has('mission'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mission') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            <div class="form-group">
                                    <label for="country">country</label>
                                    <input type ="text" class="form-control" name ="country" id="country" value="{{$neyog->country}}" required>


                                </div>
                                <div class="form-group">
                                        <label for="website">website</label>
                                        <input type ="text" class="form-control" name ="website" id="website" value="{{$neyog->website}}" required>
    
    
                                    </div>
                                    <div class="form-group">
                                            <label for="email">email</label>
                                            <input type ="text" class="form-control" name ="email" id="email" value="{{$neyog->email}}" required>
        
        
                                        </div>
                                        <div class="form-group">
                                                <label for="phone">phone</label>
                                                <input type ="text" class="form-control" name ="phone" id="phone" value="{{$neyog->phone}}" required>
            
            
                                            </div>
                                            <div class="form-group">
                                                    <label for="address">address</label>
                                                    <input type ="text" class="form-control" name ="address" id="address" value="{{$neyog->address}}" required>
                
                
                                                </div>


                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>




                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save neyog">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
