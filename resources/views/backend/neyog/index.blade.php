@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label> List</label>
                        <a href="{{route('neyog.create')}}" class="btn btn-primary pull-right">Add </a>
                         </div>
                           {{-- commented here --}}

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table class="table table-bordered" id="data_table">
                            <thead>
                            <tr>

                                <th>SN</th>
                                <th>mission</th>
                                <th>country</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($neyog as $s)
                                <tr>

                                    <td>{{$i++}}</td>
                                    <td>{{$s->mission}}</td>
                                    <td>{{$s->country}}</td>

                                    <td>
                                        @if($s->status==1)
                                            <label class="label label-success">Active</label>
                                        @else
                                            <label class="label label-danger">Inactive</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('neyog.edit',$s)}}" class="btn btn-warning btn-block">Edit</a>
                                        <a href="{{route('neyog.show',$s)}}" class="btn btn-info btn-block">View Detail</a>

                                        <form action="{{route('neyog.destroy',$s)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

