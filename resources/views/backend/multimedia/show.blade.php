@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>multimedia Detail</label>
                        <a href="{{route('multimedia.index')}}" class="btn btn-primary pull-right">multimedia List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$multimedia->title}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/multimedia_upload/'.$multimedia->image)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Other File</th>
                                <td><a href="{{asset('/multimedia_upload/'.$multimedia->other_file)}}" target="_blank">{{asset('/multimedia_upload/'.$multimedia->other_file)}}</a></td>
                            </tr>



                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($multimedia->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$multimedia->description!!}</td>
                            </tr>
                            <tr>
                                <th>Short Description</th>
                                <td>{!!$multimedia->short_description!!}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($multimedia->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($multimedia->updated_by))
                                        {{\App\User::find($multimedia->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$multimedia->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$multimedia->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('multimedia.edit',$multimedia)}}" class="btn btn-warning">Edit multimedia</a>
                                </td>
                                <td>
                                    <form action="{{route('multimedia.destroy',$multimedia)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

