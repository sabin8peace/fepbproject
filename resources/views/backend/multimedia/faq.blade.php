@extends('layouts.backmaster')



@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create बारम्बार सोधिने प्रश्न</label>
                        <a href="{{route('multimedia.index')}}" class="btn btn-primary pull-right">बारम्बार सोधिने प्रश्न List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($multimedia->id))
                            <form method="post" action="{{route('multimedia.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('multimedia.update',$multimedia)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$multimedia->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    {{--  <label for="title">Title</label>  --}}
                                    <input type ="hidden" class="form-control" name ="title" id="title" value="FAQ SECTION" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            {{--  <div class="form-group">
                                <label for="filetype">filetype</label>
                                <input type ="text" class="form-control" name ="filetype" id="filetype" value="{{$multimedia->filetype}}" required>
                                @if ($errors->has('filetype'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('filetype') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}
                            {{--  <div class="form-group">
                                <label for="date">Date</label>
                                <input type ="text" class="form-control" name ="date" id="date" value="{{$multimedia->date}}" required>
                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}


                            {{--  <div class="form-group">
                                <label for="image">image</label>

                                <input type="file" name="image" id="image" class="form-control" value="{{$multimedia->image}}">

                                <td><img src="{{asset('/multimedia_upload/'.$multimedia->image)}}" height="100" width="100"></td>

                            </div>  --}}
                            {{--  <div class="form-group">
                                <label for="other_file">Files</label>

                                <input type="file" name="other_file" id="other_file" class="form-control" value="{{$multimedia->other_file}}">

                                <td><a href="{{asset('/multimedia_upload/'.$multimedia->other_file)}}" target="_blank">{{asset('/multimedia_upload/'.$multimedia->other_file)}}</a></td>

                            </div>  --}}
                            {{--  <div class="form-group">
                                <span class="data-label" for="category">multimedia Type</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                        <option selected value="{{$multimedia->category}}">{{$multimedia->category}}</option>
                                        @foreach($multimediacategory as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>  --}}
                        {{--  <div class="form-group">
                                <span class="data-label" for="category">Downloads category</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                    @if($multimedia->category=='suchana')
                                        <option value="{{$multimedia->category}}">Suchana Samachar</option>
                                        @else
                                        <option value="{{$multimedia->category}}">{{$multimedia->category}}</option>
                                    @endif
                                        <option value="ain" selected >Ain Neyam</option>
                                        <option value="bulletin" >Bulletin</option>
                                        <option value="pratibedhan" >pratibedhan</option>
                                        <option value="report" >Reports</option>
                                        <option value="other" >Other</option>
                                </select>

                        </div>  --}}
                        <input type="hidden" id="category" name="category" value="faq">

                            <div class="form-group">
                                <label for="short_description">Question</label>
                                <textarea class="form-control " name ="short_description" id="short_description"  required>{!!$multimedia->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="description">Answer</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$multimedia->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>



                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Information">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
