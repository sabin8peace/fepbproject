@extends('layouts.backmaster')

@section('css')
    {{--  <link type="text/css" rel="stylesheet" href="{{asset('assets/datatables/css/jquery.dataTables.min.css')}}">  --}}
@endsection

@section('js')
    {{--  <script type="text/javascript" src="{{asset('assets/datatables/js/jquery.dataTables.min.js')}}"></script>  --}}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#data_table').DataTable();
        });
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label> सेटिंग लिस्ट </label>
                        {{--  <a href="{{route('setting.create')}}" class="btn btn-primary pull-right">Create Setting</a>  --}}

                    </div>

                    <div class="panel-body">

                        @if(Session::has('success'))
                            <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{Session::get('error')}}</div>
                        @endif


                        <table  class="table table-bordered" id="data_table">

                            <thead>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Logo</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @php($i=1)
                            @foreach($setting as $s)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$s->name}}</td>
                                    <td><img src="{{asset('/setting_upload/'.$s->logo)}}" height="100" width="100"></td>
                                    <td>{{$s->address}}</td>
                                    <td>{{$s->phone1}}</td>
                                    <td>
                                        <a href="{{route('setting.edit',$s)}}" class="btn btn-warning btn-block">Edit</a>
                                        <a href="{{route('setting.show',$s)}}" class="btn btn-info btn-block">View Detail</a>

                                        {{--  <form action="{{route('setting.destroy',$s)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>  --}}
                                    </td>

                                </tr>


                            @endforeach

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
