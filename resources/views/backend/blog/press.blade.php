@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create प्रेस विज्ञप्ति</label>
                        <a href="{{route('blog.index')}}" class="btn btn-primary pull-right">प्रेस विज्ञप्ति List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($blog->id))
                            <form method="post" action="{{route('blog.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('blog.update',$blog)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$blog->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">File Name</label>
                                <input type ="text" class="form-control" name ="title" id="title" value="{{$blog->title}}" required>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type ="text" class="form-control" name ="date" id="date" value="{{$blog->date}}" required>
                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif

                            </div>

                            {{--  <div class="form-group">
                                <label for="image">image</label>

                                <input type="file" name="image" id="image" class="form-control" value="{{$blog->image}}">

                                <td><img src="{{asset('/blog_upload/'.$blog->image)}}" height="100" width="100"></td>

                            </div>  --}}
                            <div class="form-group">
                                <label for="other_file">File</label>

                                <input type="file" name="other_file" id="other_file" class="form-control" value="{{$blog->other_file}}">

                                <td><a href="{{asset('/blog_upload/'.$blog->other_file)}}" target="_blank">{{asset('/blog_upload/'.$blog->other_file)}}</a></td>

                            </div>
                            {{--  <div class="form-group">
                                <span class="data-label" for="category">Blog Type</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                        <option selected value="{{$blog->category}}">{{$blog->category}}</option>
                                        @foreach($blogcategory as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>  --}}
                        <div class="form-group">
                            <span class="data-label" for="category">Media category</span>
                            <select class="form-control myselect" name="category" id="category" required>
                                @if($blog->category=='suchana')
                                    <option value="{{$blog->category}}">Suchana Samachar</option>
                                    @else
                                    <option value="{{$blog->category}}">{{$blog->category}}</option>
                                @endif
                                    <option value="suchana" >Suchana, Samachar</option>
                                    <option value="press" selected >Press</option>
                                    <option value="photo" >Photo</option>
                                    <option value="video" >Video</option>
                            </select>
                            {{--  <input type="hidden" id="category" name="category" value="boardwork">  --}}

                    </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            {{--  <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$blog->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}
                            {{--  <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea class="form-control ckeditor" name ="short_description" id="short_description"  required>{!!$blog->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
