@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>blog Detail</label>
                        <a href="{{route('blog.index')}}" class="btn btn-primary pull-right">blog List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$blog->title}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/blog_upload/'.$blog->image)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Other File</th>
                                <td><a href="{{asset('/blog_upload/'.$blog->other_file)}}" target="_blank">{{asset('/blog_upload/'.$blog->other_file)}}</a></td>
                            </tr>



                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($blog->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$blog->description!!}</td>
                            </tr>
                            <tr>
                                <th>Short Description</th>
                                <td>{!!$blog->short_description!!}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($blog->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($blog->updated_by))
                                        {{\App\User::find($blog->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$blog->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$blog->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('blog.edit',$blog)}}" class="btn btn-warning">Edit blog</a>
                                </td>
                                <td>
                                    <form action="{{route('blog.destroy',$blog)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

