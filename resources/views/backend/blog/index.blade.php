@extends('layouts.backmaster')
@section('css')
    {{--  <link type="text/css" rel="stylesheet" href="{{asset('assets/datatables/css/jquery.dataTables.min.css')}}">  --}}
@endsection

@section('javascript')
    {{--  <script type="text/javascript" src="{{asset('assets/datatables/js/jquery.dataTables.min.js')}}"></script>  --}}
    {{--  <script type="text/javascript">
        $(document).ready(function(){
            $('#blog_table').DataTable();
        });
    </script>  --}}
@endsection

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                    <label>
                        <a href="{{route('press.create')}}" class="btn btn-primary pull-right">Create प्रेस विज्ञप्ति</a>
                        <a href="{{route('video.create')}}" class="btn btn-primary pull-right">Create भिडियो संगह्र</a>
                        <a href="{{route('gallery.create')}}" class="btn btn-primary pull-right">Create तस्विर संग्रह</a>
                        <a href="{{route('bill.create')}}" class="btn btn-primary pull-right">Create विल </a>
                        <a href="{{route('suchana.create')}}" class="btn btn-primary pull-right">Create सूचना तथा समाचार</a>

                    </label>
                                    </div>

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table class="table table-bordered" id="data_table">
                            <thead>
                            <tr>

                                <th>SN</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($blog as $s)
                                <tr>

                                    <td>{{$i++}}</td>
                                    <td>{{$s->title}}</td>
                                    <td>{{$s->category}}</td>

                                    <td>
                                        @if($s->status==1)
                                            <label class="label label-success">Active</label>
                                        @else
                                            <label class="label label-danger">Inactive</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('blog.edit',$s)}}" class="btn btn-warning btn-block">Edit</a>
                                        <a href="{{route('blog.show',$s)}}" class="btn btn-info btn-block">View Detail</a>

                                        <form action="{{route('blog.destroy',$s)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

