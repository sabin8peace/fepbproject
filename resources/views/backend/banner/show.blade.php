@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>banner Detail</label>
                        <a href="{{route('banner.index')}}" class="btn btn-primary pull-right">banner List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$banner->title}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/banner_upload/'.$banner->image)}}" height="100" width="100"></td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($banner->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$banner->description!!}</td>
                            </tr>

                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($banner->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($banner->updated_by))
                                        {{\App\User::find($banner->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$banner->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$banner->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('banner.edit',$banner)}}" class="btn btn-warning">Edit banner</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('banner.destroy',$banner)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

