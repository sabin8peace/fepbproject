@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>general Detail</label>
                        <a href="{{route('general.index')}}" class="btn btn-primary pull-right">general List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$general->title}}</td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($general->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$general->short_description!!}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$general->note!!}</td>
                            </tr>

                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($general->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($general->updated_by))
                                        {{\App\User::find($general->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$general->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$general->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('general.edit',$general)}}" class="btn btn-warning">Edit general</a>
                                </td>
                                <td>
                                    <form action="{{route('general.destroy',$general)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

