@extends('adminlte::page')
@section('css')
    {{--  <link type="text/css" rel="stylesheet" href="{{asset('assets/datatables/css/jquery.dataTables.min.css')}}">  --}}
@endsection

@section('javascript')
    {{--  <script type="text/javascript" src="{{asset('assets/datatables/js/jquery.dataTables.min.js')}}"></script>  --}}
    {{--  <script type="text/javascript">
        $(document).ready(function(){
            $('#yojanaKaryakram_table').DataTable();
        });
    </script>  --}}
@endsection

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>योजना कार्यक्रम List</label>
                        <a href="{{route('yojanaKaryakram.create')}}" class="btn btn-primary pull-right">Create योजना</a>
                        <a href="{{route('yojanaKaryakram.karyakram')}}" class="btn btn-primary pull-right">Create कार्यक्रम</a>
                    </div>

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table class="table table-bordered" id="yojanaKaryakram_table">
                            <thead>
                            <tr>

                                <th>SN</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($yojanaKaryakram as $s)
                                <tr>

                                    <td>{{$i++}}</td>
                                    <td>{{$s->title}}</td>
                                    <td>{{$s->category}}</td>

                                    <td>
                                        @if($s->status==1)
                                            <label class="label label-success">Active</label>
                                        @else
                                            <label class="label label-danger">Inactive</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('yojanaKaryakram.edit',$s)}}" class="btn btn-warning btn-block">Edit</a>
                                        <a href="{{route('yojanaKaryakram.show',$s)}}" class="btn btn-info btn-block">View Detail</a>

                                        <form action="{{route('yojanaKaryakram.destroy',$s)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

