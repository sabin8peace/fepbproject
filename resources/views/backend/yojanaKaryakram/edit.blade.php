@extends('adminlte::page')
@section('js')
    <script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('assets/jqueryvalidation/dist/jquery.validate.min.js')}}"></script>--}}
    {{--<script type="text/javascript">--}}
    {{--$(document).ready(function () {--}}
    {{--$('#slider_form').validate({--}}
    {{--rules: {--}}
    {{--'description': {--}}
    {{--required: true--}}
    {{--}--}}
    {{--},--}}
    {{--messages: {--}}
    {{--title: "This field is required.",--}}
    {{--link: "This field is required.",--}}
    {{--image: "This field is required.",--}}
    {{--rank: "This field is required.",--}}
    {{--description: "This field is required."--}}
    {{--},--}}
    {{--ignore: []--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Edit Slider</label>
                        <a href="{{route('slider.index')}}" class="btn btn-primary pull-right">Slider List</a>
                    </div>

                    <div class="panel-body">
                        <form method="post" action="{{route('slider.update',$slider)}}" method="post" id="slider_form">
                            <input type="hidden" name="_method" value="put">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type ="text" class="form-control" name ="title" value="{{$slider->title}}" id="title" required>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type ="text" class="form-control" name ="image" value="{{$slider->image}}" id="image" required>
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="rank">Rank</label>
                                <input type ="number" class="form-control" name ="rank" value="{{$slider->rank}}" id="rank" required>
                                @if ($errors->has('rank'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rank') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                @if($slider->status==1)

                                    <input type ="radio"  name ="status" value="1" checked>Active
                                    <input type ="radio"  name ="status" value="0" > Inactive
                                @else
                                    <input type ="radio"  name ="status" value="1" >Active
                                    <input type ="radio"  name ="status" value="0" checked>Inactive
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor " name ="description" id="description" required>{{$slider->description}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <!-- <label>Created By</label>-->
                                <input type ="hidden" class="form-control" name ="created_by" value="{{$slider->created_by}}">

                            </div>
                            <div class="form-group">
                                <!-- <label>Updated By</label>-->
                                <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">

                            </div>

                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Update Slider">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

