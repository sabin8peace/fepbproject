@extends('adminlte::page')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Contact Detail</label>
                        <a href="{{route('contact.index')}}" class="btn btn-primary pull-right">Contact List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$contact->name}}</td>
                            </tr>

                            <tr>
                                <th>Email</th>
                                <td>{{$contact->email}}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>{{$contact->phone}}</td>
                            </tr>
                            <tr>
                                <th>Subject</th>
                                <td>{{$contact->subject}}</td>
                            </tr>

                            <tr>
                                <th>Message</th>
                                <td>{{$contact->message}}</td>
                            </tr>

                            <tr>
                                <th>Date</th>
                                <td>{{$contact->date}}</td>
                            </tr>

                            <tr>
                                <th>Remarks</th>
                                <td>{{$contact->remarks}}</td>
                            </tr>

                            <tr>
                                <th>Created At</th>
                                <td>{{$contact->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$contact->updated_at}}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{route('contact.edit',$contact)}}" class="btn btn-warning">Edit Contact</a>
                                </td>
                                <td>
                                    <form action="{{route('contact.destroy',$contact)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>


                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
