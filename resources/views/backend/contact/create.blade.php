@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create contact</label>
                        <a href="{{route('contact.index')}}" class="btn btn-primary pull-right">contact List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($contact->id))
                            <form method="post" action="{{route('contact.store')}}"  enctype="multipart/form-data" id="valid_form" >
                                {{csrf_field()}}

                                @else
                                <form method="post" action="{{route('contact.update',$contact)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">
                                        {{csrf_field()}}


                             @endif




                            <div class="form-group">
                                <label for="name">Name</label>

                                <input type ="text" class="form-control" name ="name" id="name" value="{{$contact->name}}" required >
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>





                            <div class="form-group">
                                <label for="phone1">Phone</label>
                                <input type ="text" class="form-control" name ="phone" id="phone"  value="{{$contact->phone}}"   required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <label for="email"> Email</label>
                                <input type ="email" class="form-control" name ="email" id="email" value="{{$contact->email}}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="subject">subject</label>
                                <input type ="text" class="form-control" name ="subject" id="subject" value="{{$contact->subject}}">
                                @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control ckeditor" name ="message" id="message"  required>{!!$contact->message!!} </textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save contact">

                            </div>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
