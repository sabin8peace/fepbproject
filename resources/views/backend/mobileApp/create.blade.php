@extends('layouts.backmaster')



@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create mobileApp</label>
                        <a href="{{route('mobileApp.index')}}" class="btn btn-primary pull-right">mobileApp List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($mobileApp->id))
                            <form method="post" action="{{route('mobileApp.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('mobileApp.update',$mobileApp)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$mobileApp->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type ="text" class="form-control" name ="title" id="title" value="{{$mobileApp->title}}" required>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="link">link</label>
                                <input type ="text" class="form-control" name ="link" id="link" value="{{$mobileApp->link}}" required>
                                @if ($errors->has('link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="image">image</label>

                                <input type="file" name="image" id="image" class="form-control" value="{{$mobileApp->image}}">

                                <td><img src="{{asset('/mobileApp_upload/'.$mobileApp->image)}}" height="100" width="100"></td>

                            </div>
                            <div class="form-group">
                                <label for="mobile_image">Mobile image</label>

                                <input type="file" name="mobile_image" id="mobile_image" class="form-control" value="{{$mobileApp->mobile_image}}">

                                <td><img src="{{asset('/mobileApp_upload/'.$mobileApp->mobile_image)}}" height="100" width="100"></td>

                            </div>



                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$mobileApp->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea class="form-control ckeditor" name ="short_description" id="short_description"  required>{!!$mobileApp->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save mobileApp">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
