@extends('layouts.backmaster')
@section('css')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/datatables/css/jquery.dataTables.min.css')}}">
@endsection

@section('javascript')
    <script type="text/javascript" src="{{asset('assets/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#category_table').DataTable();
        });
    </script>
@endsection
@section('content')
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label> List</label>
                        <a href="{{url('backend/create_category'.'/'.$single_category)}}" class="btn btn-primary pull-right">Create Category</a>
                    </div>

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table  class="table table-bordered" id="category_table">
                            <thead>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Status</th>

                            <th>Action</th>
                            </thead>
                            <tbody>
                            @php($i=1)
                                @foreach($product_category as $c)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$c->name}}</td>
                                       <td>{{$c->maincategory}}</td>

                                            <td>
                                            @if($c->status==1)
                                                <label class="label label-success">Active</label>
                                            @else
                                                <label class="label label-danger">Inactive</label>
                                            @endif
                                        </td>

                                        <td>
                                            <a href="{{route('product_category.edit',$c)}}" class="btn btn-warning btn-block"><i class="fa fa-edit">&nbsp;&nbsp;&nbsp;Edit</i></a>
                                            <form action="{{route('product_category.destroy',$c)}}" method="post"  onsubmit="return confirm('Are You Sure??')">
                                                <input type="hidden" name="_method" value="delete" >
                                                {{csrf_field()}}
                                                <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-trash">&nbsp;&nbsp;&nbsp; Delete</i></button>
                                            </form>
                                        </td>
                                    </tr>


                                @endforeach

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
@endsection
