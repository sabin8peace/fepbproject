@extends('layouts.backmaster')

@section('content')
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Edit Information</label>
                        {{--  <a href="{{route('product_category.index')}}" class="btn btn-primary pull-right">View List</a>  --}}
                        <a href="{{url('/category_index'.'/'.$product_category->maincategory)}}" class="btn btn-primary pull-right">View List</a>

                    </div>

                    <div class="panel-body">


                        <form action="{{route('product_category.update',$product_category)}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group ">
                                <label for="name">Title</label>
                                <input class="form-control" type="text" name="name" id="name" value="{{$product_category->name}}" required>


                            </div>
                            <div class="form-group ">
                                <label for="maincategory">Information Type</label>
                                <?php $tmp=App\ProductCategory::find($product_category->maincategory);?>
                                <input type ="text" class="form-control" name ="maincategory" id="maincategory" value="{{$product_category->maincategory}}" readonly>

                                {{--  <select class="form-control myselect" name="maincategory" id="maincategory">
                                    <option selected value="{{$product_category->maincategory}}">{{$product_category->maincategory}}</option>
                                @foreach($main_category as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>  --}}
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="radio" name="status" id="status" value="1" checked>Active
                                <input type="radio" name="status" id="status" value="0"  >Inactive
                            </div>



                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Update Information">

                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection

