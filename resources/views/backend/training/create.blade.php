@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('training.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($training->id))
                            <form method="post" action="{{route('training.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('training.update',$training)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$training->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type ="text" class="form-control" name ="title" id="title" value="{{$training->title}}" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <span class="data-label" for="category">Select Type</span>
                                    <select class="form-control myselect" name="category_id" id="category_id" required>
                                        @if (isset($training->id))
                                        {{-- <option selected value="{{$training->category_id}}">{{App\ProductCategory::find($training->category_id)->name}}</option> --}}
                                        <option selected value="{{$training->category_id}}">{{$training->product_category->name}}</option>
                                            @else
                                            <option selected value="">Select </option>

                                        @endif
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
    
                            </div>
                            {{--  <div class="form-group">
                                <label for="filetype">filetype</label>
                                <input type ="text" class="form-control" name ="filetype" id="filetype" value="{{$training->filetype}}" required>
                                @if ($errors->has('filetype'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('filetype') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}
                            {{-- <div class="form-group">
                                <label for="date">date</label>
                                <input type ="text" class="form-control" name ="date" id="date" value="{{$training->date}}" required>
                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif

                            </div> --}}

                            {{-- <div class="form-group">
                                <label for="image">image</label>

                                <input type="file" name="image" id="image" class="form-control" value="{{$training->image}}">

                                <td><img src="{{asset('/training_upload/'.$training->image)}}" height="100" width="100"></td>

                            </div> --}}
                            {{-- <div class="form-group">
                                <label for="other_file">Files</label>

                                <input type="file" name="other_file" id="other_file" class="form-control" value="{{$training->other_file}}">

                                <td><a href="{{asset('/training_upload/'.$training->other_file)}}" target="_blank">{{asset('/training_upload/'.$training->other_file)}}</a></td>

                            </div> --}}
                            {{--  <div class="form-group">
                                <span class="data-label" for="category">training Type</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                        <option selected value="{{$training->category}}">{{$training->category}}</option>
                                        @foreach($trainingcategory as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>  --}}
                        {{-- <div class="form-group">
                                <span class="data-label" for="category"> category</span>
                                <select class="form-control myselect" name="category" id="category" required>

                                        <option value="{{$training->category}}">{{$training->category}}</option>

                                        <option value="video" >Video</option>
                                        <option value="audio" >Audio</option>
                                        <option value="photo" >Photo</option>

                                </select>

                        </div> --}}
                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$training->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            {{-- <div class="form-group">
                                <label for="short_description">Link or source</label>
                                <textarea class="form-control " name ="short_description" id="short_description"  required>{!!$training->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div> --}}


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save training">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
