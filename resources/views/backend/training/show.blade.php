@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>training Detail</label>
                        <a href="{{route('training.index')}}" class="btn btn-primary pull-right">training List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$training->title}}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>{{$training->product_category->name}}</td>
                            </tr>
                            {{-- <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/training_upload/'.$training->image)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Other File</th>
                                <td><a href="{{asset('/training_upload/'.$training->other_file)}}" target="_blank">{{asset('/training_upload/'.$training->other_file)}}</a></td>
                            </tr> --}}



                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($training->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$training->description!!}</td>
                            </tr>
                            {{-- <tr>
                                <th>Short Description</th>
                                <td>{!!$training->short_description!!}</td>
                            </tr> --}}
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($training->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($training->updated_by))
                                        {{\App\User::find($training->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$training->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$training->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('training.edit',$training)}}" class="btn btn-warning">Edit training</a>
                                </td>
                                <td>
                                    <form action="{{route('training.destroy',$training)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

