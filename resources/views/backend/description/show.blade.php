@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>description Detail</label>
                        <a href="{{route('description.index')}}" class="btn btn-primary pull-right">description List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <thead>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                    @foreach ($description as $i=>$item)
                                    <tr>
                                            <td>{!!$item->title!!}</td>
                                            <td>
                                                    @if (strlen($item->description) <=150)   
                                                    {!!$item->description!!}   
                                                @else     {!!substr($item->description, 0, 150) . '...'!!}  
                                                 @endif
                                                {{-- {!!$item->description!!} --}}
                                            </td>
                                            <td>  @if($item->status==1)
                                                    <label class="label label-success">Active</label>
                                                @else
                                                    <label class="label label-danger">Inactive</label>
                                                @endif
                                            </td>
                                            <td>
                                                    <form action="{{route('description.destroy',$item)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                                            <input type="hidden" name="_method" value="delete">
                                                            {{csrf_field()}}
                                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                                        </form>
                                            </td>
                                        </tr>                                 
                                              
        
                                    @endforeach
                             
                            </tbody>
                           

  
                            


                            {{-- <tr>
                                <td>
                                    <a href="{{route('description.edit',$description)}}" class="btn btn-warning">Edit description</a>
                                </td>
                                <td>
                                    <form action="{{route('description.destroy',$description)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr> --}}
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

