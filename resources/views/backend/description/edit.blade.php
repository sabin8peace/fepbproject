@extends('adminlte::page')
@push('js')

<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script>
          $('.input_fields_wrap').hide();



        $(document).ready(function() {
            $(".more_field").click(function(e) {
                {
                   e.preventDefault();
                    $('.more_field').hide();
                    $('.input_fields_wrap').show();

                }
            });
            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment

                    $(wrapper).append('<div><label>Main Title:</label><textarea class="form-control " name="maintitle[]" required></textarea><br><label>ShortIndividual  Description</label><textarea id="shorteditor'+x+'"  class="form-control"  name ="short_description[]" required></textarea><br><label>Main Title:</label><textarea class="form-control " name="title[]" required></textarea><br><label>Individual Description</label><textarea id="editor'+x+'"  class="form-control"  name ="description[]" required></textarea><br><br><label>Note</label><textarea id="noteeditor'+x+'"  class="form-control"  name ="note[]" required></textarea><br><button class="remove_field button">Remove</button></div>'); //add input box
                }
                addTinyMCE();
            });


                    $(wrapper).append('<div><label> Title:</label><textarea class="form-control " name="maintitle[]" ></textarea><br><label>Short Description:</label><textarea class="form-control " name="title[]" ></textarea><br><label>Prakriya</label><textarea id="shorteditor'+x+'"  class="form-control"  name ="short_description[]" ></textarea><br><label>Aawashyak Paper</label><textarea id="editor'+x+'"  class="form-control"  name ="description[]" ></textarea><br><br><label>Note</label><textarea id="noteeditor'+x+'"  class="form-control"  name ="note[]" ></textarea><br><button class="remove_field button">Remove</button></div>'); //add input box
                }
                addTinyMCE();
            });

            $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            });
            function addTinyMCE() {
                // Initialize
                CKEDITOR.replace('editor'+x);
                CKEDITOR.replace('shorteditor'+x);
                CKEDITOR.replace('noteeditor'+x);
            }
        });


    </script>

@endpush

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('description.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">

                                <form method="post" action="{{route('description.updatestore')}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        {{--  <input type="hidden" name="_method" value="put">  --}}
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                            {{csrf_field()}}
                           <div class="form-group">
                                <span class="data-label" for="category">Description Type</span>
                                <select class="form-control myselect" name="category_id" id="category_id" required>
                                        <option selected value="{{$category_id}}">{{App\ProductCategory::find($category_id)->name}}</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>

                            @foreach ($description as $i=>$item)
                            <div>

                                    <label>Main Title:</label><textarea class="form-control " name="maintitle[]" id="edittitle{{$i}}" required>{!!$item->maintitle!!}</textarea>
                                    <label>Individual Title:</label><textarea class="form-control " name="title[]" id="edittitle{{$i}}" required>{!!$item->title!!}</textarea><br>
                                    <br><label>Short Description</label><textarea class="form-control ckeditor" name="short_description[]" id="shortdescription{{$i}}" required>{!!$item->short_description!!}</textarea>
                                    <br><label>Description</label><textarea class="form-control ckeditor" name="description[]" id="editdescription{{$i}}" required>{!!$item->description!!}</textarea>

                                    <label> Title:</label><textarea class="form-control " name="maintitle[]" id="edittitle{{$i}}" required>{!!$item->maintitle!!}</textarea>
                                    <label>Short Descripiton:</label><textarea class="form-control " name="title[]" id="edittitle{{$i}}" required>{!!$item->title!!}</textarea><br>
                                    <br><label>Prakreya</label><textarea class="form-control ckeditor" name="short_description[]" id="shortdescription{{$i}}" required>{!!$item->short_description!!}</textarea>
                                    <br><label>Aawashyak Paper</label><textarea class="form-control ckeditor" name="description[]" id="editdescription{{$i}}" required>{!!$item->description!!}</textarea>

                                    <br><label>Note</label><textarea class="form-control ckeditor" name="note[]" id="notedescription{{$i}}" required>{!!$item->note!!}</textarea>

                                </div>
                                <input type="hidden" value="{{$i}}" name="count" id="count">
                                <input type="hidden" value="{{$item->id}}" name="editid[]" id="edit{{$i}}">
                                <br>


                            @endforeach

                            <hr>
                        <button class="more_field">Add More Fields</button>

                        <div class="input_fields_wrap">
                                <button class="add_field_button">Add More Fields</button>
                                <div>
                                    <label> Individual MainTitle:</label><textarea class="form-control " name="maintitle[]" ></textarea>
                                    <br><label>Individual Short Description</label><textarea class="form-control ckeditor" name="short_description[]" ></textarea>
                                    <br><label>Individual Title:</label><textarea class="form-control " name="title[]" ></textarea>
                                    <br><label>Individual Description</label><textarea class="form-control ckeditor" name="description[]" ></textarea>
                                    <br><label>Individual Note</label><textarea class="form-control ckeditor" name="note[]" ></textarea>
                                </div>
                            </div>


                            <div class="col-md-12 col-lg-12">
                                <input class="btn btn-success form-control" type ="submit" name ="submit" value="Save ">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
