@extends('adminlte::page')
@push('js')
{{--  <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>  --}}
{{--  <script type="text/javascript" src="{{asset('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<script>
    tinymce.init({
      selector: '#mytextarea'
    });
    </script>  --}}
<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>

{{--  <script>
        $('.multi-field-wrapper').each(function() {
            var $wrapper = $('.multi-fields', this);
            $(".add-field", $(this)).click(function (e) {
                var a =  $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('select').val('').focus();
                //a.attr('id','abcd');

            });
            $('.multi-field .remove-field', $wrapper).click(function () {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent('.multi-field').remove();
            });
        });


</script>  --}}

{{--  google  --}}
<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="text" name="mytext[]"/><textarea class="form-control ckeditor" id="mytextarea" name ="title[]" required></textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
{{--  google  --}}

@endpush

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('description.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($description->id))
                            <form method="post" action="{{route('description.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('description.update',$description)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$description->created_by}}">
                             @endif
                            {{csrf_field()}}
                           <div class="form-group">
                                <span class="data-label" for="category">Description Type</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                        <option selected value="{{$description->general_id}}">{{$description->general_id}}</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->title}}">{{$category->title}}</option>
                                    @endforeach
                                </select>

                        </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            {{--  <div class="multi-fields">
                                <div class="multi-field-wrapper">
                                    <div class="col-md-2 col-md-offset-10">
                                         <button type="button" class="add-field btn btn-success"><i class="fa fa-plus"></i> </button>
                                    </div>
                                    <div class="multi-fields">
                                        <div class="multi-field">
                                                    <div >
                                                            <label for="title">Title</label>
                                                            <textarea class="form-control" name ="title[]" required></textarea>
                                                        </div>
                                                <div>
                                                    <label for="description"> Description</label>
                                                    <textarea id="mytextarea" class="form-control ckeditor" name ="description[]" required></textarea>
                                                </div>
                                            <button type="button" class="remove-field btn btn-danger form-control col-md-4 col-lg-4"><i class="fa fa-times"></i> </button>
                                            <br>

                                        </div>
                                    </div>
                                </div>

                            </div>  --}}
                            {{--  google  --}}
                            <div class="input_fields_wrap">
                                <button class="add_field_button">Add More Fields</button>
                                <div><input type="text" name="mytext[]"><br><textarea class="form-control ckeditor" id="mytextarea" name ="title[]" required></textarea></div>
                            </div>
                            {{--  google  --}}

                            <div class="col-md-12 col-lg-12">
                                <input class="btn btn-success form-control" type ="submit" name ="submit" value="Save ">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
