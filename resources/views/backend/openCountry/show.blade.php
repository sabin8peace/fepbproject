@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>openCountry Detail</label>
                        <a href="{{route('openCountry.index')}}" class="btn btn-primary pull-right">openCountry List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>country_name</th>
                                <td>{{$openCountry->country_name}}</td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($openCountry->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>next_field</th>
                                <td>{{$openCountry->next_field}}</td>
                            </tr>

                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($openCountry->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($openCountry->updated_by))
                                        {{\App\User::find($openCountry->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$openCountry->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$openCountry->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('openCountry.edit',$openCountry)}}" class="btn btn-warning">Edit openCountry</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('openCountry.destroy',$openCountry)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

