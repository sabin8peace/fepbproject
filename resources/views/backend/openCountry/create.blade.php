@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('openCountry.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($openCountry->id))
                            <form method="post" action="{{route('openCountry.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('openCountry.update',$openCountry)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$openCountry->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="country_name">country name</label>
                                    <input type ="text" class="form-control" name ="country_name" id="country_name" value="{{$openCountry->country_name}}" required>
                                    @if ($errors->has('country_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country_name') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            <div class="form-group">
                                    <label for="next_field">next_field</label>
                                    <input type ="text" class="form-control" name ="next_field" id="next_field" value="{{$openCountry->next_field}}" required>


                                </div>


                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>




                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save openCountry">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
