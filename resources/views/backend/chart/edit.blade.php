@extends('adminlte::page')
@push('js')

<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script>
        $('.multi-field-wrapper').hide();



        $(document).ready(function() {
            $(".more_field").click(function(e) {
                {
                   e.preventDefault();
                    $('.more_field').hide();
                    $('.multi-field-wrapper').show();

                }
            });
       

         
        });

      $('.multi-field-wrapper').each(function() {
            var $wrapper = $('.multi-fields', this);
            $(".add-field", $(this)).click(function (e) {
                var a =  $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('select').val('').focus();
                //a.attr('id','abcd');

            });
            $('.multi-field .remove-field', $wrapper).click(function () {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent('.multi-field').remove();
            });
        });

    </script>

@endpush

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('chart.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">

                                <form method="post" action="{{route('chart.updatestore')}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        {{--  <input type="hidden" name="_method" value="put">  --}}
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                            {{csrf_field()}}
                           <div class="form-group">
                                <span class="data-label" for="category">chart Type</span>
                                <select class="form-control myselect" name="category_id" id="category_id" required>
                                        <option selected value="{{$category_id}}">{{\App\ProductCategory::find($category_id)->name}}</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>

                            @foreach ($chart as $i=>$item)
                            <div>
                                    <input type ="text"  name ="fiscal[]" value="{{$item->fiscal}}"   placeholder="Fiscal Year">
                                    <input type ="text"  name ="male[]" value="{{$item->male}}"   placeholder="Male Number">
                                    <input type ="text"  name ="female[]" value="{{$item->female}}"   placeholder="Female Number">
                                    <input type ="text"  name ="total[]" value="{{$item->total}}"  placeholder="Total Number">
                                   
                                    {{-- <label>Fiscal:</label><textarea class="form-control " name="fiscal[]" id="edittitle{{$i}}" required>{!!$item->title!!}</textarea> --}}
                                    {{-- <br><label>chart</label><textarea class="form-control ckeditor" name="chart[]" id="editchart{{$i}}" required>{!!$item->chart!!}</textarea> --}}
                            </div>
                                <input type="hidden" value="{{$i}}" name="count" id="count">
                                <input type="hidden" value="{{$item->id}}" name="editid[]" id="edit{{$i}}">
                                <br>


                            @endforeach

                            <hr>
                        <button class="more_field" onclick="showdiv()">Add More Fields</button>

                        <div class="multi-fields">
                            <div class="multi-field-wrapper">
                                <div class="col-md-2 col-md-offset-10"> <button type="button" class="add-field btn btn-success"><i class="fa fa-plus"></i> </button>
                                </div>
                                <div class="multi-fields">
                                    <div class="multi-field">
                                            {{-- <label>Fiscal Year:</label> --}}
                                            <input type ="text"  name ="fiscal[]"   placeholder="Fiscal Year">
                                            <input type ="text"  name ="male[]" value="0"   placeholder="Male Number">
                                            <input type ="text"  name ="female[]" value="0"   placeholder="Female Number">
                                            <input type ="text"  name ="total[]"   placeholder="Total Number">
                                           
                                        <button type="button" class="remove-field btn btn-danger"><i class="fa fa-times"></i> </button>
                                    </div>
                                </div>
                            </div>

                        </div>



                            <div class="col-md-12 col-lg-12">
                                <input class="btn btn-success form-control" type ="submit" name ="submit" value="Save ">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
