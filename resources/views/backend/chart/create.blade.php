@extends('adminlte::page')
@push('css')
<style>
    .multi-field
    {
        margin-top: 20px;
    }
</style>
@endpush
@push('js')

<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script>

        $('.multi-field-wrapper').each(function() {
            var $wrapper = $('.multi-fields', this);
            $(".add-field", $(this)).click(function (e) {
                var a =  $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('select').val('').focus();
                //a.attr('id','abcd');

            });
            $('.multi-field .remove-field', $wrapper).click(function () {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent('.multi-field').remove();
            });
        });

    </script>

@endpush

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('chart.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($chart->id))
                            <form method="post" action="{{route('chart.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('chart.update',$chart)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$chart->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                <span class="data-label" for="category"> Type</span>
                                <select class="form-control myselect" name="category_id" id="category_id" required>
                                    @if (isset($chart->id))
                                    {{-- <option selected value="{{$chart->category_id}}">{{App\ProductCategory::find($chart->category_id)->name}}</option> --}}
                                    <option selected value="{{$chart->category_id}}">{{$chart->product_category->name}}</option>
                                        @else
                                        <option selected value="">Select </option>

                                    @endif
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>

                                <div class="multi-fields">
                                        <div class="multi-field-wrapper">
                                            <div class="col-md-2 col-md-offset-10"> <button type="button" class="add-field btn btn-success"><i class="fa fa-plus"></i> </button>
                                            </div>
                                            <div class="multi-fields">
                                                <div class="multi-field">
                                                        {{-- <label>Fiscal Year:</label> --}}
                                                        <input type ="text"  name ="fiscal[]"  required placeholder="Fiscal Year">
                                                        <label>M</label><input type ="text"  name ="male[]" value="0"  required placeholder="Male Number">
                                                        <label>F</label><input type ="text"  name ="female[]" value="0"  required placeholder="Female Number">
                                                        <input type ="text"  name ="total[]"  required placeholder="Total Number">
                                                       
                                                    <button type="button" class="remove-field btn btn-danger"><i class="fa fa-times"></i> </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                            <div class="col-md-12 col-lg-12">
                                <input class="btn btn-success form-control" type ="submit" name ="submit" value="Save ">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
