@extends('layouts.backmaster')



@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create बोर्डको सचिवालय </label>
                        <a href="{{route('boardSachibalaya.index')}}" class="btn btn-primary pull-right">बोर्डको सचिवालय List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($boardSachibalaya->id))
                            <form method="post" action="{{route('boardSachibalaya.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('boardSachibalaya.update',$boardSachibalaya)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$boardSachibalaya->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type ="text" class="form-control" name ="title" id="title" value="{{$boardSachibalaya->title}}" >
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="post">post</label>
                                <input type ="text" class="form-control" name ="post" id="post" value="{{$boardSachibalaya->post}}" >
                                @if ($errors->has('post'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('post') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="resource">resource</label>
                                <input type ="text" class="form-control" name ="resource" id="resource" value="{{$boardSachibalaya->resource}}" >
                                @if ($errors->has('resource'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('resource') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name ="description" id="description"  required>{!!$boardSachibalaya->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>



                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Information">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
