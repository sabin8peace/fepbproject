<html lang="en">
<head>
    <title>Laravel 5.6 Import Export to Excel and csv Example - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
</head>
<body>
    <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
          <h1>Laravel 5.6 Import Export to Excel and CSV Example - ItSolutionStuff.com</h1>
          </div>
          <div class="panel-body">

            <a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
            <a href="{{ url('downloadExcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
            <a href="{{ url('downloadExcel/csv') }}"><button class="btn btn-success">Download CSV</button></a>


            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif

                {{--  <input type="file" name="import_file" />
                <button class="btn btn-primary">Import File</button>
            </form>  --}}

                Choose your xls/csv File : <input type="file" name="file" class="form-control">

                <input type="submit" class="btn btn-primary btn-lg" style="margin-top: 3%" value="Submit second">
            </form>

          </div>
        </div>
    </div>
</body>
</html>
