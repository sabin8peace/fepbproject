<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table='blogs';
    protected $fillable=[
        'title',
        'category',
        'status',
        'description',
        'date',
        'image',
        'other_file',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
