<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table='product_categories';
    protected  $fillable=['name','maincategory','status'];

    public function trainings()
    {
        return $this->hasMany('App\Training','category_id');
    }
    public function charts()
    {
        return $this->hasMany('App\Chart','category_id');
    }
    public function descriptions()
    {
        return $this->hasMany('App\Description','category_id');
    }


}
