<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YojanaKaryakram extends Model
{
    protected $table='yojana_karyakrams';
    protected $fillable=[
        'title',
        'category',
        'status',
        'description',
        'date',
        'image',
        'other_file',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
