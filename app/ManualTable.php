<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManualTable extends Model
{
    protected $table='manual_tables';
    protected $fillable=[
        'title',
        'general_id',
        'description',
        'table_name',

    ];
    public function tableentries()
    {
        return $this->hasMany('App\TableEntry');
    }
    public function general()
    {
        return $this->belongsTo('App\General');
    }

}
