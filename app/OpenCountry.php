<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenCountry extends Model
{
    protected $table='open_countries';
    protected $fillable=[    
        'country_name',
        'next_field',              
        'status',
        'created_by',
        'updated_by'
        ];
}
