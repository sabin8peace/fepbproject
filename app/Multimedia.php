<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    protected $table='multimedia';
    protected $fillable=[
        'title',
        'category',
        'status',
        'description',
        'date',
        'image',
        'other_file',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
