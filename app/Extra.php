<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
    protected $table='extras';
    protected $fillable=[
        'title',
        'category',
        'status',
        'description',
        'date',
        'filetype',
        'image',
        'other_file',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
