<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelFile extends Model
{
    protected $table='excel_files';
    protected $fillable=[
        'title',
        'category',
        'description',
        'date',
        'other_file',
        'short_description',

    ];
}
