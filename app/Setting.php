<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='setting';
    protected $fillable=[
        'logo',
        'name',
        'address',
        'phone1',
        'phone2',
        'tollfree',
        'email',
        'fax',
        'facebook_link',
        'twitter_link',
        'linkedin_link',
        'youtube_link',
        'website',
        'created_by',
        'updated_by'

    ];
}
