<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportantLink extends Model
{
    protected $table='important_links';
    protected $fillable=[
        'title',
        'status',
        'link',
        'created_by',
        'updated_by'
    ];
}
