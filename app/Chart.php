<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $table='charts';
    protected $fillable=[
        'title',
        'category',
        'category_id',
        'status',
        'desscription',
        'date',
        'fiscal',
        'male',
        'female',
        'total',
        'created_by',
        'updated_by'
    ];
    public function product_category()
    {
        return $this->belongsTo('App\ProductCategory','category_id');
    }
}
