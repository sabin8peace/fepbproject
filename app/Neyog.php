<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neyog extends Model
{
    protected $table='neyogs';
    protected $fillable=[
        'mission',
        'country',
        'website',
        'email',
        'phone',
        'address',        
        'status',
        'created_by',
        'updated_by'
        ];
}
