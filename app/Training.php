<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $table='trainings';
    protected $fillable=[
        'title',
        'category',
        'category_id',
        'status',
        'description',
        'date',
        'image',
        'other_file',
        'short_description',
        'created_by',
        'updated_by'
    ];
    public function product_category()
    {
        return $this->belongsTo('App\ProductCategory','category_id');
    }
}
