<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table='abouts';
    protected $fillable=[
        'title',
        'status',
        'description',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
