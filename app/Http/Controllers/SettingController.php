<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $setting=\App\Setting::all();
        return view('backend.setting.index',compact('setting'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = new Setting();
        return view('backend.setting.create',compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $logo='';
        if(!empty($request->file('logo'))){
            $file =$request->file('logo');
            $path=base_path().'/public/setting_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $logo=$name;
                $input['logo']=$logo;
            }
        }

        $status=Setting::create($input);


        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        return view('backend.setting.show',compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        // $user = User::find($id);
        // $action = URL::route('setting.update',$setting);
        // dd($setting->logo);
        return view('backend.setting.create',compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {

        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('logo')))
        {
            $file=$request->file('logo');
            $path=base_path().'/public/setting_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name))
            {
                $image=$name;

            }
            $input['logo'] = $image;

        }
        // dd($input);

        $status=$setting->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        $status=$setting->delete();
        if($status){
            Session::flash('success','Setting deleted successfully.');
        }else{
            Session::flash('error','Setting cannot be deleted.');
        }
        return redirect('backend/setting');
    }
}
