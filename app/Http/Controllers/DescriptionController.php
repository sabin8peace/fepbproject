<?php

namespace App\Http\Controllers;

use App\Description;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $description=Description::orderBy('id','desc')->get();
        $description=Description::groupBy('category_id')->get();
        return view('backend.description.index',compact('description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $description = new Description();
        $categories=DB::table('product_categories')->where('maincategory','Board Welfare Work')->orderBy('id','desc')->get();
        // return view('backend.general.create',compact('general','categories'));
        // $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.description.create',compact('description','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatestore(Request $request)
    {    $title=$request->input('title');
        $maintitle=$request->input('maintitle');
        $description=$request->input('description');
        $short_description=$request->input('short_description');
        $note=$request->input('note');
        $tmpid=$request->input('editid');
        for($i = 0;$i < count($title);$i++){
            if($title[$i]!=null)
            {
                if (isset($tmpid[$i])) {
                    $tmpstore=Description::find($tmpid[$i]);
                    $tmpstore->maintitle=$maintitle[$i];
                    $tmpstore->title=$title[$i];
                    $tmpstore->note=$note[$i];
                    $tmpstore->short_description=$short_description[$i];
                    $tmpstore->description=$description[$i];
                    $tmpstore->category_id= $request->input('category_id');
                    $tmpstore->status=$request->input('status');                    
                    $status=$tmpstore->update();
                }
                else
                {
                    $status=Description::create([
                        'category_id' => $request->input('category_id'),
                        'maintitle'=> $maintitle[$i],
                        'title'=> $title[$i],
                        'description' => $description[$i],
                        'note' => $note[$i],
                        'short_description' => $short_description[$i],
                        'created_by'=>$request->input('created_by'),
                        'status'=>$request->input('status'),
                    ]);

                }



            }
        }

        // $status=Description::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/description');


    }
    public function store(Request $request)
    {
        // dd($request);
        // $input=$request->input('category');
        // $input=$request->input('category');
        $title=$request->input('title');
        $maintitle=$request->input('maintitle');
        $description=$request->input('description');
        $short_description=$request->input('short_description');
        $note=$request->input('note');
        for($i = 0;$i < count($title);$i++){
            $status=Description::create([
                'category_id' => $request->input('category_id'),
                'maintitle'=> $maintitle[$i],
                'title'=> $title[$i],
                'description' => $description[$i],
                'note' => $note[$i],
                'short_description' => $short_description[$i],
                'created_by'=>$request->input('created_by'),
                'status'=>$request->input('status'),
            ]);
        }

        // $status=Description::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/description');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function show(Description $description)
    {
        $category_id=$description->category_id;
        $name=ProductCategory::find($category_id)->name;
        // dd($name);
        $description=Description:: where('category_id',$category_id)->orderBy('id','desc')->get();
        // dd($description);
        $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.description.show',compact('description','categories'));

        // return view('backend.description.show',compact('description'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function edit(Description $description)
    {
        $category_id=$description->category_id;
        $name=ProductCategory::find($category_id)->name;
        // dd($name);
        $description=Description:: where('category_id',$category_id)->orderBy('id','desc')->get();
        // dd($description);
        $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.description.edit',compact('description','categories','category_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Description $description)
    {
        dd($request);
        $title=$request->input('title');
        $description=$request->input('description');
        for($i = 0;$i < count($title);$i++){
            $status=Description::create([
                'category_id' => $request->input('category'),
                'title'=> $title[$i],
                'description' => $description[$i],
                'created_by'=>$request->input('created_by')
            ]);
        }

        // $status=Description::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/description');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Description  $description
     * @return \Illuminate\Http\Response
     */
    public function destroy(Description $description)
    {
        $category=$description->category_id;
        $status=$description->delete();

        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        // return redirect('backend/description');
        return $this->showfn($category);
    }
    public function showfn( $category_id)
    {
        // $category_id=$chart->category_id;
        $category_id=$category_id;
        $name=ProductCategory::find($category_id)->name;
        // dd($name);
        $description=Description:: where('category_id',$category_id)->orderBy('id','desc')->get();
        // dd($description);
        $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.description.show',compact('description','categories'));

        // $chart=Chart:: where('category_id',$category_id)->orderBy('id','desc')->get();
        // return view('backend.chart.show',compact('chart','category_id'));

    }
}
