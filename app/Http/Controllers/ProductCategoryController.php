<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_category = ProductCategory::all();
        return view('backend.product_category.index',compact('product_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $single_category='sabin';
        $main_category= ProductCategory::all();

        return view('backend.product_category.create',compact('main_category','single_category'));

    }
    public function categoryindex($type)
    {
        $single_category=$type;
        $product_category = ProductCategory::where('maincategory',$type)->get();
        // dd($type);
        if($type=='sabin')
        {
            $product_category = ProductCategory::all();
            return view('backend.product_category.index',compact('product_category'));

        }
        else{
            return view('backend.product_category.index',compact('product_category','single_category'));
        }
        // return view('product_category.index',compact('product_category'));


    }
    public function createcategory($type)
    {
        // dd($type);
        $single_category='sabin';
        $main_category= ProductCategory::all();
        foreach($main_category as $category)
        {
            if($category->name==$type)
            {
                $single_category=$category->name;

            }


        }
        // dd($single_category);

        return view('backend.product_category.create',compact('main_category','single_category'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name=$request->input('name');
        $maincategory=$request->input('maincategory');
        // dd($maincategory);

        $status=ProductCategory::create([
            'name' => $request->input('name'),
            'maincategory'=>$request->input('maincategory'),
            'status' => $request->input('status'),
        ]);
        if($status){
            Session::flash('success','Record added successfully.');
        }else{
            Session::flash('error','Record cannot be added.');
        }
        if($maincategory=='0')
        {

            $single_category=$name;
            // dd($single_category);
            return view('backend.product_category.create',compact('main_category','single_category'));
        }
        else{
            $single_category=$maincategory;
            // dd($single_category);
            return view('backend.product_category.create',compact('main_category','single_category'));

        }

        // return redirect('product_category');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $product_category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $product_category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_category=ProductCategory::find($id);
        $main_category=ProductCategory::all();
        return view('backend.product_category.edit' , compact('product_category','main_category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $product_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_category=ProductCategory::find($id);
        $product_category->name = $request->input('name');
        $product_category->maincategory=$request->input('maincategory');
        $product_category->status = $request->input('status');

        $status=$product_category->update();
        if($status){
            Session::flash('success','category updated successfully.');
        }else{
            Session::flash('error','category cannot be updated.');
        }

        $maincategory=$request->input('maincategory');
        $name=$request->input('name');
        if($maincategory=='0')
        {

            $single_category=$name;
            // dd($single_category);
            return view('backend.product_category.create',compact('main_category','single_category'));
        }
        else{
            $single_category=$maincategory;
            // dd($single_category);
            return view('backend.product_category.create',compact('main_category','single_category'));

        }
        return redirect('backend/product_category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $product_category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_category=ProductCategory::find($id);
        $main_category=$product_category->maincategory;

        $status=$product_category->delete();
        if($status){
            Session::flash('success','category deleted successfully.');
        }else{
            Session::flash('error','category cannot be deleted.');
        }
        return $this->categoryindex($main_category);
        // return redirect('product_category');
    }

}
