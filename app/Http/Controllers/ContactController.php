<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact=Contact::orderBy('id','desc')->get();
        return view('backend.contact.index',compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contact = new Contact();
        return view('backend.contact.create',compact('contact'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status=Contact::create([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message'),
            'date'=>$request->input('date'),
        ]);
        if($status){
            Session::flash('success','Information and message saved successfully.');
        }else{
            Session::flash('error','Information and message cannot be added.');
        }
        return redirect('backend/contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return view('backend.contact.show',compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //return view('backend.contact.edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        /*
        $contact->name=$request->input('name');
        $contact->email=$request->input('email');
        $contact->phone=$request->input('phone');
        $contact->subject=$request->input('subject');
        $contact->message=$request->input('message');
        $contact->date=$request->input('date');
        $contact->remarks=$request->input('remarks');

        $status=$contact->update();
        if($status){
            Session::flash('success','Contact updated successfully.');
        }else{
            Session::flash('error','Contact cannot be updated.');
        }
        return redirect('contact');
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $status=$contact->delete();
        if($status){
            Session::flash('success','Contact deleted successfully.');
        }else{
            Session::flash('error','Contact cannot be deleted.');
        }
        return redirect('backend/contact');
    }
}
