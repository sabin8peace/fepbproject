<?php

namespace App\Http\Controllers;

use App\Neyog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NeyogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $neyog=Neyog::all();
        return view('backend.neyog.index',compact('neyog'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $neyog = new neyog();
        return view('backend.neyog.create',compact('neyog'));
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $status=Neyog::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/neyog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Neyog  $neyog
     * @return \Illuminate\Http\Response
     */
    public function show(Neyog $neyog)
    {
        return view('backend.neyog.show',compact('neyog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Neyog  $neyog
     * @return \Illuminate\Http\Response
     */
    public function edit(Neyog $neyog)
    {
        return view('backend.neyog.create',compact('neyog'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Neyog  $neyog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Neyog $neyog)
    {
        $input = $request->all();
        $status=$neyog->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/neyog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Neyog  $neyog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Neyog $neyog)
    {
        $status=$neyog->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/neyog');
    }
}
