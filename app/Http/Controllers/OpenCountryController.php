<?php

namespace App\Http\Controllers;

use App\OpenCountry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OpenCountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $openCountry=OpenCountry::all();
        return view('backend.openCountry.index',compact('openCountry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $openCountry = new OpenCountry();
        return view('backend.openCountry.create',compact('openCountry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $status=OpenCountry::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/openCountry');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OpenCountry  $openCountry
     * @return \Illuminate\Http\Response
     */
    public function show(OpenCountry $openCountry)
    {
        return view('backend.openCountry.show',compact('openCountry'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OpenCountry  $openCountry
     * @return \Illuminate\Http\Response
     */
    public function edit(OpenCountry $openCountry)
    {
        return view('backend.openCountry.create',compact('openCountry'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OpenCountry  $openCountry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpenCountry $openCountry)
    {
        $input = $request->all();
        $status=$openCountry->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/openCountry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OpenCountry  $openCountry
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpenCountry $openCountry)
    {
        $status=$openCountry->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/openCountry');
    }
}
