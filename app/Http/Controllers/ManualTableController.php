<?php

namespace App\Http\Controllers;

use App\ManualTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ManualTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $manualTable=ManualTable::orderBy('id','desc')->get();
        return view('backend.manualTable.index',compact('manualTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $manualTable = new ManualTable();
        $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.manualTable.create',compact('manualTable','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $input=$request->all();
        $tablename=$request->table_name;

        $tablename = str_replace(' ', '_', $tablename);
        // dd($tablename);
        $input['table_name']=$tablename;

        $status=ManualTable::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/manualTable');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ManualTable  $manualTable
     * @return \Illuminate\Http\Response
     */
    public function show(ManualTable $manualTable)
    {
        return view('backend.manualTable.show',compact('manualTable'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ManualTable  $manualTable
     * @return \Illuminate\Http\Response
     */
    public function edit(ManualTable $manualTable)
    {
        $categories=DB::table('generals')->orderBy('id','desc')->get();
        return view('backend.manualTable.create',compact('manualTable','categories'));

        // return view('backend.manualTable.create',compact('manualTable'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ManualTable  $manualTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManualTable $manualTable)
    {
        $input = $request->all();
        // dd($request);

        $input=$request->all();
        $tablename=$request->table_name;
        $tablename = str_replace(' ', '_', $tablename);
        $input['table_name']=$tablename;
        $status=$manualTable->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/manualTable');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ManualTable  $manualTable
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManualTable $manualTable)
    {
        $status=$manualTable->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/manualTable');
    }
}
