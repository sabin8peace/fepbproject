<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function writemail()
    {
        return view('email.mailsend');

    }
    public function mail(Request $request)

    {
        $docs='';
        if(!empty($request->file('image')))
        {
            $file=$request->file('image');
            $path=base_path().'/public/document_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name))
            {
                $docs=$name;
                $docs=base_path().'\public\document_upload'.'\\'.$docs;
            }

        }
    //    dd($docs);
        $name=$request->input('name');
        $subject=$request->input('subject');
        $to=$request->input('email');
        $msg=$request->input('message');
        $message=$name.','.$msg;
//        dd($message);


//        $name = 'Krunal';
       Mail::to($to)->send(new SendMailable($message,$subject,$docs));

        // return 'Email was sent';
        return view('email.confirm',compact('name'));
    }
}
