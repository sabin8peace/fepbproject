<?php

namespace App\Http\Controllers;

use App\Multimedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MultimediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $multimedia=Multimedia::orderBy('id','desc')->get();
        return view('backend.multimedia.index',compact('multimedia'));
    }
    public function chetanaindex($category)
    {
        $multimedia=Multimedia::where('category',$category)->orderBy('id','desc')->get();
        return view('backend.multimedia.index',compact('multimedia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.create',compact('multimedia','multimediacategory'));
    }
    public function multimediavideo()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.video',compact('multimedia','multimediacategory'));
    }
    public function multimediaaudio()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.audio',compact('multimedia','multimediacategory'));
    }
    public function multimediaphoto()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.photo',compact('multimedia','multimediacategory'));
    }
    public function faq()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.faq',compact('multimedia','multimediacategory'));

    }
    public function basicnote()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.basicnote',compact('multimedia','multimediacategory'));

    }
    public function institute()
    {
        $multimedia = new Multimedia();
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.multimedia.institute',compact('multimedia','multimediacategory'));

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/multimedia_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/multimedia_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        $status=Multimedia::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/multimedia');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Multimedia  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function show(Multimedia $multimedia)
    {
        return view('backend.multimedia.show',compact('multimedia'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Multimedia  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function edit(Multimedia $multimedia)
    {
        $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        if($multimedia->category=='video')
        {
            return view('backend.multimedia.video',compact('multimedia','multimediacategory'));
        }
        elseif($multimedia->category=='audio')
        {
            return view('backend.multimedia.audio',compact('multimedia','multimediacategory'));
        }
        elseif($multimedia->category=='photo')
        {
            return view('backend.multimedia.photo',compact('multimedia','multimediacategory'));
        }

        elseif($multimedia->category=='basicnote')
        {
            return view('backend.multimedia.basicnote',compact('multimedia','multimediacategory'));
        }
        elseif($multimedia->category=='faq')
        {
            return view('backend.multimedia.faq',compact('multimedia','multimediacategory'));
        }
        elseif($multimedia->category=='institute')
        {
            return view('backend.multimedia.institute',compact('multimedia','multimediacategory'));
        }

        else
        return view('backend.multimedia.create',compact('multimedia','multimediacategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Multimedia  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Multimedia $multimedia)
    {
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/multimedia_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/multimedia_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        // dd($input);

        $status=$multimedia->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/multimedia');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Multimedia  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Multimedia $multimedia)
    {
        $status=$multimedia->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/multimedia');

    }
}
