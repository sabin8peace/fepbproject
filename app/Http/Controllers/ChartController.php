<?php

namespace App\Http\Controllers;

use App\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $chart=Chart::groupBy('category_id')->get();
        // dd($chart);
        return view('backend.chart.index',compact('chart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $chart = new Chart();       
        $categories=DB::table('product_categories')->where('maincategory','chart')->orderBy('id','desc')->get();
        return view('backend.chart.create',compact('chart','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatestore(Request $request)
    {
        $fiscal=$request->input('fiscal');
        $male=$request->input('male');
        $female=$request->input('female');
        $total=$request->input('total');        
        $tmpid=$request->input('editid');
        for($i = 0;$i < count($fiscal);$i++){
            if($fiscal[$i]!=null)
            {
                if (isset($tmpid[$i])) {
                    $tmpstore=chart::find($tmpid[$i]);
                    $tmpstore->category_id = $request->input('category_id');
                    $tmpstore->fiscal=$fiscal[$i];
                    $tmpstore->male=$male[$i];
                    $tmpstore->female=$female[$i];
                    $tmpstore->total=$total[$i];
                    $tmpstore->updated_by=$request->input('updated_by');
                    $tmpstore->status=$request->input('status');

                    $status=$tmpstore->update();
                }
                else
                {
                    $status=chart::create([
                        'category_id' => $request->input('category_id'),
                        'fiscal'=> $fiscal[$i],
                        'male' => $male[$i],
                        'female' => $female[$i],
                        'total' => $total[$i],
                        'created_by'=>$request->input('created_by'),
                        'status'=>$request->input('status')
                    ]);

                }



            }
        }

        // $status=chart::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/chart');


    }
    public function store(Request $request)
    {
        // dd($request);
        // $input=$request->input('category');
        // $input=$request->input('category');
        $fiscal=$request->input('fiscal');
        $male=$request->input('male');
        $female=$request->input('female');
        $total=$request->input('total');
        
        for($i = 0;$i < count($fiscal);$i++){
            $status=Chart::create([
                'category_id' => $request->input('category_id'),
                'fiscal'=> $fiscal[$i],
                'male' => $male[$i],
                'female' => $female[$i],
                'total' => $total[$i],
                'created_by'=>$request->input('created_by'),
                'status'=>$request->input('status')
            ]);
        }

        // $status=chart::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/chart');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Chart  $chart
     * @return \Illuminate\Http\Response
     */
    public function show(Chart $chart)
    {
        $category_id=$chart->category_id;
        $chart=Chart:: where('category_id',$category_id)->orderBy('id','desc')->get();
        return view('backend.chart.show',compact('chart','category_id'));

    }
    public function showfn( $category_id)
    {
        // $category_id=$chart->category_id;
        $chart=Chart:: where('category_id',$category_id)->orderBy('id','desc')->get();
        return view('backend.chart.show',compact('chart','category_id'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chart  $chart
     * @return \Illuminate\Http\Response
     */
    public function edit(Chart $chart)
    {
        // dd($name);
        $category_id=$chart->category_id;
        $chart=Chart:: where('category_id',$category_id)->orderBy('id','desc')->get();
        // dd($chart);
        $categories=DB::table('product_categories')->where('maincategory','chart')->orderBy('id','desc')->get();
        return view('backend.chart.edit',compact('chart','categories','category_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chart  $chart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chart $chart)
    {
        dd($request);
        $fiscal=$request->input('fiscal');
        $chart=$request->input('chart');
        for($i = 0;$i < count($fiscal);$i++){
            $status=Chart::create([
                'category_id' => $request->input('category'),
                'fiscal'=> $fiscal[$i],
                'chart' => $chart[$i],
                'created_by'=>$request->input('created_by')
            ]);
        }

        // $status=chart::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/chart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chart  $chart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chart $chart)
    {
        $category=$chart->category_id;
        $status=$chart->delete();

        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        // return redirect('backend/chart');
        return $this->showfn($category);
    }
}
