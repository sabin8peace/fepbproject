<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=Blog::orderBy('id','desc')->get();
        return view('backend.blog.index',compact('blog'));
    }
    public function mediaindex($category)
    {
        $blog=Blog::where('category',$category)->orderBy('id','desc')->get();
        return view('backend.blog.index',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();
        return view('backend.blog.create',compact('blog','blogcategory'));
    }
    public function suchana()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();
        return view('backend.blog.suchana',compact('blog','blogcategory'));
    }
    public function press()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();
        return view('backend.blog.press',compact('blog','blogcategory'));
    }
    public function gallery()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Album')->get();
        return view('backend.blog.gallery',compact('blog','blogcategory'));
    }
    public function video()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();
        return view('backend.blog.video',compact('blog','blogcategory'));
    }
    public function bill()
    {
        $blog = new Blog();
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();
        return view('backend.blog.bill',compact('blog','blogcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/blog_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/blog_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        $status=Blog::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('backend.blog.show',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        // dd($blog->category);
        $blogcategory=DB::table('product_categories')->where('maincategory','Blog')->get();

        if($blog->category=='press')
        {
            return view('backend.blog.press',compact('blog','blogcategory'));

        }
        elseif($blog->category=='video')
        {
            return view('backend.blog.video',compact('blog','blogcategory'));

        }
        elseif($blog->category=='suchana')
        {
            return view('backend.blog.suchana',compact('blog','blogcategory'));

        }
        elseif($blog->category=='photo')
        {
            $blogcategory=DB::table('product_categories')->where('maincategory','Album')->get();

            return view('backend.blog.gallery',compact('blog','blogcategory'));

        }
        elseif($blog->category=='bill')
        {
            return view('backend.blog.bill',compact('blog','blogcategory'));

        }
        else
            return view('backend.blog.create',compact('blog','blogcategory'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/blog_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/blog_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        // dd($input);

        $status=$blog->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $status=$blog->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/blog');
    }
}
