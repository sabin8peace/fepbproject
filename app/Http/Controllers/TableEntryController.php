<?php

namespace App\Http\Controllers;

use PDO;
use App\TableEntry;
use App\ManualTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TableEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $tableEntry=DB::table('table_entries')->distinct()->get();
        // dd($tableEntry);
        // $tableEntry=TableEntry::orderBy('id','desc')->distinct()->get(['category','id']);
        // $tableEntry=TableEntry::orderBy('id','desc')->select('category','manual_table_id','table_field','field_type','status','created_by','updated_by','id')->distinct()->get();
        // $tableEntry=TableEntry::orderBy('id','desc')->get();
        $tableEntry=TableEntry::groupBy('category')->get();
        // dd($tableEntry);
        return view('backend.tableEntry.index',compact('tableEntry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tableEntry = new TableEntry();
        $categories=DB::table('manual_tables')->orderBy('id','desc')->get();
        return view('backend.tableEntry.create',compact('tableEntry','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tblid=$request->input('manual_table_id');
        // dd($request);

        $tablename=ManualTable::where('id',$tblid)->pluck('table_name');
        $table_name=$tablename[0];
        $table_field=$request->input('table_field');
        for($i=0;$i<count($table_field);$i++)
        {
            $table_field[$i]=str_replace(' ', '_', $table_field[$i]);

        }
        // dd($table_field);

        $field_type=$request->input('field_type');
        for($i = 0;$i < count($table_field);$i++){
            $status=TableEntry::create([
                'manual_table_id' => $request->input('manual_table_id'),
                'table_field'=> $table_field[$i],
                'field_type' => $field_type[$i],
                'category'=>$table_name,
                'created_by'=>$request->input('created_by'),
                'status'=>$request->input('status'),
                'updated_by'=>$request->input('updated_by')
            ]);
        }

        // $status=tableEntry::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        // return redirect('backend/tableEntry');
        return $this->create_table($table_name,$table_field,$field_type);
    }
    public function create_table($table_name,$table_field,$field_type)
    {
        // dd($field_type);
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "db_fepb";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        // sql to create table
        $sql = "CREATE TABLE $table_name (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,";

        for($i=0;$i<count($table_field);$i++)
        {
           $sql=$sql.$table_field[$i].' '. $field_type[$i].', ';

        }
$sql=$sql.'created_at TIMESTAMP';
        $sql=$sql.')';
// dd($sql);
        if (mysqli_query($conn, $sql)) {
            echo "Table  created successfully";
        } else {
            echo "Error creating table: " . mysqli_error($conn);
        }

        mysqli_close($conn);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TableEntry  $tableEntry
     * @return \Illuminate\Http\Response
     */
    public function show(TableEntry $tableEntry)
    {
        return view('backend.tableEntry.show',compact('tableEntry'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TableEntry  $tableEntry
     * @return \Illuminate\Http\Response
     */
    public function edit(TableEntry $tableEntry)
    {
        // dd($tableEntry);
        $categories=DB::table('product_categories')->orderBy('id','desc')->get();
        return view('backend.tableEntry.create',compact('tableEntry','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TableEntry  $tableEntry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TableEntry $tableEntry)
    {
        $input = $request->all();
        // dd($request);

        $status=$tableEntry->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/tableEntry');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TableEntry  $tableEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(TableEntry $tableEntry)
    {
        $status=$tableEntry->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/tableEntry');
    }
}
