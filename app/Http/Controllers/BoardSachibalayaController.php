<?php

namespace App\Http\Controllers;

use App\BoardSachibalaya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BoardSachibalayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boardSachibalaya=BoardSachibalaya::orderBy('id','desc')->get();
        // $boardSachibalaya=boardSachibalaya::where('category','boardSachibalaya')->get();
        return view('backend.boardSachibalaya.index',compact('boardSachibalaya'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boardSachibalaya = new BoardSachibalaya();
        return view('backend.boardSachibalaya.create',compact('boardSachibalaya'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $status=BoardSachibalaya::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/boardSachibalaya');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoardSachibalaya  $boardSachibalaya
     * @return \Illuminate\Http\Response
     */
    public function show(BoardSachibalaya $boardSachibalaya)
    {
        return view('backend.boardSachibalaya.show',compact('boardSachibalaya'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoardSachibalaya  $boardSachibalaya
     * @return \Illuminate\Http\Response
     */
    public function edit(BoardSachibalaya $boardSachibalaya)
    {
        return view('backend.boardSachibalaya.create',compact('boardSachibalaya'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoardSachibalaya  $boardSachibalaya
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoardSachibalaya $boardSachibalaya)
    {
        $input = $request->all();

        $status=$boardSachibalaya->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/boardSachibalaya');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoardSachibalaya  $boardSachibalaya
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoardSachibalaya $boardSachibalaya)
    {
        $status=$boardSachibalaya->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/boardSachibalaya');
    }
}
