<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Simple extends Model
{
    protected $table='simples';
    protected $fillable=[
        'title',
        'category',
        'status',
        'description',
        'created_by',
        'updated_by'
    ];
}
