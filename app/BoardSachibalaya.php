<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardSachibalaya extends Model
{
    protected $table='board_sachibalayas';
    protected $fillable=[
        'title',
        'post',
        'resource',
        'status',
        'description',
        'created_by',
        'updated_by'
    ];
}
