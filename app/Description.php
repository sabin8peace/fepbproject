<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $table='descriptions';
    protected $fillable=[
        'title',
        'maintitle',
        'category_id',
        'status',
        'description',
        'note',
        'short_description',
        'created_by',
        'updated_by'
    ];

    public function product_category()
    {
        return $this->belongsTo('App\ProductCategory','category_id');
    }
}
