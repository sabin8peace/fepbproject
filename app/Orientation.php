<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Orientation extends Model
{ 
   protected $table = 'orientation_done';
   protected $fillable = ['ID', 'Permission','Local_Government','District','Province', 'OrName', 'Address', 'Contact', 'Email', 'Website', 'Status', 'Remark', 'latitude', 'longitude'];
}
