<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    
    protected $table='members';
    protected  $fillable=['name','description','address','phone','email','position','rank','joined_from','status','image','created_by','updated_by'];

}
