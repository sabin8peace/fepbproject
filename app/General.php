<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $table='generals';
    protected $fillable=[
        'title',
        'category',
        'status',
        'note',
        'short_description',
        'created_by',
        'updated_by'
    ];

  
    public function manualtables()
    {
        return $this->hasMany('App\ManualTable');

    }
}
