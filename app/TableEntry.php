<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableEntry extends Model
{
    protected $table='table_entries';
    protected $fillable=[
        'manual_table_id',
        'table_field',
        'status',
        'category',
        'field_type',
        'created_by',
        'updated_by'
    ];
    public function manualtable()
    {
        return $this->belongsTo('App\ManualTable');
    }
}
