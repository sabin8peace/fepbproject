-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2018 at 06:41 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fepb`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'बोर्डको परिचय', 1, '<p>नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार प्रवर्द्धन बोर्डको व्यवस्था गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ ।</p>\r\n\r\n<p>माननीय श्रम तथा रोजगार मन्त्रीको अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा अध्यक्ष सहित २५ जना सदस्यहरु रहनु भएको छ ।</p>', '<p>नेपाल सरकारले बैदेशिक रोजगार व्यवसायलाई प्रवर्द्धन गर्न, सो व्यवसायलाई सुरक्षित, व्यवस्थित र मर्यादित बनाउन तथा बैदेशिक रोजगारमा जाने कामदार र वैदेशिक रोजगार व्यवसायीको हकहित संरक्षण कार्य गर्ने प्रयोजनको लागि बैदेशिक रोजगार प्रवर्द्धन बोर्डको व्यवस्था गरेको छ ।बैदेशिक रोजगार ऐन २०६४ को दफा ३८ मा भएको व्यवस्था बमोजिम यस वोर्डको गठन भएको हो । वोर्डको नियमित कार्य संचालन गर्न बैदेशिक रोजगार प्रवर्द्धन वोर्डको सचिवालय छ ।</p>\r\n\r\n<p>माननीय श्रम तथा रोजगार मन्त्रीको अध्यक्षता रहने यस वोर्डमा उच्च पदस्थ सरकारी अधिकारी, बैदेशिक रोजगार व्यवसायी, ट्रेड युनियन, बैदेशिक रोजगार विज्ञ र यस क्षेत्रमा कार्यरत संस्थाहरको प्रतिनिधित्व रहने व्यवस्था छ । हाल वोर्डमा अध्यक्ष सहित २५ जना सदस्यहरु रहनु भएको छ ।</p>', 2, 2, '2018-11-28 03:23:33', '2018-12-06 03:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `image`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'banner 1', '5bffc02d1a4d7_og-logo.png', 1, 'thi si description of the banner page to be displayed in first', 2, 2, '2018-11-29 04:47:13', '2018-11-29 04:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'samachar1', 'kartik 2,2074', '5bfcd94e4ad25_3894133980_7e38f821fa.jpg', '5bfcd94e4dae5_CMS_Creative_164657191_Kingfisher.jpg', 'samachar', 1, 'main description here', 'wiauskg sadgdoibsd dgoisdg', 2, NULL, '2018-11-26 23:57:38', '2018-11-26 23:57:38'),
(2, 'suchana 1', 'mangsir 2, 2075', '5bfcdab0b56c4_well-images-for-wallpaper-desktop-24.jpg', '5bfcdab0b5d3b_images.jpg', 'suchana', 1, '<p>suchana ko edit page</p>', '<p>suchana ko ekbjewsfa</p>', 2, 2, '2018-11-27 00:03:32', '2018-12-06 03:26:36'),
(3, 'Press 1', 'push 2, 4044', NULL, NULL, 'press', 1, 'oiadlgoirsgb', 'ogirsdgolgs', 2, 2, '2018-11-27 00:04:23', '2018-11-27 00:04:33'),
(4, 'biill 1', 'magh 14 1075', NULL, NULL, 'bill', 1, 'awiskdjvdfakfjb', 'kdsjbgkdsjgb sdgiu', 2, NULL, '2018-11-27 00:05:01', '2018-11-27 00:05:01'),
(5, 'Chester Norman', '9782jwwe', NULL, '5bfe68ce51ae8_download.html', 'press', 1, NULL, NULL, 2, 2, '2018-11-28 04:22:10', '2018-11-28 04:22:41'),
(6, 'album2', 'jkasd2', '5bff8f1b380ee_5ba3cffb1b6aa_WhatsApp Image 2018-09-20 at 10.34.20 PM.jpeg', NULL, 'photo', 1, 'delfwfdsan', NULL, 2, 2, '2018-11-29 01:17:51', '2018-12-02 00:06:10'),
(7, 'video 1', 'dskba 324', '5bff8fa0d2fa9_eventbanner.jpg', NULL, 'video', 1, 'ldksab fsdg ', 'https://www.youtube.com/watch?time_continue=1&v=xadQwMU4qtU', 2, 2, '2018-11-29 01:20:04', '2018-12-06 05:06:40'),
(8, 'album1', 'djasf dsaf', '5bff96fa1ef64_5ba3d0489b0f9_WhatsApp Image 2018-09-20 at 10.30.53 PM.jpeg', NULL, 'photo', 1, 'dlsab eridgsk vosdgv', NULL, 2, 2, '2018-11-29 01:51:26', '2018-12-02 00:05:50'),
(9, 'album2', 'kweajs 214', '5c0371176c191_CMS_Creative_164657191_Kingfisher.jpg', NULL, 'photo', 1, 'eidsb dsigk asdgik edgsiagk&nbsp;', 'uiaksd sidug', 2, NULL, '2018-12-01 23:58:51', '2018-12-01 23:58:51');

-- --------------------------------------------------------

--
-- Table structure for table `board_sachibalayas`
--

CREATE TABLE `board_sachibalayas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_sachibalayas`
--

INSERT INTO `board_sachibalayas` (`id`, `title`, `post`, `resource`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'sabin', 'adakxhya', '2', 1, 'thdsifhhsdf', 2, NULL, '2018-11-28 20:30:06', '2018-11-28 20:30:06'),
(2, NULL, 'upaadakshya', '3', 1, NULL, 2, NULL, '2018-11-28 20:30:20', '2018-11-28 20:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `board_works`
--

CREATE TABLE `board_works` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_works`
--

INSERT INTO `board_works` (`id`, `title`, `status`, `category`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'board gathan', 1, 'boardgathan', '<h4><strong>board gahtan title</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This is inside text for the page</p>', 2, 2, '2018-11-28 02:26:02', '2018-12-06 00:31:56'),
(11, 'work 1', 1, 'boardWork', '<h4><strong>Title for board work is here</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Detail about the work section is here</p>', 2, 2, '2018-12-06 00:17:30', '2018-12-06 00:31:16');

-- --------------------------------------------------------

--
-- Table structure for table `charts`
--

CREATE TABLE `charts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `male` double DEFAULT NULL,
  `female` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `charts`
--

INSERT INTO `charts` (`id`, `title`, `date`, `fiscal`, `male`, `female`, `total`, `category`, `status`, `description`, `created_by`, `updated_by`, `category_id`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, '2064/65', 0, 0, 1300, NULL, 1, NULL, 2, NULL, 35, '2018-12-12 23:49:41', '2018-12-13 00:02:29'),
(3, NULL, NULL, '2066/67', 0, 0, 1800, NULL, 1, NULL, 2, NULL, 35, '2018-12-12 23:49:41', '2018-12-13 00:02:29'),
(12, NULL, NULL, '61/62', 0, 0, 150, NULL, 1, NULL, 2, NULL, 37, '2018-12-13 01:26:32', '2018-12-13 01:26:32');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(3, 'sabin', 'sabin8peace@gmail.com', NULL, 'usayd adsf', 'sasghd messag', '2018-11-30 01:10:04', '2018-11-30 01:10:04'),
(4, 'sandhya', 'sandhya8peace@gmail.com', NULL, 'slhd ddsafk', 'dsk idfg fsdg', '2018-11-30 01:10:27', '2018-11-30 01:10:27'),
(5, 'aewugsf', 'sabin8peace@gmail.com', NULL, 'weasf', 'rewsdhg', '2018-11-30 01:19:33', '2018-11-30 01:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `descriptions`
--

CREATE TABLE `descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `maintitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `descriptions`
--

INSERT INTO `descriptions` (`id`, `maintitle`, `title`, `status`, `description`, `note`, `short_description`, `created_by`, `category_id`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, '2iugjhvvjh', '2dfkfsbgdgsigbkb', 1, '<p>2fkvcxhbnfdxbk</p>', '<p>2fsfddlgfdlkg</p>', '<p>2kdsbgskdgd</p>', 2, 20, NULL, '2018-12-14 04:28:08', '2018-12-14 04:28:08'),
(4, '3 srdoilhsdg', '3fsodfilgh', 1, '<p>3 godflhnb</p>', '<p>3 rddfhi</p>', '<p>3rdlfkn</p>', 2, 20, NULL, '2018-12-14 04:50:00', '2018-12-14 04:50:00'),
(5, '4orsieddf', '4fdolhkndfdh', 1, '<p>4gdlhfnv</p>', '<p>4eodlghkl</p>', '<p>4preosdfhdnl</p>', 2, 20, NULL, '2018-12-14 04:50:00', '2018-12-14 04:50:00'),
(6, 'idausfdssddb 1', 'ukfdssdjafkj1', 1, '<p>isdkafkfsjddm1</p>', '<p>ikwasgfbdskfb1</p>', '<p>dskfb 1</p>', 2, 24, NULL, '2018-12-14 05:16:02', '2018-12-14 05:16:02'),
(7, '2 fsdzbdsfoig', '2ofdgl trfodigs redg', 1, '<p>2rosdgl rtsotoddyreyresh</p>', '<p>2oelkgbdoerig</p>', '<p>2ros grsfdogisdbf</p>', 2, 24, NULL, '2018-12-14 05:16:02', '2018-12-14 05:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `excel_files`
--

CREATE TABLE `excel_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `date` text COLLATE utf8mb4_unicode_ci,
  `other_file` text COLLATE utf8mb4_unicode_ci,
  `category` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_files`
--

INSERT INTO `excel_files` (`id`, `title`, `date`, `other_file`, `category`, `description`, `short_description`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(16, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(17, 'itsolutionstuff.com', NULL, NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(18, 'Demo.itsolutionstuff.com', NULL, NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(19, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(20, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(21, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(22, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(23, 'itsolutionstuff.com', NULL, NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(24, 'Demo.itsolutionstuff.com', NULL, NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL),
(25, 'itsolutionstuff.com', '273547234', NULL, NULL, 'Larave Tuto..', NULL, NULL, NULL),
(26, 'Demo.itsolutionstuff.com', '9879879', NULL, NULL, 'Demo fo Larave Tuto..', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filetype` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`id`, `title`, `date`, `image`, `other_file`, `category`, `filetype`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'report1', '2018-11-27', '5bfcdf97a01e4_report1.jpg', '5bfae7a2a1619_C4C Inventory.pdf', 'report', NULL, 1, 'salkhslaglka', 'www.fnc.com', 2, 2, '2018-11-25 12:34:14', '2018-11-27 00:24:27'),
(3, 'report2', 'manhs 24949', '5bfcdfa16cf1f_report2.jpg', NULL, 'report', NULL, 1, 'qewkgsdabdsbgkj', 'ekwdsg', 2, 2, '2018-11-27 00:23:30', '2018-11-27 00:24:37'),
(4, 'report 3', 'ldadsf 83,3485', '5bfcdfb00007a_report1.jpg', NULL, 'report', NULL, 1, 'eweqksadbsdak', NULL, 2, 2, '2018-11-27 00:23:53', '2018-11-27 00:24:52'),
(5, 'video title 1', 'magh 2,3233', '5bfce01363359_video-player.png', NULL, 'video', NULL, 1, 'this is descripition of video section', 'https://www.youtube.com/watch?v=UDmky4jM4qU', 2, 2, '2018-11-27 00:26:31', '2018-11-27 00:30:23'),
(6, 'video title 2', 'falgun 2,2349', '5bfce04a019e0_video-player.png', NULL, 'video', NULL, 1, 'this is video detail 2 ikedsdja dfidsugb rrirfdsgxb rfdigb rtdfigb rtddif g<br />\r\nrfdskgb fgdjkg fd gfkd gifidg xrfsdg', 'https://www.youtube.com/watch?v=9I_bOgWQDZw', 2, 2, '2018-11-27 00:27:26', '2018-11-27 00:30:44'),
(7, 'video title 3', 'msh 3,34257', '5bfce0df2d5b9_video-player.png', NULL, 'video', NULL, 1, 'laskhf serddgk ferersiidg rffsddug rtef dsgirtisdf gg fdg usdfg&nbsp;', 'https://www.youtube.com/watch?v=UIftZGIyxHg', 2, NULL, '2018-11-27 00:29:55', '2018-11-27 00:29:55'),
(8, 'other file', 'jsd 2213`', NULL, '5bfe757326e28_contactus.html', 'other', 'in dsof', 1, NULL, NULL, 2, NULL, '2018-11-28 05:16:07', '2018-11-28 05:16:07'),
(9, 'report 1', 'dks 232', '5bff7b19c4bb6_report1.jpg', '5bff7b19c5768_Daily Report (2).pdf', 'report', 'progress', 1, NULL, NULL, 2, NULL, '2018-11-28 23:52:29', '2018-11-28 23:52:29'),
(10, 'testing product', 'asldfnk98', '5bff7b48ec5da_256-256-76f453c62108782f0cad9bfc2da1ae9d.png', '5bff7b48ed6b2_Experience-Letter1.pdf', 'report', 'publication', 1, NULL, NULL, 2, 2, '2018-11-28 23:53:16', '2018-11-28 23:53:47'),
(11, 'ani 1', 'asfd97', NULL, '5bffb197d9d6a_5bff8910167ee_Daily Report (1).pdf', 'ain', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 03:44:59', '2018-11-29 03:44:59'),
(12, 'bulletin 1', 'sjaf 32', '5bffb1b52221a_5ba3ccc9c5c14_APJB9192.JPG', '5bffb1b522c36_Experience-Letter1.pdf', 'bulletin', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 03:45:29', '2018-11-29 03:45:29'),
(13, 'pretibedhan', 'sl12`', NULL, '5bffb1cbe95a5_5bff892210158_cvmilan-1-4 (1).pdf', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'वार्षिक प्रतिवेदन', 2, 2, '2018-11-29 03:45:51', '2018-12-14 13:01:01'),
(14, 'adhyan pra isadf', 'ds 345', NULL, '5bffb35858301_5bff8910167ee_Daily Report (1).pdf', 'pratibedhan', 'adhyan prathibedhan', 1, NULL, NULL, 2, NULL, '2018-11-29 03:52:28', '2018-11-29 03:52:28'),
(15, 'nebadhan form', 'das 323', NULL, '5bffbb01d7533_5bff8910167ee_Daily Report (1).pdf', 'nebadan', NULL, 1, NULL, NULL, 2, NULL, '2018-11-29 04:25:09', '2018-11-29 04:25:09'),
(16, 'bulletin', 'dateh 2364', '5c10bd46150ff_th.jpg', '5c10bd4615884_Capture.PNG', 'bulletin', NULL, 1, NULL, NULL, 2, 2, '2018-12-12 02:03:22', '2018-12-12 02:08:48'),
(17, 'prakashan', 'date 31', '5c10bea0c20e8_th.jpg', '5c10bea0c2750_acl.PNG', 'prakashan', NULL, 1, NULL, NULL, 2, NULL, '2018-12-12 02:09:08', '2018-12-12 02:09:08'),
(18, 'adhyan pratibedhan', 'date 3213', NULL, '5c10c0b34af37_inventory.PNG', 'pratibedhan', 'adhyan prathibedhan', 1, NULL, NULL, 2, NULL, '2018-12-12 02:17:59', '2018-12-12 02:17:59'),
(19, 'pratibedhan', 'date 32', NULL, '5c10c1b66e00b_inventory.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'मासिक प्रतिवेदन', 2, 2, '2018-12-12 02:22:18', '2018-12-12 02:23:57'),
(20, 'FAQ SECTION', NULL, NULL, NULL, 'faq', NULL, 1, '<p>this&nbsp; is answer for first question</p>', 'this is qtn 1', 2, 2, '2018-12-12 04:10:52', '2018-12-12 04:11:51'),
(21, 'कामदारहरुले ध्यान दिनु पर्ने कुराहरु title here', NULL, NULL, NULL, 'basicnote', NULL, 1, '<p>description of&nbsp;कामदारहरुले ध्यान दिनु पर्ने कुराहरु</p>', NULL, 2, 2, '2018-12-12 04:13:12', '2018-12-12 04:15:17'),
(22, 'सहयोगी संस्थाहरु  title here', 'date 213', NULL, NULL, 'institute', NULL, 1, '<p><strong>सहयोगी संस्थाहरु </strong> description here</p>', NULL, 2, 2, '2018-12-12 04:14:55', '2018-12-12 04:15:04'),
(23, 'pragati pratibeshan 2', 'ksdfdh 35', NULL, '5c13fa16b4d5f_inventory.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'चौमासिक प्रतिवेदन', 2, NULL, '2018-12-14 12:59:38', '2018-12-14 12:59:38'),
(24, 'pragati pratibedhan 3', 'askf 46', NULL, '5c13fa39c2ebd_acl.PNG', 'pratibedhan', 'pragati prathibedhan', 1, NULL, 'वार्षिक प्रतिवेदन', 2, NULL, '2018-12-14 13:00:13', '2018-12-14 13:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `first_table`
--

CREATE TABLE `first_table` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` text,
  `email` text,
  `phone` double DEFAULT NULL,
  `address` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `important_links`
--

CREATE TABLE `important_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `link` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `important_links`
--

INSERT INTO `important_links` (`id`, `title`, `status`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'link 1', 1, 'www.sabinbajgain.com.np', 2, NULL, '2018-12-06 02:31:45', '2018-12-06 02:31:45'),
(3, 'link2', 1, 'www.codeforcore.com', 2, NULL, '2018-12-06 02:32:30', '2018-12-06 02:32:30'),
(4, 'reodh;l', 1, 'kfdh', 2, 2, '2018-12-06 20:18:37', '2018-12-06 20:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `jana_gunasos`
--

CREATE TABLE `jana_gunasos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jana_gunasos`
--

INSERT INTO `jana_gunasos` (`id`, `name`, `email`, `phone`, `address`, `message`, `image`, `created_at`, `updated_at`) VALUES
(1, 'sabin', 'sabin8peace@gmail.com', 983294834, 'ktm', '<p>uidsakgf disuf seridig fdisg</p>', '5c0f62c97fc99_th.jpg', '2018-12-11 01:25:01', '2018-12-11 01:25:01'),
(3, 'sabin', 'sabin@sabin.com', 2147483647, 'kathmansu', 'sajkd dsfi dsgbb ddsgd g', '5c0f9d894b021_th.jpg', '2018-12-11 05:35:41', '2018-12-11 05:35:41'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-11 05:44:05', '2018-12-11 05:44:05'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-14 03:15:37', '2018-12-14 03:15:37');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manual_tables`
--

CREATE TABLE `manual_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `table_name` text COLLATE utf8mb4_unicode_ci,
  `general_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manual_tables`
--

INSERT INTO `manual_tables` (`id`, `title`, `table_name`, `general_id`, `description`, `created_at`, `updated_at`) VALUES
(5, 'first table', 'first_table', 1, '<p>table 1</p>', '2018-12-10 00:38:14', '2018-12-10 00:38:14'),
(6, 'sectable', 'second_table', 1, '<p>uiasdg ewigasd aesdg&nbsp;</p>', '2018-12-10 05:15:46', '2018-12-10 05:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `rank` int(11) DEFAULT NULL,
  `joined_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `image`, `description`, `rank`, `joined_from`, `address`, `phone`, `email`, `position`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Ivan Lopez', '5bfba2a5d6fa1_3894133980_7e38f821fa.jpg', 'wiuakfs', 1, '2018-11-09', '300 Popip Place', '7179323072', 'gar@host.local', 'hazupo', 1, 2, NULL, '2018-11-26 01:52:09', '2018-11-26 01:52:09'),
(2, 'Ivajvwevsaf', '5bfbaadb82a9a_well-images-for-wallpaper-desktop-24.jpg', 'wiuakfs', 1, '2018-11-09', '300 Popip Place', '7179323072', 'gar@host.local', 'hazupo', 1, 2, 2, '2018-11-26 02:24:31', '2018-11-26 02:27:11'),
(3, 'sabin', '5bfcdbda69508_images.jpg', 'this is written for ceo', 3, '2018-11-10', 'kathmandu nepal', '98703738378', 's@g.com', 'CEO', 1, 2, NULL, '2018-11-27 00:08:30', '2018-11-27 00:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_13_152715_create_product_categories_table', 2),
(4, '2018_11_24_022402_create_mails_table', 2),
(5, '2018_10_07_035208_create_settings_table', 3),
(6, '2018_10_07_034806_create_sliders_table', 4),
(7, '2018_11_25_143226_create_abouts_table', 5),
(8, '2018_11_25_153815_create_board_works_table', 6),
(9, '2018_11_25_162046_create_mobile_apps_table', 7),
(10, '2018_11_25_164354_create_blogs_table', 8),
(11, '2018_11_25_165913_create_extras_table', 9),
(12, '2018_10_07_033851_create_contacts_table', 10),
(13, '2018_11_26_061143_create_members_table', 11),
(14, '2018_11_28_084205_create_board_sachibalayas_table', 12),
(15, '2018_11_28_090618_create_yojana_karyakrams_table', 13),
(16, '2018_11_28_133015_create_multimedia_table', 14),
(17, '2018_11_29_102024_create_banners_table', 15),
(18, '2018_12_02_073108_create_simples_table', 16),
(21, '2018_12_06_080524_create_important_links_table', 18),
(22, '2018_12_07_093152_create_excel_files_table', 19),
(25, '2018_12_08_043224_create_manual_tables_table', 20),
(26, '2018_12_08_050312_create_table_entries_table', 20),
(27, '2018_12_11_064513_create_jana_gunasos_table', 21),
(28, '2018_12_12_111124_create_trainings_table', 22),
(29, '2018_12_13_015919_create_charts_table', 23),
(30, '2018_12_04_065317_create_generals_table', 24),
(31, '2018_12_04_065343_create_descriptions_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_apps`
--

CREATE TABLE `mobile_apps` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobile_apps`
--

INSERT INTO `mobile_apps` (`id`, `title`, `link`, `image`, `mobile_image`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'This is the title of the project', 'https://play.google.com/store/apps/details?id=com.Braindigit.DOFEApp', '5c00a1d5b10cb_mobile.jpg', '5c00a1d5b1e29_mobimg2.png', 1, '<ul>\r\n	<li>This App is very good</li>\r\n	<li>this is list2</li>\r\n	<li>tdsif is 3</li>\r\n	<li>ysd gserdg 4</li>\r\n	<li>weaskdgbe sdg 5</li>\r\n	<li>sdkjgbfdslkg 9</li>\r\n</ul>', 'this is short description o fteh project', 2, 2, '2018-11-25 10:55:48', '2018-11-29 20:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `multimedia`
--

CREATE TABLE `multimedia` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `multimedia`
--

INSERT INTO `multimedia` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'audio', 'siakf', NULL, '5bfe9e9299f79_logo.png', 'audio', 1, 'dslkngs sgdk', NULL, 2, NULL, '2018-11-28 08:11:34', '2018-11-28 08:11:34'),
(2, 'video 1', 'vidksadbjf', '5c08f22cf3508_5bfcdab0b5d3b_images.jpg', NULL, 'video', 1, 'ths is description of viedo', 'https://www.youtube.com/watch?v=xadQwMU4qtU', 2, 2, '2018-11-28 08:14:08', '2018-12-06 04:47:10'),
(3, 'phphp', 'ksafb', '5bfe9f3dcabc5_mobimg2.png', NULL, 'photo', 1, 'daskg dsgk', NULL, 2, NULL, '2018-11-28 08:14:25', '2018-11-28 08:14:25'),
(6, 'wefvoami', 'zoffikbi', '5bffc3b71c289_news1.jpg', NULL, 'photo', 1, 'datuod', NULL, 2, NULL, '2018-11-29 05:02:19', '2018-11-29 05:02:19'),
(7, 'ewtoova', 'fimteg', '5bffc3c42302c_news.jpg', NULL, 'photo', 1, 'vejo', NULL, 2, NULL, '2018-11-29 05:02:32', '2018-11-29 05:02:32'),
(8, 'ivapup', 'nocjob', NULL, '5bffc3e6d3ecd_5b80eafe113db_Banana Minions Ringtone -) - YouTube[via torchbrowser.com] (1).aac', 'audio', 1, 'mape', NULL, 2, NULL, '2018-11-29 05:03:06', '2018-11-29 05:03:06'),
(9, 'iwnebe', 'vacidod', NULL, '5bffc3f7a9d8b_5b80ef1cd796f_Banana Minions Ringtone -) - YouTube[via torchbrowser.com].aac', 'audio', 1, 'lew', NULL, 2, NULL, '2018-11-29 05:03:23', '2018-11-29 05:03:23'),
(10, 'pgp', 'kasf234', '5c08ee966c191_5bfcdab0b5d3b_images (1).jpg', NULL, 'photo', 1, 'poasflnasf', NULL, 2, NULL, '2018-12-06 03:55:38', '2018-12-06 03:55:38'),
(11, 'FAQ SECTION', NULL, NULL, NULL, 'faq', 1, '<p>answere of qtn</p>', 'question is here', 2, NULL, '2018-12-12 04:34:09', '2018-12-12 04:34:09'),
(12, 'sahayogi sanstha here', NULL, NULL, NULL, 'institute', 1, '<p>desc of sahayogi sanstha here</p>\r\n\r\n<ul>\r\n	<li>ioadsgkf</li>\r\n	<li>feasd</li>\r\n	<li>g</li>\r\n	<li>sddgd</li>\r\n	<li>g</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', NULL, 2, 2, '2018-12-12 04:34:48', '2018-12-13 04:25:47'),
(13, 'kamdar le dhyan dinu parni ko title', NULL, NULL, NULL, 'basicnote', 1, '<p>description of kamdar le dhyan dinu ar</p>\r\n\r\n<ul>\r\n	<li>this si point</li>\r\n	<li>tsf eaofidfsg osdig</li>\r\n	<li>fsdogil erosdg&nbsp;</li>\r\n	<li>reesdbobgil resgd</li>\r\n	<li>redsdbg sregdf</li>\r\n</ul>', NULL, 2, 2, '2018-12-12 04:35:19', '2018-12-13 04:20:56'),
(14, 'FAQ SECTION', NULL, NULL, NULL, 'faq', 1, '<p>This is second question ko answer ho</p>', 'second question', 2, NULL, '2018-12-13 04:14:36', '2018-12-13 04:14:36');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('fepbadmin@admin.com', '$2y$10$yHnRh/CPS8bSn.tu3suxIuWP4oM8HUfIHaJIfWATgCHRasPKz9Ld.', '2018-11-23 20:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maincategory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `maincategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 'product1', '0', 1, '2018-11-24 23:32:08', '2018-11-24 23:32:08'),
(2, 'Blog', '0', 1, '2018-11-25 11:40:00', '2018-11-25 11:40:00'),
(4, 'samachar', 'Blog', 1, '2018-11-25 11:41:12', '2018-11-26 21:08:18'),
(5, 'suchana', 'Blog', 1, '2018-11-25 11:42:05', '2018-11-26 21:08:44'),
(6, 'Other', '0', 1, '2018-11-25 11:54:43', '2018-11-25 11:54:43'),
(7, 'yojana', 'Other', 1, '2018-11-25 11:54:53', '2018-11-25 11:54:53'),
(8, 'karyakram', 'Other', 1, '2018-11-25 11:55:20', '2018-11-25 11:55:20'),
(9, 'audio', 'Other', 1, '2018-11-25 11:55:31', '2018-11-25 11:55:31'),
(11, 'press', 'Blog', 1, '2018-11-26 21:08:58', '2018-11-26 21:08:58'),
(12, 'bill', 'Blog', 1, '2018-11-26 21:09:05', '2018-11-26 21:09:05'),
(13, 'report', 'Other', 1, '2018-11-27 00:22:39', '2018-11-27 00:22:39'),
(14, 'video', 'Other', 1, '2018-11-27 00:25:23', '2018-11-27 00:25:23'),
(15, 'Album', '0', 1, '2018-12-01 23:43:39', '2018-12-01 23:43:39'),
(16, 'album1', 'Album', 1, '2018-12-01 23:43:49', '2018-12-01 23:43:49'),
(17, 'album2', 'Album', 1, '2018-12-01 23:51:15', '2018-12-01 23:51:15'),
(18, 'Service', '0', 1, '2018-12-03 23:35:18', '2018-12-03 23:35:18'),
(19, 'Board Welfare Work', 'Service', 1, '2018-12-03 23:40:35', '2018-12-03 23:40:35'),
(20, 'मृतक कामदारको नजिकका हकवालालाई आर्थिक सहायता प्र्रदान', 'Board Welfare Work', 1, '2018-12-03 23:42:13', '2018-12-03 23:42:13'),
(21, 'अङ्गभङ्ग वा अशक्त भई बिदेशमा काम गर्न नसकेको कारण नेपाल फर्की निवेदन गर्ने कामदारलाई आर्थिक सहायता प्रदान', 'Board Welfare Work', 1, '2018-12-03 23:42:27', '2018-12-03 23:42:27'),
(22, 'कामदारका परिवारालई स्वास्थ्य उपचार सहयोग', 'Board Welfare Work', 1, '2018-12-03 23:42:38', '2018-12-03 23:42:38'),
(23, 'शव व्यवस्थापन', 'Board Welfare Work', 1, '2018-12-03 23:42:47', '2018-12-03 23:42:47'),
(24, 'उद्धार सम्बन्धी कार्य', 'Board Welfare Work', 1, '2018-12-03 23:42:55', '2018-12-03 23:42:55'),
(25, 'आप्रवासी स्रोतकेन्द्र', 'Service', 1, '2018-12-03 23:43:27', '2018-12-03 23:43:27'),
(26, 'Free Quently Asked Question', 'Service', 1, '2018-12-03 23:43:37', '2018-12-03 23:43:37'),
(27, 'कामदारहरुले ध्यान दिनु पर्ने कुराहरु', 'Service', 1, '2018-12-03 23:43:45', '2018-12-03 23:43:45'),
(28, 'खुल्ला गरिएका देशहरु', 'Service', 1, '2018-12-03 23:43:54', '2018-12-03 23:43:54'),
(29, 'Helping Institution', 'Service', 1, '2018-12-03 23:44:01', '2018-12-03 23:44:01'),
(30, 'Skillfull training', '0', 1, '2018-12-12 05:50:31', '2018-12-12 05:50:31'),
(31, 'skilltraining 1', 'Skillfull training', 1, '2018-12-12 05:52:32', '2018-12-12 05:52:32'),
(32, 'skill training 2', 'Skillfull training', 1, '2018-12-12 11:29:04', '2018-12-12 11:29:04'),
(33, 'chart', '0', 1, '2018-12-12 23:41:46', '2018-12-12 23:41:46'),
(34, 'chart1', 'chart', 1, '2018-12-12 23:41:56', '2018-12-12 23:41:56'),
(35, 'chart2', 'chart', 1, '2018-12-12 23:42:03', '2018-12-12 23:42:03'),
(36, 'chart 3', 'chart', 1, '2018-12-13 01:07:06', '2018-12-13 01:07:06'),
(37, 'मृतक कामदारका परिवारलाई आर्थिक सहायता', 'chart', 1, '2018-12-13 01:14:26', '2018-12-13 01:14:26'),
(38, 'जानकारीमूलक सामग्री प्रकाशन एवं वितरण', 'Skillfull training', 1, '2018-12-13 01:40:58', '2018-12-13 01:40:58'),
(39, 'महिलाले अभिमुखीकरण तालिममा तिरेको शुल्क सोधभर्ना', 'Skillfull training', 1, '2018-12-13 01:41:10', '2018-12-13 01:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `second_table`
--

CREATE TABLE `second_table` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` text,
  `name` text,
  `phone` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tollfree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `logo`, `name`, `address`, `phone1`, `phone2`, `tollfree`, `fax`, `email`, `facebook_link`, `twitter_link`, `youtube_link`, `linkedin_link`, `website`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, '5c00c27a19e62_logo.png', 'वैदेशिक रोजगार प्रवर्द्धन बोर्ड नयाँबानेश्वर, काठमाडाैं', 'वैदेशिक रोजगार प्रवर्द्धन बोर्ड नयाँबानेश्वर, काठमाडाैं', '००१-४१०५०६८', '४१०५०६२', '१६६००१-५०००५', '०१४१०५०६१', 'info@fepb.gov.np', 'guhnage', 'me', 'badjijtep', 'bezu', 'http://fepb.gov.np', 2, 2, '2018-11-25 04:41:07', '2018-11-29 23:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `simples`
--

CREATE TABLE `simples` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `simples`
--

INSERT INTO `simples` (`id`, `title`, `category`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(9, 'infographic title here', 'infographic', 1, '<p><strong>this is the title for infographic comes from backend</strong></p>', 2, 2, '2018-12-02 05:27:35', '2018-12-06 03:56:52');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `status`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'kjshkjsd', '5bfab02f58162_29542849_2196988220318791_5257040631484924734_n.jpg', 1, 'laskgzndlgk', 2, 2, '2018-11-25 08:37:19', '2018-11-25 08:37:39'),
(4, 'second slider ho yo', '5bfca3257f2bc_well-images-for-wallpaper-desktop-24.jpg', 1, 'thsi si second slider&nbsp;', 2, NULL, '2018-11-26 20:06:33', '2018-11-26 20:06:33'),
(5, 'third slider', '5bfca34ad2888_3894133980_7e38f821fa.jpg', 1, 'this is third slider description', 2, NULL, '2018-11-26 20:07:10', '2018-11-26 20:07:10');

-- --------------------------------------------------------

--
-- Table structure for table `table_entries`
--

CREATE TABLE `table_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `manual_table_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_entries`
--

INSERT INTO `table_entries` (`id`, `table_field`, `field_type`, `category`, `status`, `created_by`, `updated_by`, `manual_table_id`, `created_at`, `updated_at`) VALUES
(1, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(2, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(3, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:16:37', '2018-12-10 05:16:37'),
(4, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(5, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(6, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:44', '2018-12-10 05:30:44'),
(7, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(8, 'email', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(9, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:30:57', '2018-12-10 05:30:57'),
(10, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(11, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(12, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:04', '2018-12-10 05:32:04'),
(13, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(14, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(15, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:32:36', '2018-12-10 05:32:36'),
(16, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(17, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(18, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:19', '2018-12-10 05:33:19'),
(19, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(20, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(21, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:33:55', '2018-12-10 05:33:55'),
(22, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:23', '2018-12-10 05:35:23'),
(23, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:23', '2018-12-10 05:35:23'),
(24, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:24', '2018-12-10 05:35:24'),
(25, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(26, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(27, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:35:45', '2018-12-10 05:35:45'),
(28, 'title', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:08', '2018-12-10 05:36:08'),
(29, 'name', 'text', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:09', '2018-12-10 05:36:09'),
(30, 'phone', 'integer', 'second_table', 1, 2, NULL, 6, '2018-12-10 05:36:09', '2018-12-10 05:36:09'),
(31, 'name', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(32, 'email', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(33, 'phone', 'DOUBLE', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53'),
(34, 'address', 'TEXT', 'first_table', 1, 2, NULL, 5, '2018-12-10 05:42:53', '2018-12-10 05:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `category_id`, `created_at`, `updated_at`) VALUES
(2, 'this is traiing 2', NULL, NULL, NULL, NULL, 1, '<p>is is for training 2</p>', NULL, 2, 2, 38, '2018-12-12 11:29:40', '2018-12-13 01:55:40'),
(3, 'this is title for training 3', NULL, NULL, NULL, NULL, 1, '<p>thdsi dsfia dodsga&nbsp;</p>\r\n\r\n<ul>\r\n	<li>iukdsajf</li>\r\n	<li>dfsufkjds</li>\r\n	<li>dfsofilksdl</li>\r\n	<li>fdsiugdfgakdsf</li>\r\n	<li>easdviufusadjf</li>\r\n	<li>dsadwasfj</li>\r\n</ul>', NULL, 2, 2, 39, '2018-12-13 01:56:03', '2018-12-13 05:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'fepb admin', 'fepbadmin@admin.com', NULL, '$2y$10$NrdWx3ihpFCdrjxXHRhzSOiH72owuSfNyiA5uuvl4sNIMYkWcVyea', NULL, '2018-11-21 10:11:28', '2018-11-21 10:11:28'),
(2, 'admin', 'admin@admin.com', NULL, '$2y$10$Rb6yZBtNafOYTEfS8Bhsxe7qYi4F.8NtU.p7TurN0u/xavUmfRmfi', 'OwN4aZUwlf2S7mHYAH0FsuDXPXmFcSeyOCG2zd906apGPXndotdj2y9vQN6r', '2018-11-23 20:09:54', '2018-11-23 20:09:54');

-- --------------------------------------------------------

--
-- Table structure for table `yojana_karyakrams`
--

CREATE TABLE `yojana_karyakrams` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `yojana_karyakrams`
--

INSERT INTO `yojana_karyakrams` (`id`, `title`, `date`, `image`, `other_file`, `category`, `status`, `description`, `short_description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'karyakram1', 'askd 32', NULL, '5bff8910167ee_Daily Report (1).pdf', 'karyakram', 1, NULL, NULL, 2, NULL, '2018-11-29 00:52:04', '2018-11-29 00:52:04'),
(2, 'yojana 1', 'aistugd 21', NULL, '5bff892210158_cvmilan-1-4.pdf', 'yojana', 1, NULL, NULL, 2, NULL, '2018-11-29 00:52:22', '2018-11-29 00:52:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abouts_created_by_foreign` (`created_by`),
  ADD KEY `abouts_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_created_by_foreign` (`created_by`),
  ADD KEY `banners_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_created_by_foreign` (`created_by`),
  ADD KEY `blogs_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_sachibalayas_created_by_foreign` (`created_by`),
  ADD KEY `board_sachibalayas_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `board_works`
--
ALTER TABLE `board_works`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_works_created_by_foreign` (`created_by`),
  ADD KEY `board_works_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `charts`
--
ALTER TABLE `charts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charts_created_by_foreign` (`created_by`),
  ADD KEY `charts_updated_by_foreign` (`updated_by`),
  ADD KEY `charts_category_id_foreign` (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `descriptions_created_by_foreign` (`created_by`),
  ADD KEY `descriptions_updated_by_foreign` (`updated_by`),
  ADD KEY `descriptions_category_id_foreign` (`category_id`);

--
-- Indexes for table `excel_files`
--
ALTER TABLE `excel_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `extras_created_by_foreign` (`created_by`),
  ADD KEY `extras_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `first_table`
--
ALTER TABLE `first_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `generals_created_by_foreign` (`created_by`),
  ADD KEY `generals_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `important_links`
--
ALTER TABLE `important_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `important_links_created_by_foreign` (`created_by`),
  ADD KEY `important_links_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `jana_gunasos`
--
ALTER TABLE `jana_gunasos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manual_tables`
--
ALTER TABLE `manual_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manual_tables_general_id_foreign` (`general_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_created_by_foreign` (`created_by`),
  ADD KEY `members_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_apps`
--
ALTER TABLE `mobile_apps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile_apps_created_by_foreign` (`created_by`),
  ADD KEY `mobile_apps_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `multimedia`
--
ALTER TABLE `multimedia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `multimedia_created_by_foreign` (`created_by`),
  ADD KEY `multimedia_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `second_table`
--
ALTER TABLE `second_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `setting_email_unique` (`email`),
  ADD KEY `setting_created_by_foreign` (`created_by`),
  ADD KEY `setting_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `simples`
--
ALTER TABLE `simples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `simples_created_by_foreign` (`created_by`),
  ADD KEY `simples_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_created_by_foreign` (`created_by`),
  ADD KEY `sliders_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `table_entries`
--
ALTER TABLE `table_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `table_entries_created_by_foreign` (`created_by`),
  ADD KEY `table_entries_updated_by_foreign` (`updated_by`),
  ADD KEY `table_entries_manual_table_id_foreign` (`manual_table_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainings_created_by_foreign` (`created_by`),
  ADD KEY `trainings_updated_by_foreign` (`updated_by`),
  ADD KEY `trainings_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `yojana_karyakrams`
--
ALTER TABLE `yojana_karyakrams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `yojana_karyakrams_created_by_foreign` (`created_by`),
  ADD KEY `yojana_karyakrams_updated_by_foreign` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `board_works`
--
ALTER TABLE `board_works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `charts`
--
ALTER TABLE `charts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `excel_files`
--
ALTER TABLE `excel_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `first_table`
--
ALTER TABLE `first_table`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `important_links`
--
ALTER TABLE `important_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jana_gunasos`
--
ALTER TABLE `jana_gunasos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manual_tables`
--
ALTER TABLE `manual_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `mobile_apps`
--
ALTER TABLE `mobile_apps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `second_table`
--
ALTER TABLE `second_table`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `simples`
--
ALTER TABLE `simples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_entries`
--
ALTER TABLE `table_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `yojana_karyakrams`
--
ALTER TABLE `yojana_karyakrams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `abouts`
--
ALTER TABLE `abouts`
  ADD CONSTRAINT `abouts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `abouts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `banners_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blogs_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_sachibalayas`
--
ALTER TABLE `board_sachibalayas`
  ADD CONSTRAINT `board_sachibalayas_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `board_sachibalayas_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `board_works`
--
ALTER TABLE `board_works`
  ADD CONSTRAINT `board_works_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `board_works_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `charts`
--
ALTER TABLE `charts`
  ADD CONSTRAINT `charts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `charts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `charts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `descriptions`
--
ALTER TABLE `descriptions`
  ADD CONSTRAINT `descriptions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descriptions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `extras`
--
ALTER TABLE `extras`
  ADD CONSTRAINT `extras_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `extras_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `generals`
--
ALTER TABLE `generals`
  ADD CONSTRAINT `generals_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `generals_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `important_links`
--
ALTER TABLE `important_links`
  ADD CONSTRAINT `important_links_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `important_links_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manual_tables`
--
ALTER TABLE `manual_tables`
  ADD CONSTRAINT `manual_tables_general_id_foreign` FOREIGN KEY (`general_id`) REFERENCES `generals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `members_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mobile_apps`
--
ALTER TABLE `mobile_apps`
  ADD CONSTRAINT `mobile_apps_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mobile_apps_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `multimedia`
--
ALTER TABLE `multimedia`
  ADD CONSTRAINT `multimedia_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `multimedia_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting`
--
ALTER TABLE `setting`
  ADD CONSTRAINT `setting_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `setting_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `simples`
--
ALTER TABLE `simples`
  ADD CONSTRAINT `simples_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `simples_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sliders_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `table_entries`
--
ALTER TABLE `table_entries`
  ADD CONSTRAINT `table_entries_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `table_entries_manual_table_id_foreign` FOREIGN KEY (`manual_table_id`) REFERENCES `manual_tables` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `table_entries_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trainings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trainings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yojana_karyakrams`
--
ALTER TABLE `yojana_karyakrams`
  ADD CONSTRAINT `yojana_karyakrams_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `yojana_karyakrams_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
